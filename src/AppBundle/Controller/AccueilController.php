<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Article;

class AccueilController extends Controller
{
    /**
     * @Route("/")
     */
    public function showAction()
    {
        $repository = $this->getDoctrine()->getRepository(Article::class);

        $article = $repository->findBy(array(), array('date' => 'DESC'));

        return $this->render('genus/show.html.twig', array(
            'articles'    => $article,
        ));
    }
}