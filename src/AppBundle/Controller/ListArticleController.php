<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Article;

class ListArticleController extends Controller
{
    /**
     * @Route("/list")
     */
    public function  listAction()
    {
        $repository = $this->getDoctrine()->getRepository(Article::class);
        $listArticle = $repository->findAll();

        return $this->render('genus/showlist.html.twig', array(
            'list' => $listArticle,
        ));
    }
}