<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Article;
use AppBundle\Entity\Category;

class OnlyOneController extends Controller
{
    /**
     * @Route("/article/{id}")
     */
    public function  showOneAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(Article::class);
        $repository2 = $this->getDoctrine()->getRepository(Category::class);
        $article = $repository->findOneById($id);
        $category = $repository2->findAll();

        return $this->render('genus/showone.html.twig', array(
            'article' => $article,
            'category' => $category,
            'id' => $id
        ));
    }
}