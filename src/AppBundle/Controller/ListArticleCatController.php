<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Article;
use AppBundle\Entity\Category;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ListArticleCatController extends Controller
{
    /**
     * @Route("/listartcat/{id}")
     */
    public function showAction($id)
    {
        $cat = $this->getDoctrine()->getRepository(Category::class);
        $art = $this->getDoctrine()->getRepository(Article::class);
        $listCat = $cat->findOneById($id);
        $listart = $art->findBy(['category' => $id]);

        return $this->render('genus/listartcat.html.twig', array(
            'category' => $listCat,
            'article' => $listart
        ));
    }
}
