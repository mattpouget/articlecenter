<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Category;

class ListCategoryController extends Controller
{
    /**
     * @Route("/listcat")
     */
    public function  listAction()
    {
        $repository = $this->getDoctrine()->getRepository(Category::class);
        $listCat = $repository->findAll();

        return $this->render('genus/catlist.html.twig', array(
            'category' => $listCat,
        ));
    }
}