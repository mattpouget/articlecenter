<?php

namespace AppBundle\Controller;


use AppBundle\Form\Type\imageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\ArticleFormType;

class AddArticleController extends Controller
{
    /**
     * @Route("/add", name="addArt")
     */
    public function  addAction(Request $request, $article = null)
    {
        $em = $this->getDoctrine()->getManager();
        if(!$article) $article = new Article();
        $category = $em->getRepository('AppBundle:Category')->findAll();
        $form = $this->createForm(ArticleFormType::class, $article);
        $form->handleRequest($request);

         if ($form->isSubmitted() && $form->isValid()) {

             $em->persist($article);
             $em->flush();
        }

        return $this->render('genus/add.html.twig', array(
            'form' => $form->createView(), 'category' => $category
            ));
    }
}