<?php

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AddCatController extends Controller
{
    /**
     * @Route("/addcat", name="addCat")
     */
    public function addAction(Request $request)
    {
        $categories_submit = new Category();
        $form = $this->createFormBuilder($categories_submit)
            ->add('name', TextType::class, array('label' => 'Nom de la catégorie : '))
            ->add('save', SubmitType::class, array('label' => 'Post'))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $submit = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($submit);
            $em->flush();
        }

        return $this->render('genus/addcat.html.twig', array(
            'form' => $form->createView()
        ));
    }
}