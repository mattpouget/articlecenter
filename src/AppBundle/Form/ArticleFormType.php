<?php

namespace AppBundle\Form;

use AppBundle\Entity\Article;
use AppBundle\Form\Type\ImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',       TextType::class, array('label' =>'Nom de l\'article : '))
            ->add('content',    TextareaType::class, array('label' => 'Contenu de l\'article : '))
            ->add('author',     TextType::class, array('label' => 'Nom de l\'auteur : '))
            ->add('date',       DateType::class, array('label' => 'Date de création de l\'article : '))
            ->add('image',      ImageType::class, array('required' => false))
            ->add('category',  EntityType::class, array(
                'label' => 'Catégorie de l\'article : ',
                'class' => 'AppBundle:Category',
                'choice_label' => 'name'))
            ->add('save',SubmitType::class, array('label' => 'Post'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => Article::class));
    }
}