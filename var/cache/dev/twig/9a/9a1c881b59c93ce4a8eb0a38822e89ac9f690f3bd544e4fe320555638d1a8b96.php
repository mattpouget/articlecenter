<?php

/* genus/listartcat.html.twig */
class __TwigTemplate_64764c31b1b9ef13c05e2ef6e14bcbcc59f8a45425c7756aa522e5195e791e75 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "genus/listartcat.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec734e4c06c9b6f61f7f22b85b1a1e7063cc0bc2410032c1edbc1d7be418e73e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec734e4c06c9b6f61f7f22b85b1a1e7063cc0bc2410032c1edbc1d7be418e73e->enter($__internal_ec734e4c06c9b6f61f7f22b85b1a1e7063cc0bc2410032c1edbc1d7be418e73e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/listartcat.html.twig"));

        $__internal_b87922f43313dcf53b0166332bc13aa10734c03db6ebd9e77c2945671e266657 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b87922f43313dcf53b0166332bc13aa10734c03db6ebd9e77c2945671e266657->enter($__internal_b87922f43313dcf53b0166332bc13aa10734c03db6ebd9e77c2945671e266657_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/listartcat.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ec734e4c06c9b6f61f7f22b85b1a1e7063cc0bc2410032c1edbc1d7be418e73e->leave($__internal_ec734e4c06c9b6f61f7f22b85b1a1e7063cc0bc2410032c1edbc1d7be418e73e_prof);

        
        $__internal_b87922f43313dcf53b0166332bc13aa10734c03db6ebd9e77c2945671e266657->leave($__internal_b87922f43313dcf53b0166332bc13aa10734c03db6ebd9e77c2945671e266657_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_3e21e64d7dedd323ed4bdbe351e692d524c359e5ac594658f14ef87c112d2536 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3e21e64d7dedd323ed4bdbe351e692d524c359e5ac594658f14ef87c112d2536->enter($__internal_3e21e64d7dedd323ed4bdbe351e692d524c359e5ac594658f14ef87c112d2536_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_1de6a181e797374d3b8d778cfaf110f1123a4b7d7554b9046b2d14d7a2226629 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1de6a181e797374d3b8d778cfaf110f1123a4b7d7554b9046b2d14d7a2226629->enter($__internal_1de6a181e797374d3b8d778cfaf110f1123a4b7d7554b9046b2d14d7a2226629_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Article Center";
        
        $__internal_1de6a181e797374d3b8d778cfaf110f1123a4b7d7554b9046b2d14d7a2226629->leave($__internal_1de6a181e797374d3b8d778cfaf110f1123a4b7d7554b9046b2d14d7a2226629_prof);

        
        $__internal_3e21e64d7dedd323ed4bdbe351e692d524c359e5ac594658f14ef87c112d2536->leave($__internal_3e21e64d7dedd323ed4bdbe351e692d524c359e5ac594658f14ef87c112d2536_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_3dab7d691744c21454739d2237001e2a3e7b208a1c17113ecf303fb6df6afad0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3dab7d691744c21454739d2237001e2a3e7b208a1c17113ecf303fb6df6afad0->enter($__internal_3dab7d691744c21454739d2237001e2a3e7b208a1c17113ecf303fb6df6afad0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1591f1950b8dfd65a8d823869c23290fcb795a963dafce1c26481633dc98de7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1591f1950b8dfd65a8d823869c23290fcb795a963dafce1c26481633dc98de7c->enter($__internal_1591f1950b8dfd65a8d823869c23290fcb795a963dafce1c26481633dc98de7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.min.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"listcat\">Catégories</a></li>
                    ";
        // line 25
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 26
            echo "                        <li><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\" class=\"btn btn-danger\">Logout</a></li>
                    ";
        } else {
            // line 28
            echo "                        <li><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\" class=\"btn btn-success\">Login</a></li>
                    ";
        }
        // line 30
        echo "                    <li><a href=\"/register\" class=\"btn btn-info\">Sign up.</a></li>
                </ul>
                ";
        // line 32
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            // line 33
            echo "                    <ul class=\"nav navbar-nav navbar-right\">
                        <li><a href=\"/admin\">AdminBoard</a></li>
                    </ul>
                ";
        }
        // line 37
        echo "            </div>
        </div>
    </nav>

    <div class=\"jumbotron\">
        <h1>Tout les articles ";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "name", array()), "html", null, true);
        echo "</h1>
        <table class=\"table table-striped table-hover \">

            <tbody>
            ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["article"]);
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 47
            echo "                <tr class=\"active\">
                    <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
            echo "</td>
                    <td><img src=\"/images/";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["article"], "image", array()), "imageName", array()), "html", null, true);
            echo "\" width=\"40\" height=\"40\"></td>
                    <td>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "name", array()), "html", null, true);
            echo "</td>
                    <td><a href=\"/article/";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary btn-xs\">Voir l'article &rarr;</a></td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "            </tbody>
        </table>
    </div>


";
        
        $__internal_1591f1950b8dfd65a8d823869c23290fcb795a963dafce1c26481633dc98de7c->leave($__internal_1591f1950b8dfd65a8d823869c23290fcb795a963dafce1c26481633dc98de7c_prof);

        
        $__internal_3dab7d691744c21454739d2237001e2a3e7b208a1c17113ecf303fb6df6afad0->leave($__internal_3dab7d691744c21454739d2237001e2a3e7b208a1c17113ecf303fb6df6afad0_prof);

    }

    public function getTemplateName()
    {
        return "genus/listartcat.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 54,  151 => 51,  147 => 50,  143 => 49,  139 => 48,  136 => 47,  132 => 46,  125 => 42,  118 => 37,  112 => 33,  110 => 32,  106 => 30,  100 => 28,  94 => 26,  92 => 25,  71 => 7,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %}Article Center{% endblock %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.min.css') }}\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"listcat\">Catégories</a></li>
                    {% if is_granted('ROLE_USER') %}
                        <li><a href=\"{{ path('fos_user_security_logout') }}\" class=\"btn btn-danger\">Logout</a></li>
                    {% else %}
                        <li><a href=\"{{ path('fos_user_security_login') }}\" class=\"btn btn-success\">Login</a></li>
                    {% endif %}
                    <li><a href=\"/register\" class=\"btn btn-info\">Sign up.</a></li>
                </ul>
                {% if is_granted('ROLE_ADMIN') %}
                    <ul class=\"nav navbar-nav navbar-right\">
                        <li><a href=\"/admin\">AdminBoard</a></li>
                    </ul>
                {% endif %}
            </div>
        </div>
    </nav>

    <div class=\"jumbotron\">
        <h1>Tout les articles {{ category.name }}</h1>
        <table class=\"table table-striped table-hover \">

            <tbody>
            {% for article in article %}
                <tr class=\"active\">
                    <td>{{ article.id }}</td>
                    <td><img src=\"/images/{{ article.image.imageName }}\" width=\"40\" height=\"40\"></td>
                    <td>{{ article.name }}</td>
                    <td><a href=\"/article/{{ article.id }}\" class=\"btn btn-primary btn-xs\">Voir l'article &rarr;</a></td>
                </tr>
            {% endfor %}
            </tbody>
        </table>
    </div>


{% endblock %}", "genus/listartcat.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/genus/listartcat.html.twig");
    }
}
