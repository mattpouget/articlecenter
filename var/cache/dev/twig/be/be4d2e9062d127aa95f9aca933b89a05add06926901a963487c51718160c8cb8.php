<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_0686bf2cf6a9c9244fc46d0895d590bdddbab50822442a59b0a9e96605ccc990 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6d5224c7ebdc49117ade83bd314830d8f812917f324e56a5a43ed7f2b692333f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6d5224c7ebdc49117ade83bd314830d8f812917f324e56a5a43ed7f2b692333f->enter($__internal_6d5224c7ebdc49117ade83bd314830d8f812917f324e56a5a43ed7f2b692333f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_5385aa1c47f19b58bb9da21316e113ea567f75812b80e3ed7e8bfdc8380e223f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5385aa1c47f19b58bb9da21316e113ea567f75812b80e3ed7e8bfdc8380e223f->enter($__internal_5385aa1c47f19b58bb9da21316e113ea567f75812b80e3ed7e8bfdc8380e223f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_6d5224c7ebdc49117ade83bd314830d8f812917f324e56a5a43ed7f2b692333f->leave($__internal_6d5224c7ebdc49117ade83bd314830d8f812917f324e56a5a43ed7f2b692333f_prof);

        
        $__internal_5385aa1c47f19b58bb9da21316e113ea567f75812b80e3ed7e8bfdc8380e223f->leave($__internal_5385aa1c47f19b58bb9da21316e113ea567f75812b80e3ed7e8bfdc8380e223f_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_f696b1f53ea81195c0b86ce09f75a2ec117ccc1f6b058ab7987781c7f56f71e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f696b1f53ea81195c0b86ce09f75a2ec117ccc1f6b058ab7987781c7f56f71e6->enter($__internal_f696b1f53ea81195c0b86ce09f75a2ec117ccc1f6b058ab7987781c7f56f71e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_7f47f8d384130059b3efe581969383e2eca34e143a7f67f47bdf4df36df7f7ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7f47f8d384130059b3efe581969383e2eca34e143a7f67f47bdf4df36df7f7ed->enter($__internal_7f47f8d384130059b3efe581969383e2eca34e143a7f67f47bdf4df36df7f7ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_7f47f8d384130059b3efe581969383e2eca34e143a7f67f47bdf4df36df7f7ed->leave($__internal_7f47f8d384130059b3efe581969383e2eca34e143a7f67f47bdf4df36df7f7ed_prof);

        
        $__internal_f696b1f53ea81195c0b86ce09f75a2ec117ccc1f6b058ab7987781c7f56f71e6->leave($__internal_f696b1f53ea81195c0b86ce09f75a2ec117ccc1f6b058ab7987781c7f56f71e6_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_a92ab05e106a5f834e781bb7235eaaffce0ae94255ba3eaa63d5a78b5307e59e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a92ab05e106a5f834e781bb7235eaaffce0ae94255ba3eaa63d5a78b5307e59e->enter($__internal_a92ab05e106a5f834e781bb7235eaaffce0ae94255ba3eaa63d5a78b5307e59e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_ea3ce4d4d9ec9a070cc34b569a7221b5144fba00505a51981d1471a7f34fd6b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ea3ce4d4d9ec9a070cc34b569a7221b5144fba00505a51981d1471a7f34fd6b8->enter($__internal_ea3ce4d4d9ec9a070cc34b569a7221b5144fba00505a51981d1471a7f34fd6b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_ea3ce4d4d9ec9a070cc34b569a7221b5144fba00505a51981d1471a7f34fd6b8->leave($__internal_ea3ce4d4d9ec9a070cc34b569a7221b5144fba00505a51981d1471a7f34fd6b8_prof);

        
        $__internal_a92ab05e106a5f834e781bb7235eaaffce0ae94255ba3eaa63d5a78b5307e59e->leave($__internal_a92ab05e106a5f834e781bb7235eaaffce0ae94255ba3eaa63d5a78b5307e59e_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_09a43fedb06a921acbe2902006567219cc28611f14a4a678247ce20e9bfff369 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09a43fedb06a921acbe2902006567219cc28611f14a4a678247ce20e9bfff369->enter($__internal_09a43fedb06a921acbe2902006567219cc28611f14a4a678247ce20e9bfff369_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_36b84573bee069eb17e9401dc8d0a974d05c2be2be97e2cc4c9cc408dd030937 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36b84573bee069eb17e9401dc8d0a974d05c2be2be97e2cc4c9cc408dd030937->enter($__internal_36b84573bee069eb17e9401dc8d0a974d05c2be2be97e2cc4c9cc408dd030937_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_36b84573bee069eb17e9401dc8d0a974d05c2be2be97e2cc4c9cc408dd030937->leave($__internal_36b84573bee069eb17e9401dc8d0a974d05c2be2be97e2cc4c9cc408dd030937_prof);

        
        $__internal_09a43fedb06a921acbe2902006567219cc28611f14a4a678247ce20e9bfff369->leave($__internal_09a43fedb06a921acbe2902006567219cc28611f14a4a678247ce20e9bfff369_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "/Users/matt/Prog/Symfo/Mymfo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
