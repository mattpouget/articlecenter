<?php

/* category/new.html.twig */
class __TwigTemplate_a27bb0cb282a59b3115892434b3dab6992463b42280e0847d6bca5dc90010f84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "category/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9a75f2688300283344185db0c0d9b9fc460650af3003bfb8221807ed7135ec84 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9a75f2688300283344185db0c0d9b9fc460650af3003bfb8221807ed7135ec84->enter($__internal_9a75f2688300283344185db0c0d9b9fc460650af3003bfb8221807ed7135ec84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "category/new.html.twig"));

        $__internal_a17542420376f5a220b6712328f1b527580de9be07eb2b08b1065a94262e581d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a17542420376f5a220b6712328f1b527580de9be07eb2b08b1065a94262e581d->enter($__internal_a17542420376f5a220b6712328f1b527580de9be07eb2b08b1065a94262e581d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "category/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9a75f2688300283344185db0c0d9b9fc460650af3003bfb8221807ed7135ec84->leave($__internal_9a75f2688300283344185db0c0d9b9fc460650af3003bfb8221807ed7135ec84_prof);

        
        $__internal_a17542420376f5a220b6712328f1b527580de9be07eb2b08b1065a94262e581d->leave($__internal_a17542420376f5a220b6712328f1b527580de9be07eb2b08b1065a94262e581d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_e674aa18840f538d4b5b21a7a36a15549c35a7615d35dfacd8e0b824ef5d61de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e674aa18840f538d4b5b21a7a36a15549c35a7615d35dfacd8e0b824ef5d61de->enter($__internal_e674aa18840f538d4b5b21a7a36a15549c35a7615d35dfacd8e0b824ef5d61de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_021e01c4820be377f8ab0b22686d4a035c59518dce94bbbdea0650c4ea0d4ea5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_021e01c4820be377f8ab0b22686d4a035c59518dce94bbbdea0650c4ea0d4ea5->enter($__internal_021e01c4820be377f8ab0b22686d4a035c59518dce94bbbdea0650c4ea0d4ea5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.min.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>

    <h1>Category creation</h1>

    ";
        // line 10
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 11
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" class=\"btn btn-info\"/>
    ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    <a href=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("catcrud_index");
        echo "\" class=\"btn btn-default\">Back to the list</a>
";
        
        $__internal_021e01c4820be377f8ab0b22686d4a035c59518dce94bbbdea0650c4ea0d4ea5->leave($__internal_021e01c4820be377f8ab0b22686d4a035c59518dce94bbbdea0650c4ea0d4ea5_prof);

        
        $__internal_e674aa18840f538d4b5b21a7a36a15549c35a7615d35dfacd8e0b824ef5d61de->leave($__internal_e674aa18840f538d4b5b21a7a36a15549c35a7615d35dfacd8e0b824ef5d61de_prof);

    }

    public function getTemplateName()
    {
        return "category/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 14,  69 => 13,  64 => 11,  60 => 10,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.min.css') }}\" crossorigin=\"anonymous\">
    </head>

    <h1>Category creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" class=\"btn btn-info\"/>
    {{ form_end(form) }}
    <a href=\"{{ path('catcrud_index') }}\" class=\"btn btn-default\">Back to the list</a>
{% endblock %}
", "category/new.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/category/new.html.twig");
    }
}
