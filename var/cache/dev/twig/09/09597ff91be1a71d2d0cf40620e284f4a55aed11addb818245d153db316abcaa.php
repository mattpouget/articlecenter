<?php

/* genus/showlist.html.twig */
class __TwigTemplate_c82a1ca284db5cbe0f124fdf1ea7a3c334fdf2c0f1eb886dfd2b64c8f51ee353 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "genus/showlist.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0a5cc4014fc623e033d14f3b361389cdbb629d6dc3728db8364b5182d3c278fd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a5cc4014fc623e033d14f3b361389cdbb629d6dc3728db8364b5182d3c278fd->enter($__internal_0a5cc4014fc623e033d14f3b361389cdbb629d6dc3728db8364b5182d3c278fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/showlist.html.twig"));

        $__internal_52492e47f026b62258a3bd61c77770a465cc89a0bb186cad69d013cc5e7540b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_52492e47f026b62258a3bd61c77770a465cc89a0bb186cad69d013cc5e7540b4->enter($__internal_52492e47f026b62258a3bd61c77770a465cc89a0bb186cad69d013cc5e7540b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/showlist.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0a5cc4014fc623e033d14f3b361389cdbb629d6dc3728db8364b5182d3c278fd->leave($__internal_0a5cc4014fc623e033d14f3b361389cdbb629d6dc3728db8364b5182d3c278fd_prof);

        
        $__internal_52492e47f026b62258a3bd61c77770a465cc89a0bb186cad69d013cc5e7540b4->leave($__internal_52492e47f026b62258a3bd61c77770a465cc89a0bb186cad69d013cc5e7540b4_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_9c53e5cce4609e212c73fb1804780684c5e9a039f4761d34ed76feaf4ccba6b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9c53e5cce4609e212c73fb1804780684c5e9a039f4761d34ed76feaf4ccba6b0->enter($__internal_9c53e5cce4609e212c73fb1804780684c5e9a039f4761d34ed76feaf4ccba6b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_1e5b66bea69436eb8f8cebc94d50355bf6dc5eddd6ad30fd3f84d09368ee13fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e5b66bea69436eb8f8cebc94d50355bf6dc5eddd6ad30fd3f84d09368ee13fb->enter($__internal_1e5b66bea69436eb8f8cebc94d50355bf6dc5eddd6ad30fd3f84d09368ee13fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Article Center";
        
        $__internal_1e5b66bea69436eb8f8cebc94d50355bf6dc5eddd6ad30fd3f84d09368ee13fb->leave($__internal_1e5b66bea69436eb8f8cebc94d50355bf6dc5eddd6ad30fd3f84d09368ee13fb_prof);

        
        $__internal_9c53e5cce4609e212c73fb1804780684c5e9a039f4761d34ed76feaf4ccba6b0->leave($__internal_9c53e5cce4609e212c73fb1804780684c5e9a039f4761d34ed76feaf4ccba6b0_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_82c319f679893f2128806244194172f2d731d37374c2449e01b09e94584976ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_82c319f679893f2128806244194172f2d731d37374c2449e01b09e94584976ec->enter($__internal_82c319f679893f2128806244194172f2d731d37374c2449e01b09e94584976ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_fc4c64c15c7e610c78149f32b48cc63bc6bb67b4bc8dddd39d18ad6b9f42eabd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fc4c64c15c7e610c78149f32b48cc63bc6bb67b4bc8dddd39d18ad6b9f42eabd->enter($__internal_fc4c64c15c7e610c78149f32b48cc63bc6bb67b4bc8dddd39d18ad6b9f42eabd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.min.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"listcat\">Catégories</a></li>
                    ";
        // line 25
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 26
            echo "                        <li><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\" class=\"btn btn-danger\">Logout</a></li>
                    ";
        } else {
            // line 28
            echo "                        <li><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\" class=\"btn btn-success\">Login</a></li>
                    ";
        }
        // line 30
        echo "                    <li><a href=\"/register\" class=\"btn btn-info\">Sign up.</a></li>
                </ul>
                ";
        // line 32
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            // line 33
            echo "                    <ul class=\"nav navbar-nav navbar-right\">
                        <li><a href=\"/admin\">AdminBoard</a></li>
                    </ul>
                ";
        }
        // line 37
        echo "            </div>
        </div>
    </nav>

    <div class=\"jumbotron\">
        <h1>Tout les articles</h1>
        <table class=\"table table-striped table-hover \">
            <tbody>
           ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["list"]) ? $context["list"] : $this->getContext($context, "list")));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 46
            echo "            <tr class=\"active\">
                <td><img src=\"/images/";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["article"], "image", array()), "imageName", array()), "html", null, true);
            echo "\" width=\"40\" height=\"40\"></td>
                <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["article"], "category", array()), "name", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "name", array()), "html", null, true);
            echo "</td>
                <td><i>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "Author", array()), "html", null, true);
            echo "</i></td>
                <td><a href=\"/article/";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary btn-xs\">Voir l'article &rarr;</a></td>
            </tr>
           ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "            </tbody>
        </table>
    </div>


";
        
        $__internal_fc4c64c15c7e610c78149f32b48cc63bc6bb67b4bc8dddd39d18ad6b9f42eabd->leave($__internal_fc4c64c15c7e610c78149f32b48cc63bc6bb67b4bc8dddd39d18ad6b9f42eabd_prof);

        
        $__internal_82c319f679893f2128806244194172f2d731d37374c2449e01b09e94584976ec->leave($__internal_82c319f679893f2128806244194172f2d731d37374c2449e01b09e94584976ec_prof);

    }

    public function getTemplateName()
    {
        return "genus/showlist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 54,  151 => 51,  147 => 50,  143 => 49,  139 => 48,  135 => 47,  132 => 46,  128 => 45,  118 => 37,  112 => 33,  110 => 32,  106 => 30,  100 => 28,  94 => 26,  92 => 25,  71 => 7,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %}Article Center{% endblock %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.min.css') }}\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"listcat\">Catégories</a></li>
                    {% if is_granted('ROLE_USER') %}
                        <li><a href=\"{{ path('fos_user_security_logout') }}\" class=\"btn btn-danger\">Logout</a></li>
                    {% else %}
                        <li><a href=\"{{ path('fos_user_security_login') }}\" class=\"btn btn-success\">Login</a></li>
                    {% endif %}
                    <li><a href=\"/register\" class=\"btn btn-info\">Sign up.</a></li>
                </ul>
                {% if is_granted('ROLE_ADMIN') %}
                    <ul class=\"nav navbar-nav navbar-right\">
                        <li><a href=\"/admin\">AdminBoard</a></li>
                    </ul>
                {% endif %}
            </div>
        </div>
    </nav>

    <div class=\"jumbotron\">
        <h1>Tout les articles</h1>
        <table class=\"table table-striped table-hover \">
            <tbody>
           {% for article in list %}
            <tr class=\"active\">
                <td><img src=\"/images/{{ article.image.imageName }}\" width=\"40\" height=\"40\"></td>
                <td>{{ article.category.name }}</td>
                <td>{{ article.name }}</td>
                <td><i>{{ article.Author }}</i></td>
                <td><a href=\"/article/{{ article.id }}\" class=\"btn btn-primary btn-xs\">Voir l'article &rarr;</a></td>
            </tr>
           {% endfor %}
            </tbody>
        </table>
    </div>


{% endblock %}", "genus/showlist.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/genus/showlist.html.twig");
    }
}
