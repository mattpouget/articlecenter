<?php

/* genus/admin.html.twig */
class __TwigTemplate_fba21c20085aeb7a2c31221b06e1f0ec8f8dff430ea3391d87374c1e0c28a410 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "genus/admin.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9fb451b2bbaa99e4b9997cb7340e096753ea4c877bfdb8ddeaaa4ef1c40e2f86 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9fb451b2bbaa99e4b9997cb7340e096753ea4c877bfdb8ddeaaa4ef1c40e2f86->enter($__internal_9fb451b2bbaa99e4b9997cb7340e096753ea4c877bfdb8ddeaaa4ef1c40e2f86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/admin.html.twig"));

        $__internal_f600e7a6fb005b559f1873154d49c7a0a28ba72ead0eb88140e4a8e34d86e7c2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f600e7a6fb005b559f1873154d49c7a0a28ba72ead0eb88140e4a8e34d86e7c2->enter($__internal_f600e7a6fb005b559f1873154d49c7a0a28ba72ead0eb88140e4a8e34d86e7c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/admin.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9fb451b2bbaa99e4b9997cb7340e096753ea4c877bfdb8ddeaaa4ef1c40e2f86->leave($__internal_9fb451b2bbaa99e4b9997cb7340e096753ea4c877bfdb8ddeaaa4ef1c40e2f86_prof);

        
        $__internal_f600e7a6fb005b559f1873154d49c7a0a28ba72ead0eb88140e4a8e34d86e7c2->leave($__internal_f600e7a6fb005b559f1873154d49c7a0a28ba72ead0eb88140e4a8e34d86e7c2_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_9c7820f7c0e88fe646a8b9801a92a38e5d1e3c3cb0f365b2cdcf64a8d3af6385 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9c7820f7c0e88fe646a8b9801a92a38e5d1e3c3cb0f365b2cdcf64a8d3af6385->enter($__internal_9c7820f7c0e88fe646a8b9801a92a38e5d1e3c3cb0f365b2cdcf64a8d3af6385_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_cf3a8b99c8283ade774eae4eb65c4ad1f074074242888143c9fae794dddc42b0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cf3a8b99c8283ade774eae4eb65c4ad1f074074242888143c9fae794dddc42b0->enter($__internal_cf3a8b99c8283ade774eae4eb65c4ad1f074074242888143c9fae794dddc42b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "AdminBoard";
        
        $__internal_cf3a8b99c8283ade774eae4eb65c4ad1f074074242888143c9fae794dddc42b0->leave($__internal_cf3a8b99c8283ade774eae4eb65c4ad1f074074242888143c9fae794dddc42b0_prof);

        
        $__internal_9c7820f7c0e88fe646a8b9801a92a38e5d1e3c3cb0f365b2cdcf64a8d3af6385->leave($__internal_9c7820f7c0e88fe646a8b9801a92a38e5d1e3c3cb0f365b2cdcf64a8d3af6385_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_007ac3d1a258117c902d0fa28bc799aea5819618818a238f6dbcd7c8ea367578 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_007ac3d1a258117c902d0fa28bc799aea5819618818a238f6dbcd7c8ea367578->enter($__internal_007ac3d1a258117c902d0fa28bc799aea5819618818a238f6dbcd7c8ea367578_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b5085a365d968f1790cf831ecfc8a53dde968bcc99f4665a58236c08c788ae56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b5085a365d968f1790cf831ecfc8a53dde968bcc99f4665a58236c08c788ae56->enter($__internal_b5085a365d968f1790cf831ecfc8a53dde968bcc99f4665a58236c08c788ae56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
<head>
    <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
</head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <h1>AdminBoard</h1>
        <a href=\"/add\" class=\"btn btn-default\">Ajouter un article</a><br>

        <h3>CRUD</h3>
    <div class=\"btn-group btn-group-justified\">
        <a href=\"/artcrud\" class=\"btn btn-default\">Article Edit</a>
        <a href=\"/catcrud\" class=\"btn btn-default\">Category Edit</a>
        <a href=\"/usercrud\" class=\"btn btn-default\">User Edit</a>
    </div>

";
        
        $__internal_b5085a365d968f1790cf831ecfc8a53dde968bcc99f4665a58236c08c788ae56->leave($__internal_b5085a365d968f1790cf831ecfc8a53dde968bcc99f4665a58236c08c788ae56_prof);

        
        $__internal_007ac3d1a258117c902d0fa28bc799aea5819618818a238f6dbcd7c8ea367578->leave($__internal_007ac3d1a258117c902d0fa28bc799aea5819618818a238f6dbcd7c8ea367578_prof);

    }

    public function getTemplateName()
    {
        return "genus/admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 7,  68 => 5,  59 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %}AdminBoard{% endblock %}
{% block body %}

<head>
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.css') }}\" crossorigin=\"anonymous\">
</head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <h1>AdminBoard</h1>
        <a href=\"/add\" class=\"btn btn-default\">Ajouter un article</a><br>

        <h3>CRUD</h3>
    <div class=\"btn-group btn-group-justified\">
        <a href=\"/artcrud\" class=\"btn btn-default\">Article Edit</a>
        <a href=\"/catcrud\" class=\"btn btn-default\">Category Edit</a>
        <a href=\"/usercrud\" class=\"btn btn-default\">User Edit</a>
    </div>

{% endblock %}", "genus/admin.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/genus/admin.html.twig");
    }
}
