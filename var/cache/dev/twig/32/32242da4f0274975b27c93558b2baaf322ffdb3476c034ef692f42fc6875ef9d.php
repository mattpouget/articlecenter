<?php

/* genus/showone.html.twig */
class __TwigTemplate_b8a6ca43aa8a65136e4a3790b819ea2a5c461996810ebe0ef0a2e3fa59417160 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "genus/showone.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_80ffef7b7d5b58474242ae413d1bf6f0e62789e7e5f987a0959a04468c6c17f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_80ffef7b7d5b58474242ae413d1bf6f0e62789e7e5f987a0959a04468c6c17f9->enter($__internal_80ffef7b7d5b58474242ae413d1bf6f0e62789e7e5f987a0959a04468c6c17f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/showone.html.twig"));

        $__internal_e5bd99e74eca3021714e0a3571aeb02423b673345d5d2a1bd4ec3514d77ecd2b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5bd99e74eca3021714e0a3571aeb02423b673345d5d2a1bd4ec3514d77ecd2b->enter($__internal_e5bd99e74eca3021714e0a3571aeb02423b673345d5d2a1bd4ec3514d77ecd2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/showone.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_80ffef7b7d5b58474242ae413d1bf6f0e62789e7e5f987a0959a04468c6c17f9->leave($__internal_80ffef7b7d5b58474242ae413d1bf6f0e62789e7e5f987a0959a04468c6c17f9_prof);

        
        $__internal_e5bd99e74eca3021714e0a3571aeb02423b673345d5d2a1bd4ec3514d77ecd2b->leave($__internal_e5bd99e74eca3021714e0a3571aeb02423b673345d5d2a1bd4ec3514d77ecd2b_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_60027c72e7b1cef8881198e24b4410f258d1b7938dc486c737f8f53e5e90454d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60027c72e7b1cef8881198e24b4410f258d1b7938dc486c737f8f53e5e90454d->enter($__internal_60027c72e7b1cef8881198e24b4410f258d1b7938dc486c737f8f53e5e90454d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_d12a972b6e3c67ec7e81d0f28cc70e45dfc7c9b485c803883b0dd275c08d82af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d12a972b6e3c67ec7e81d0f28cc70e45dfc7c9b485c803883b0dd275c08d82af->enter($__internal_d12a972b6e3c67ec7e81d0f28cc70e45dfc7c9b485c803883b0dd275c08d82af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Article Center";
        
        $__internal_d12a972b6e3c67ec7e81d0f28cc70e45dfc7c9b485c803883b0dd275c08d82af->leave($__internal_d12a972b6e3c67ec7e81d0f28cc70e45dfc7c9b485c803883b0dd275c08d82af_prof);

        
        $__internal_60027c72e7b1cef8881198e24b4410f258d1b7938dc486c737f8f53e5e90454d->leave($__internal_60027c72e7b1cef8881198e24b4410f258d1b7938dc486c737f8f53e5e90454d_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_b878d5e096e2f965c1ed5fa0b58699a113349eacfa0d6febf56794395826ab1f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b878d5e096e2f965c1ed5fa0b58699a113349eacfa0d6febf56794395826ab1f->enter($__internal_b878d5e096e2f965c1ed5fa0b58699a113349eacfa0d6febf56794395826ab1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ec3ce46046ddfe77060cd15ecd3c481ebb0eb12e143bb0df62850940eea43368 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ec3ce46046ddfe77060cd15ecd3c481ebb0eb12e143bb0df62850940eea43368->enter($__internal_ec3ce46046ddfe77060cd15ecd3c481ebb0eb12e143bb0df62850940eea43368_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                    ";
        // line 25
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 26
            echo "                        <li><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\" class=\"btn btn-danger\">Logout</a></li>
                    ";
        } else {
            // line 28
            echo "                        <li><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\" class=\"btn btn-success\">Login</a></li>
                    ";
        }
        // line 30
        echo "                    <li><a href=\"/register\" class=\"btn btn-info\">Sign up.</a></li>
                </ul>
                ";
        // line 32
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            // line 33
            echo "                    <ul class=\"nav navbar-nav navbar-right\">
                        <li><a href=\"/admin\">AdminBoard</a></li>
                    </ul>
                ";
        }
        // line 37
        echo "            </div>
        </div>
    </nav>

    <h3>Article numéro ";
        // line 41
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo " :</h3>
    <h1>";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "name", array()), "html", null, true);
        echo "</h1>
    <h2>Catégorie ";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "category", array()), "name", array()), "html", null, true);
        echo "</h2>

    <img src=\"/images/";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "image", array()), "imageName", array()), "html", null, true);
        echo "\" width=\"60%\" height=\"60%\"><br>

    <blockquote>
        <p>";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "content", array()), "html", null, true);
        echo "</p>
        <small>Écrit par <cite title=\"Source Title\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "author", array()), "html", null, true);
        echo "</cite></small>
    </blockquote>
";
        
        $__internal_ec3ce46046ddfe77060cd15ecd3c481ebb0eb12e143bb0df62850940eea43368->leave($__internal_ec3ce46046ddfe77060cd15ecd3c481ebb0eb12e143bb0df62850940eea43368_prof);

        
        $__internal_b878d5e096e2f965c1ed5fa0b58699a113349eacfa0d6febf56794395826ab1f->leave($__internal_b878d5e096e2f965c1ed5fa0b58699a113349eacfa0d6febf56794395826ab1f_prof);

    }

    public function getTemplateName()
    {
        return "genus/showone.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 49,  143 => 48,  137 => 45,  132 => 43,  128 => 42,  124 => 41,  118 => 37,  112 => 33,  110 => 32,  106 => 30,  100 => 28,  94 => 26,  92 => 25,  71 => 7,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %}Article Center{% endblock %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.css') }}\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                    {% if is_granted('ROLE_USER') %}
                        <li><a href=\"{{ path('fos_user_security_logout') }}\" class=\"btn btn-danger\">Logout</a></li>
                    {% else %}
                        <li><a href=\"{{ path('fos_user_security_login') }}\" class=\"btn btn-success\">Login</a></li>
                    {% endif %}
                    <li><a href=\"/register\" class=\"btn btn-info\">Sign up.</a></li>
                </ul>
                {% if is_granted('ROLE_ADMIN') %}
                    <ul class=\"nav navbar-nav navbar-right\">
                        <li><a href=\"/admin\">AdminBoard</a></li>
                    </ul>
                {% endif %}
            </div>
        </div>
    </nav>

    <h3>Article numéro {{ id }} :</h3>
    <h1>{{ article.name }}</h1>
    <h2>Catégorie {{ article.category.name}}</h2>

    <img src=\"/images/{{ article.image.imageName }}\" width=\"60%\" height=\"60%\"><br>

    <blockquote>
        <p>{{ article.content }}</p>
        <small>Écrit par <cite title=\"Source Title\">{{ article.author }}</cite></small>
    </blockquote>
{% endblock %}", "genus/showone.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/genus/showone.html.twig");
    }
}
