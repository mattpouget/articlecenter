<?php

/* @FOSUser/layout.html.twig */
class __TwigTemplate_108b054c42abceb4b7a080ccaa43e42e542339913d1aef0fdffb0403bfe52f59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@FOSUser/layout.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d821122f330889669f3bf923cc67023266d0ba215f6cf53791c1d416ee73aab1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d821122f330889669f3bf923cc67023266d0ba215f6cf53791c1d416ee73aab1->enter($__internal_d821122f330889669f3bf923cc67023266d0ba215f6cf53791c1d416ee73aab1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $__internal_40d198e2380c65078936b08afc4966802a10967eba092bbdaaef4cf56e6cb06c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_40d198e2380c65078936b08afc4966802a10967eba092bbdaaef4cf56e6cb06c->enter($__internal_40d198e2380c65078936b08afc4966802a10967eba092bbdaaef4cf56e6cb06c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d821122f330889669f3bf923cc67023266d0ba215f6cf53791c1d416ee73aab1->leave($__internal_d821122f330889669f3bf923cc67023266d0ba215f6cf53791c1d416ee73aab1_prof);

        
        $__internal_40d198e2380c65078936b08afc4966802a10967eba092bbdaaef4cf56e6cb06c->leave($__internal_40d198e2380c65078936b08afc4966802a10967eba092bbdaaef4cf56e6cb06c_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_53877f4175ab077b9cb16532825bff8a24880768d33ec9c41ca4870b6f6bcb1f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_53877f4175ab077b9cb16532825bff8a24880768d33ec9c41ca4870b6f6bcb1f->enter($__internal_53877f4175ab077b9cb16532825bff8a24880768d33ec9c41ca4870b6f6bcb1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_cb5dd6f2b01b5d5891f37a2912c34044316b3eab15e59dcb0edfcbd47a8d0703 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb5dd6f2b01b5d5891f37a2912c34044316b3eab15e59dcb0edfcbd47a8d0703->enter($__internal_cb5dd6f2b01b5d5891f37a2912c34044316b3eab15e59dcb0edfcbd47a8d0703_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        $this->displayBlock('fos_user_content', $context, $blocks);
        
        $__internal_cb5dd6f2b01b5d5891f37a2912c34044316b3eab15e59dcb0edfcbd47a8d0703->leave($__internal_cb5dd6f2b01b5d5891f37a2912c34044316b3eab15e59dcb0edfcbd47a8d0703_prof);

        
        $__internal_53877f4175ab077b9cb16532825bff8a24880768d33ec9c41ca4870b6f6bcb1f->leave($__internal_53877f4175ab077b9cb16532825bff8a24880768d33ec9c41ca4870b6f6bcb1f_prof);

    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b0c367fa13d76a3845c1f8530bc40aefe815f70d2956666063521b9cdc86c385 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b0c367fa13d76a3845c1f8530bc40aefe815f70d2956666063521b9cdc86c385->enter($__internal_b0c367fa13d76a3845c1f8530bc40aefe815f70d2956666063521b9cdc86c385_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_b15055accc669dcd657979b9f17c9b02188a0a677b0147aedab42626786f2803 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b15055accc669dcd657979b9f17c9b02188a0a677b0147aedab42626786f2803->enter($__internal_b15055accc669dcd657979b9f17c9b02188a0a677b0147aedab42626786f2803_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        
        $__internal_b15055accc669dcd657979b9f17c9b02188a0a677b0147aedab42626786f2803->leave($__internal_b15055accc669dcd657979b9f17c9b02188a0a677b0147aedab42626786f2803_prof);

        
        $__internal_b0c367fa13d76a3845c1f8530bc40aefe815f70d2956666063521b9cdc86c385->leave($__internal_b0c367fa13d76a3845c1f8530bc40aefe815f70d2956666063521b9cdc86c385_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    {% block fos_user_content %}{% endblock %}
{% endblock %}", "@FOSUser/layout.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/FOSUserBundle/views/layout.html.twig");
    }
}
