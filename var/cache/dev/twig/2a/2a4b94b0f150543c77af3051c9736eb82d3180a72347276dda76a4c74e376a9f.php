<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_3adef4ac8a6ac3716fdfdf3d60f9e67105f7c97a98a0711ac4696c0408a4d06d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e9bbc7f964915204a312035ac3a5dd95bb729bb95551135a08bdf5519378da2f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e9bbc7f964915204a312035ac3a5dd95bb729bb95551135a08bdf5519378da2f->enter($__internal_e9bbc7f964915204a312035ac3a5dd95bb729bb95551135a08bdf5519378da2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_dd58440ae406b8d1bb3a52086b68a73115e983cab0d183f20edd0f943544226b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd58440ae406b8d1bb3a52086b68a73115e983cab0d183f20edd0f943544226b->enter($__internal_dd58440ae406b8d1bb3a52086b68a73115e983cab0d183f20edd0f943544226b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e9bbc7f964915204a312035ac3a5dd95bb729bb95551135a08bdf5519378da2f->leave($__internal_e9bbc7f964915204a312035ac3a5dd95bb729bb95551135a08bdf5519378da2f_prof);

        
        $__internal_dd58440ae406b8d1bb3a52086b68a73115e983cab0d183f20edd0f943544226b->leave($__internal_dd58440ae406b8d1bb3a52086b68a73115e983cab0d183f20edd0f943544226b_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_a4ebf6213025bb88c1c1b0b1db172ba8e6ce9d2c249b303e4bdaac13fc84af3d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a4ebf6213025bb88c1c1b0b1db172ba8e6ce9d2c249b303e4bdaac13fc84af3d->enter($__internal_a4ebf6213025bb88c1c1b0b1db172ba8e6ce9d2c249b303e4bdaac13fc84af3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_84121ef87e56f6c19e43a004febd17cb103ed93dfcab17bf92772d9a1a121808 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_84121ef87e56f6c19e43a004febd17cb103ed93dfcab17bf92772d9a1a121808->enter($__internal_84121ef87e56f6c19e43a004febd17cb103ed93dfcab17bf92772d9a1a121808_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_84121ef87e56f6c19e43a004febd17cb103ed93dfcab17bf92772d9a1a121808->leave($__internal_84121ef87e56f6c19e43a004febd17cb103ed93dfcab17bf92772d9a1a121808_prof);

        
        $__internal_a4ebf6213025bb88c1c1b0b1db172ba8e6ce9d2c249b303e4bdaac13fc84af3d->leave($__internal_a4ebf6213025bb88c1c1b0b1db172ba8e6ce9d2c249b303e4bdaac13fc84af3d_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_4d7bc866d977a2059a173938358e45882c9fac7589c30eebb09f5cc484d33dd6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d7bc866d977a2059a173938358e45882c9fac7589c30eebb09f5cc484d33dd6->enter($__internal_4d7bc866d977a2059a173938358e45882c9fac7589c30eebb09f5cc484d33dd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_f5def4ead3023ecc89ba84a92c1c2884f275a3053d0a6327f5c4bfcd8c737b38 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5def4ead3023ecc89ba84a92c1c2884f275a3053d0a6327f5c4bfcd8c737b38->enter($__internal_f5def4ead3023ecc89ba84a92c1c2884f275a3053d0a6327f5c4bfcd8c737b38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_f5def4ead3023ecc89ba84a92c1c2884f275a3053d0a6327f5c4bfcd8c737b38->leave($__internal_f5def4ead3023ecc89ba84a92c1c2884f275a3053d0a6327f5c4bfcd8c737b38_prof);

        
        $__internal_4d7bc866d977a2059a173938358e45882c9fac7589c30eebb09f5cc484d33dd6->leave($__internal_4d7bc866d977a2059a173938358e45882c9fac7589c30eebb09f5cc484d33dd6_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_2bff2dd63c75df51a0ec3722d4a5c349d48157ff8a3a1f87ab99d1e29a274c7d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2bff2dd63c75df51a0ec3722d4a5c349d48157ff8a3a1f87ab99d1e29a274c7d->enter($__internal_2bff2dd63c75df51a0ec3722d4a5c349d48157ff8a3a1f87ab99d1e29a274c7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_c86ddc4915a6f219a05889f9fd0793ae6b4ef1355e38b9a1ee7fdc64a5e09782 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c86ddc4915a6f219a05889f9fd0793ae6b4ef1355e38b9a1ee7fdc64a5e09782->enter($__internal_c86ddc4915a6f219a05889f9fd0793ae6b4ef1355e38b9a1ee7fdc64a5e09782_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_c86ddc4915a6f219a05889f9fd0793ae6b4ef1355e38b9a1ee7fdc64a5e09782->leave($__internal_c86ddc4915a6f219a05889f9fd0793ae6b4ef1355e38b9a1ee7fdc64a5e09782_prof);

        
        $__internal_2bff2dd63c75df51a0ec3722d4a5c349d48157ff8a3a1f87ab99d1e29a274c7d->leave($__internal_2bff2dd63c75df51a0ec3722d4a5c349d48157ff8a3a1f87ab99d1e29a274c7d_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/Users/matt/Prog/Symfo/Mymfo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
