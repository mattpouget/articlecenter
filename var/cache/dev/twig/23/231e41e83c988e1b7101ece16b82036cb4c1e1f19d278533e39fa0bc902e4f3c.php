<?php

/* article/new.html.twig */
class __TwigTemplate_62035d8df220a2db3911d800b62e98ad921f804e89947bebb07631e7dee2b747 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "article/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_21fdce6b80b50e195d2cfd33cea0e0c1166c228d2aa85917d1763089e32b8a77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21fdce6b80b50e195d2cfd33cea0e0c1166c228d2aa85917d1763089e32b8a77->enter($__internal_21fdce6b80b50e195d2cfd33cea0e0c1166c228d2aa85917d1763089e32b8a77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "article/new.html.twig"));

        $__internal_9e88626ce7788ad1d1934b791f54affc2fc21323bf48bb685119956507dddf73 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e88626ce7788ad1d1934b791f54affc2fc21323bf48bb685119956507dddf73->enter($__internal_9e88626ce7788ad1d1934b791f54affc2fc21323bf48bb685119956507dddf73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "article/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_21fdce6b80b50e195d2cfd33cea0e0c1166c228d2aa85917d1763089e32b8a77->leave($__internal_21fdce6b80b50e195d2cfd33cea0e0c1166c228d2aa85917d1763089e32b8a77_prof);

        
        $__internal_9e88626ce7788ad1d1934b791f54affc2fc21323bf48bb685119956507dddf73->leave($__internal_9e88626ce7788ad1d1934b791f54affc2fc21323bf48bb685119956507dddf73_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_ee6ecef197f91da79b1a523ee892aee1bff38772cd408a7145cc06f6ff70907b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ee6ecef197f91da79b1a523ee892aee1bff38772cd408a7145cc06f6ff70907b->enter($__internal_ee6ecef197f91da79b1a523ee892aee1bff38772cd408a7145cc06f6ff70907b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_7a96b73f8549b7dbb8033487e8a2fd182f8112fa05d1e81584da3f081a7f3aa7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a96b73f8549b7dbb8033487e8a2fd182f8112fa05d1e81584da3f081a7f3aa7->enter($__internal_7a96b73f8549b7dbb8033487e8a2fd182f8112fa05d1e81584da3f081a7f3aa7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.min.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>Article creation</h1>

    ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 30
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" class=\"btn btn-info\"/>
    ";
        // line 32
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    <a href=\"";
        // line 33
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("artcrud_index");
        echo "\" class=\"btn btn-default\">Back to the list</a>
";
        
        $__internal_7a96b73f8549b7dbb8033487e8a2fd182f8112fa05d1e81584da3f081a7f3aa7->leave($__internal_7a96b73f8549b7dbb8033487e8a2fd182f8112fa05d1e81584da3f081a7f3aa7_prof);

        
        $__internal_ee6ecef197f91da79b1a523ee892aee1bff38772cd408a7145cc06f6ff70907b->leave($__internal_ee6ecef197f91da79b1a523ee892aee1bff38772cd408a7145cc06f6ff70907b_prof);

    }

    public function getTemplateName()
    {
        return "article/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 33,  88 => 32,  83 => 30,  79 => 29,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.min.css') }}\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>Article creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" class=\"btn btn-info\"/>
    {{ form_end(form) }}
    <a href=\"{{ path('artcrud_index') }}\" class=\"btn btn-default\">Back to the list</a>
{% endblock %}
", "article/new.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/article/new.html.twig");
    }
}
