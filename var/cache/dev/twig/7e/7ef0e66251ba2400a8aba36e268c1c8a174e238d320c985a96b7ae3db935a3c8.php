<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_63f91bb82c231c74887f58b71729975a2649838c828846a69c382a4c1ce93be0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a0b5b88832455b3e81673ed0b80a87b90706f6d96ccd9a2e6eca6499dfb0a3d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a0b5b88832455b3e81673ed0b80a87b90706f6d96ccd9a2e6eca6499dfb0a3d1->enter($__internal_a0b5b88832455b3e81673ed0b80a87b90706f6d96ccd9a2e6eca6499dfb0a3d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_a8e4db82952cc7edfd9ec0a54537736ef8cec56e1585e14bc0dc687cf422dce7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a8e4db82952cc7edfd9ec0a54537736ef8cec56e1585e14bc0dc687cf422dce7->enter($__internal_a8e4db82952cc7edfd9ec0a54537736ef8cec56e1585e14bc0dc687cf422dce7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a0b5b88832455b3e81673ed0b80a87b90706f6d96ccd9a2e6eca6499dfb0a3d1->leave($__internal_a0b5b88832455b3e81673ed0b80a87b90706f6d96ccd9a2e6eca6499dfb0a3d1_prof);

        
        $__internal_a8e4db82952cc7edfd9ec0a54537736ef8cec56e1585e14bc0dc687cf422dce7->leave($__internal_a8e4db82952cc7edfd9ec0a54537736ef8cec56e1585e14bc0dc687cf422dce7_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_889d269ed5acefb4ffc77796ae11b39505fbbb81f26d8dd98eed7e851d7d0a8e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_889d269ed5acefb4ffc77796ae11b39505fbbb81f26d8dd98eed7e851d7d0a8e->enter($__internal_889d269ed5acefb4ffc77796ae11b39505fbbb81f26d8dd98eed7e851d7d0a8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_9790f66e60a0a60633c4548dadf1c245228cdcad57a48c5b98e04741c6c8e457 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9790f66e60a0a60633c4548dadf1c245228cdcad57a48c5b98e04741c6c8e457->enter($__internal_9790f66e60a0a60633c4548dadf1c245228cdcad57a48c5b98e04741c6c8e457_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_9790f66e60a0a60633c4548dadf1c245228cdcad57a48c5b98e04741c6c8e457->leave($__internal_9790f66e60a0a60633c4548dadf1c245228cdcad57a48c5b98e04741c6c8e457_prof);

        
        $__internal_889d269ed5acefb4ffc77796ae11b39505fbbb81f26d8dd98eed7e851d7d0a8e->leave($__internal_889d269ed5acefb4ffc77796ae11b39505fbbb81f26d8dd98eed7e851d7d0a8e_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "/Users/matt/Prog/Symfo/Mymfo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
