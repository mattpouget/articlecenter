<?php

/* category/edit.html.twig */
class __TwigTemplate_c60d398374a9d5caef9744546a170c7713749568db5d7b90ade04b3bb9725eb5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "category/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_407194415db7e98068ac06aac2f12fc9bad781bdb1d3678e6776fbaa2c772c38 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_407194415db7e98068ac06aac2f12fc9bad781bdb1d3678e6776fbaa2c772c38->enter($__internal_407194415db7e98068ac06aac2f12fc9bad781bdb1d3678e6776fbaa2c772c38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "category/edit.html.twig"));

        $__internal_e359d7f0cd9956f6d56e491481aca2297b91c2cefac4b5a4f4ac5c6c32777471 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e359d7f0cd9956f6d56e491481aca2297b91c2cefac4b5a4f4ac5c6c32777471->enter($__internal_e359d7f0cd9956f6d56e491481aca2297b91c2cefac4b5a4f4ac5c6c32777471_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "category/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_407194415db7e98068ac06aac2f12fc9bad781bdb1d3678e6776fbaa2c772c38->leave($__internal_407194415db7e98068ac06aac2f12fc9bad781bdb1d3678e6776fbaa2c772c38_prof);

        
        $__internal_e359d7f0cd9956f6d56e491481aca2297b91c2cefac4b5a4f4ac5c6c32777471->leave($__internal_e359d7f0cd9956f6d56e491481aca2297b91c2cefac4b5a4f4ac5c6c32777471_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_5a65ef54d87124035aa5188a12635d7b923d99bc56f3dcb9583ef397b3bd6c3e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a65ef54d87124035aa5188a12635d7b923d99bc56f3dcb9583ef397b3bd6c3e->enter($__internal_5a65ef54d87124035aa5188a12635d7b923d99bc56f3dcb9583ef397b3bd6c3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_944c5d95461f86b0a3aa4b541effaad4cb7faa2380b0b431bd2c3d8d0d588591 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_944c5d95461f86b0a3aa4b541effaad4cb7faa2380b0b431bd2c3d8d0d588591->enter($__internal_944c5d95461f86b0a3aa4b541effaad4cb7faa2380b0b431bd2c3d8d0d588591_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.min.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>

    <h1>Category edit</h1>

    ";
        // line 10
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 11
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" class=\"btn btn-warning\"/>
    ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "
    <a href=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("catcrud_index");
        echo "\" class=\"btn btn-default\">Back to the list</a>
    ";
        // line 15
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
        <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
    ";
        // line 17
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
";
        
        $__internal_944c5d95461f86b0a3aa4b541effaad4cb7faa2380b0b431bd2c3d8d0d588591->leave($__internal_944c5d95461f86b0a3aa4b541effaad4cb7faa2380b0b431bd2c3d8d0d588591_prof);

        
        $__internal_5a65ef54d87124035aa5188a12635d7b923d99bc56f3dcb9583ef397b3bd6c3e->leave($__internal_5a65ef54d87124035aa5188a12635d7b923d99bc56f3dcb9583ef397b3bd6c3e_prof);

    }

    public function getTemplateName()
    {
        return "category/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 17,  77 => 15,  73 => 14,  69 => 13,  64 => 11,  60 => 10,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.min.css') }}\" crossorigin=\"anonymous\">
    </head>

    <h1>Category edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" class=\"btn btn-warning\"/>
    {{ form_end(edit_form) }}
    <a href=\"{{ path('catcrud_index') }}\" class=\"btn btn-default\">Back to the list</a>
    {{ form_start(delete_form) }}
        <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
    {{ form_end(delete_form) }}
{% endblock %}
", "category/edit.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/category/edit.html.twig");
    }
}
