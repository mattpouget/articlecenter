<?php

/* genus/show.html.twig */
class __TwigTemplate_a08a1f2f3f7fe5975b81a2059df706cd78868bdf629b8ec5ea488320509ce7c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "genus/show.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dd153d8ca783ed4f739d92fb42e9a964706558d5dc23217416b12c4ca56cde74 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dd153d8ca783ed4f739d92fb42e9a964706558d5dc23217416b12c4ca56cde74->enter($__internal_dd153d8ca783ed4f739d92fb42e9a964706558d5dc23217416b12c4ca56cde74_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/show.html.twig"));

        $__internal_0d2e0713c9fe8b88308e3da0ebe18891a1a60f2f92af4e53e2a534325f4496f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0d2e0713c9fe8b88308e3da0ebe18891a1a60f2f92af4e53e2a534325f4496f4->enter($__internal_0d2e0713c9fe8b88308e3da0ebe18891a1a60f2f92af4e53e2a534325f4496f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dd153d8ca783ed4f739d92fb42e9a964706558d5dc23217416b12c4ca56cde74->leave($__internal_dd153d8ca783ed4f739d92fb42e9a964706558d5dc23217416b12c4ca56cde74_prof);

        
        $__internal_0d2e0713c9fe8b88308e3da0ebe18891a1a60f2f92af4e53e2a534325f4496f4->leave($__internal_0d2e0713c9fe8b88308e3da0ebe18891a1a60f2f92af4e53e2a534325f4496f4_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_cd88c12fd0391a4af22bde4fbbd061c00e348635aaa7e32771097d9e73c234e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cd88c12fd0391a4af22bde4fbbd061c00e348635aaa7e32771097d9e73c234e3->enter($__internal_cd88c12fd0391a4af22bde4fbbd061c00e348635aaa7e32771097d9e73c234e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_16e24184afe88c9cb72eadfe1b56b63606c1e2f7f0a80fb6e67f254fcc48e198 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_16e24184afe88c9cb72eadfe1b56b63606c1e2f7f0a80fb6e67f254fcc48e198->enter($__internal_16e24184afe88c9cb72eadfe1b56b63606c1e2f7f0a80fb6e67f254fcc48e198_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Article Center";
        
        $__internal_16e24184afe88c9cb72eadfe1b56b63606c1e2f7f0a80fb6e67f254fcc48e198->leave($__internal_16e24184afe88c9cb72eadfe1b56b63606c1e2f7f0a80fb6e67f254fcc48e198_prof);

        
        $__internal_cd88c12fd0391a4af22bde4fbbd061c00e348635aaa7e32771097d9e73c234e3->leave($__internal_cd88c12fd0391a4af22bde4fbbd061c00e348635aaa7e32771097d9e73c234e3_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_fb011f77ca802096b280d8d89310ba6cf0ed75711fc2c4964fa51dcaf600d5d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb011f77ca802096b280d8d89310ba6cf0ed75711fc2c4964fa51dcaf600d5d4->enter($__internal_fb011f77ca802096b280d8d89310ba6cf0ed75711fc2c4964fa51dcaf600d5d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_349c07c6053e04b43f7166b5e371cea296d1f1a91f01a44e26d9ce69a0e45a43 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_349c07c6053e04b43f7166b5e371cea296d1f1a91f01a44e26d9ce69a0e45a43->enter($__internal_349c07c6053e04b43f7166b5e371cea296d1f1a91f01a44e26d9ce69a0e45a43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>

    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"listcat\">Catégories</a></li>
                    ";
        // line 26
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 27
            echo "                        <li><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\" class=\"btn btn-danger\">Logout</a></li>
                    ";
        } else {
            // line 29
            echo "                        <li><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\" class=\"btn btn-success\">Login</a></li>
                    ";
        }
        // line 31
        echo "                    <li><a href=\"/register\" class=\"btn btn-info\">Sign up.</a></li>
                </ul>
                ";
        // line 33
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            // line 34
            echo "                <ul class=\"nav navbar-nav navbar-right\">
                    <li><a href=\"/admin\">AdminBoard</a></li>
                </ul>
                ";
        }
        // line 38
        echo "            </div>
        </div>
    </nav>



    <h2>Nouveaux Articles :</h2>
    <div class=\"jumbotron\">
        ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["articles"]) ? $context["articles"] : $this->getContext($context, "articles")));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 47
            echo "            <h3>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "name", array()), "html", null, true);
            echo "</h3>
            <blockquote>
                <small>Écrit le <cite>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["article"], "date", array()), "date", array()), "html", null, true);
            echo "</cite> par <cite>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "author", array()), "html", null, true);
            echo ".</cite></small>
                <img src=\"/images/";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["article"], "image", array()), "imageName", array()), "html", null, true);
            echo "\" width=\"50%\" height=\"50%\"><br>
                <a href=\"/article/";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary btn-xs\">Voir l'article &rarr;</a>
            </blockquote>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "    </div>

    <ul class=\"pagination\">
        <li class=\"disabled\"><a href=\"#\">&laquo;</a></li>
        <li class=\"active\"><a href=\"#\">1</a></li>
        <li><a href=\"#\">2</a></li>
        <li><a href=\"#\">3</a></li>
        <li><a href=\"#\">4</a></li>
        <li><a href=\"#\">5</a></li>
        <li><a href=\"#\">6</a></li>
        <li><a href=\"#\">7</a></li>
        <li><a href=\"#\">&raquo;</a></li>
    </ul>
";
        
        $__internal_349c07c6053e04b43f7166b5e371cea296d1f1a91f01a44e26d9ce69a0e45a43->leave($__internal_349c07c6053e04b43f7166b5e371cea296d1f1a91f01a44e26d9ce69a0e45a43_prof);

        
        $__internal_fb011f77ca802096b280d8d89310ba6cf0ed75711fc2c4964fa51dcaf600d5d4->leave($__internal_fb011f77ca802096b280d8d89310ba6cf0ed75711fc2c4964fa51dcaf600d5d4_prof);

    }

    public function getTemplateName()
    {
        return "genus/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 54,  150 => 51,  146 => 50,  140 => 49,  134 => 47,  130 => 46,  120 => 38,  114 => 34,  112 => 33,  108 => 31,  102 => 29,  96 => 27,  94 => 26,  72 => 7,  68 => 5,  59 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %}Article Center{% endblock %}
{% block body %}

    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.css') }}\" crossorigin=\"anonymous\">
    </head>

    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"listcat\">Catégories</a></li>
                    {% if is_granted('ROLE_USER') %}
                        <li><a href=\"{{ path('fos_user_security_logout') }}\" class=\"btn btn-danger\">Logout</a></li>
                    {% else %}
                        <li><a href=\"{{ path('fos_user_security_login') }}\" class=\"btn btn-success\">Login</a></li>
                    {% endif %}
                    <li><a href=\"/register\" class=\"btn btn-info\">Sign up.</a></li>
                </ul>
                {% if is_granted('ROLE_ADMIN') %}
                <ul class=\"nav navbar-nav navbar-right\">
                    <li><a href=\"/admin\">AdminBoard</a></li>
                </ul>
                {% endif %}
            </div>
        </div>
    </nav>



    <h2>Nouveaux Articles :</h2>
    <div class=\"jumbotron\">
        {% for article in articles %}
            <h3>{{ article.name }}</h3>
            <blockquote>
                <small>Écrit le <cite>{{ article.date.date }}</cite> par <cite>{{ article.author }}.</cite></small>
                <img src=\"/images/{{ article.image.imageName }}\" width=\"50%\" height=\"50%\"><br>
                <a href=\"/article/{{ article.id }}\" class=\"btn btn-primary btn-xs\">Voir l'article &rarr;</a>
            </blockquote>
        {% endfor %}
    </div>

    <ul class=\"pagination\">
        <li class=\"disabled\"><a href=\"#\">&laquo;</a></li>
        <li class=\"active\"><a href=\"#\">1</a></li>
        <li><a href=\"#\">2</a></li>
        <li><a href=\"#\">3</a></li>
        <li><a href=\"#\">4</a></li>
        <li><a href=\"#\">5</a></li>
        <li><a href=\"#\">6</a></li>
        <li><a href=\"#\">7</a></li>
        <li><a href=\"#\">&raquo;</a></li>
    </ul>
{% endblock %}", "genus/show.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/genus/show.html.twig");
    }
}
