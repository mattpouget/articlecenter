<?php

/* user/show.html.twig */
class __TwigTemplate_00e820b8c342f18fe20ff0dbfe0cc221280b2576c6b8b20b0371c34907c239b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "user/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7194fe1d68199601ccb4abdbee8efc26821a4a681e95d50b53db6ac554b36944 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7194fe1d68199601ccb4abdbee8efc26821a4a681e95d50b53db6ac554b36944->enter($__internal_7194fe1d68199601ccb4abdbee8efc26821a4a681e95d50b53db6ac554b36944_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "user/show.html.twig"));

        $__internal_7ef91e2e4e9589f440769209c0c98fe3be840200332e2a56796dbbed95b71619 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7ef91e2e4e9589f440769209c0c98fe3be840200332e2a56796dbbed95b71619->enter($__internal_7ef91e2e4e9589f440769209c0c98fe3be840200332e2a56796dbbed95b71619_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "user/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7194fe1d68199601ccb4abdbee8efc26821a4a681e95d50b53db6ac554b36944->leave($__internal_7194fe1d68199601ccb4abdbee8efc26821a4a681e95d50b53db6ac554b36944_prof);

        
        $__internal_7ef91e2e4e9589f440769209c0c98fe3be840200332e2a56796dbbed95b71619->leave($__internal_7ef91e2e4e9589f440769209c0c98fe3be840200332e2a56796dbbed95b71619_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_69248711c80e678f02c1f9474efe78f20a83387b8090ac599b38678b73e1d6bd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_69248711c80e678f02c1f9474efe78f20a83387b8090ac599b38678b73e1d6bd->enter($__internal_69248711c80e678f02c1f9474efe78f20a83387b8090ac599b38678b73e1d6bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_83c58341123f9a88294e550efc5aba63343252cafc29441a2121c1be7b796be5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_83c58341123f9a88294e550efc5aba63343252cafc29441a2121c1be7b796be5->enter($__internal_83c58341123f9a88294e550efc5aba63343252cafc29441a2121c1be7b796be5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.min.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>User</h1>

    <table>
        <tbody>
            <tr>
                <th class=\"text-warning\">Id</th>
                <th class=\"text-warning\">Name</th>
            </tr>
            <tr>
                <th>";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "id", array()), "html", null, true);
        echo "</th>
                <th>";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</th>
            </tr>
        </tbody>
    </table>
    <a href=\"";
        // line 41
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usercrud_index");
        echo "\" class=\"btn btn-default\">Back to the list</a>
    <a href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usercrud_edit", array("id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-default\">Edit</a>
    ";
        // line 43
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
        <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
    ";
        // line 45
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
";
        
        $__internal_83c58341123f9a88294e550efc5aba63343252cafc29441a2121c1be7b796be5->leave($__internal_83c58341123f9a88294e550efc5aba63343252cafc29441a2121c1be7b796be5_prof);

        
        $__internal_69248711c80e678f02c1f9474efe78f20a83387b8090ac599b38678b73e1d6bd->leave($__internal_69248711c80e678f02c1f9474efe78f20a83387b8090ac599b38678b73e1d6bd_prof);

    }

    public function getTemplateName()
    {
        return "user/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 45,  105 => 43,  101 => 42,  97 => 41,  90 => 37,  86 => 36,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.min.css') }}\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>User</h1>

    <table>
        <tbody>
            <tr>
                <th class=\"text-warning\">Id</th>
                <th class=\"text-warning\">Name</th>
            </tr>
            <tr>
                <th>{{ user.id }}</th>
                <th>{{ user.username }}</th>
            </tr>
        </tbody>
    </table>
    <a href=\"{{ path('usercrud_index') }}\" class=\"btn btn-default\">Back to the list</a>
    <a href=\"{{ path('usercrud_edit', { 'id': user.id }) }}\" class=\"btn btn-default\">Edit</a>
    {{ form_start(delete_form) }}
        <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
    {{ form_end(delete_form) }}
{% endblock %}
", "user/show.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/user/show.html.twig");
    }
}
