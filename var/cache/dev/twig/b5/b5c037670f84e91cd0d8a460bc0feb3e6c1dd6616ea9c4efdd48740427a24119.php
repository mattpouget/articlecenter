<?php

/* user/index.html.twig */
class __TwigTemplate_cbbaea9a420c414daf170f0b4c035102b0b36f875415f9b1c833257eb42dc186 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "user/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9dd28af85cd3d431c741022bb8604dc99b8c3313d9442f9f60d7fff4cc3559fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9dd28af85cd3d431c741022bb8604dc99b8c3313d9442f9f60d7fff4cc3559fe->enter($__internal_9dd28af85cd3d431c741022bb8604dc99b8c3313d9442f9f60d7fff4cc3559fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "user/index.html.twig"));

        $__internal_5d9562ad3ff34e2a1ba4813136a0dd3cf0ccdd186b59f43451a7ec9c691069f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d9562ad3ff34e2a1ba4813136a0dd3cf0ccdd186b59f43451a7ec9c691069f7->enter($__internal_5d9562ad3ff34e2a1ba4813136a0dd3cf0ccdd186b59f43451a7ec9c691069f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "user/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9dd28af85cd3d431c741022bb8604dc99b8c3313d9442f9f60d7fff4cc3559fe->leave($__internal_9dd28af85cd3d431c741022bb8604dc99b8c3313d9442f9f60d7fff4cc3559fe_prof);

        
        $__internal_5d9562ad3ff34e2a1ba4813136a0dd3cf0ccdd186b59f43451a7ec9c691069f7->leave($__internal_5d9562ad3ff34e2a1ba4813136a0dd3cf0ccdd186b59f43451a7ec9c691069f7_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_49aa77d8aae697eab6d47361cf47d61ee95027d46c3c6acba34550f5a90b4a0b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_49aa77d8aae697eab6d47361cf47d61ee95027d46c3c6acba34550f5a90b4a0b->enter($__internal_49aa77d8aae697eab6d47361cf47d61ee95027d46c3c6acba34550f5a90b4a0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ae60ce36fa20cc0bf27cb2e98725ddbce0da6c240c2f30cd3544c9f505f3dc3c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae60ce36fa20cc0bf27cb2e98725ddbce0da6c240c2f30cd3544c9f505f3dc3c->enter($__internal_ae60ce36fa20cc0bf27cb2e98725ddbce0da6c240c2f30cd3544c9f505f3dc3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.min.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>Users list</h1>

    <table>
        <thead>
            <tr>
                <th class=\"text-warning\">Id</th>
                <th class=\"text-warning\">Name</th>
                <th class=\"text-warning\">Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 38
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 39
            echo "            <tr>
                <td><a href=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usercrud_show", array("id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "username", array()), "html", null, true);
            echo "</td>

                <td>
                    <a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usercrud_show", array("id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default\">show</a>
                    <a href=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usercrud_edit", array("id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default\">edit</a>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "        </tbody>
    </table>
    <a href=\"";
        // line 51
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usercrud_new");
        echo "\" class=\"btn btn-default\">Create a new user</a>
";
        
        $__internal_ae60ce36fa20cc0bf27cb2e98725ddbce0da6c240c2f30cd3544c9f505f3dc3c->leave($__internal_ae60ce36fa20cc0bf27cb2e98725ddbce0da6c240c2f30cd3544c9f505f3dc3c_prof);

        
        $__internal_49aa77d8aae697eab6d47361cf47d61ee95027d46c3c6acba34550f5a90b4a0b->leave($__internal_49aa77d8aae697eab6d47361cf47d61ee95027d46c3c6acba34550f5a90b4a0b_prof);

    }

    public function getTemplateName()
    {
        return "user/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 51,  121 => 49,  111 => 45,  107 => 44,  101 => 41,  95 => 40,  92 => 39,  88 => 38,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.min.css') }}\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>Users list</h1>

    <table>
        <thead>
            <tr>
                <th class=\"text-warning\">Id</th>
                <th class=\"text-warning\">Name</th>
                <th class=\"text-warning\">Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for user in users %}
            <tr>
                <td><a href=\"{{ path('usercrud_show', { 'id': user.id }) }}\">{{ user.id }}</a></td>
                <td>{{ user.username }}</td>

                <td>
                    <a href=\"{{ path('usercrud_show', { 'id': user.id }) }}\" class=\"btn btn-default\">show</a>
                    <a href=\"{{ path('usercrud_edit', { 'id': user.id }) }}\" class=\"btn btn-default\">edit</a>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
    <a href=\"{{ path('usercrud_new') }}\" class=\"btn btn-default\">Create a new user</a>
{% endblock %}
", "user/index.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/user/index.html.twig");
    }
}
