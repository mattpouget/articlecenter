<?php

/* article/show.html.twig */
class __TwigTemplate_84b2e4d861917773536a5d545e92f357788ae1013bab0d72a89189d9b23e2f40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "article/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_20727ff71297810982aeee1168a058f2efe2f2d7aa9d05f4643e08ff040e28cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_20727ff71297810982aeee1168a058f2efe2f2d7aa9d05f4643e08ff040e28cb->enter($__internal_20727ff71297810982aeee1168a058f2efe2f2d7aa9d05f4643e08ff040e28cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "article/show.html.twig"));

        $__internal_937dfef4a3ccbe7c54c10fa0250e172fc46d1ce30703efc28545de00a9671ad1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_937dfef4a3ccbe7c54c10fa0250e172fc46d1ce30703efc28545de00a9671ad1->enter($__internal_937dfef4a3ccbe7c54c10fa0250e172fc46d1ce30703efc28545de00a9671ad1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "article/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_20727ff71297810982aeee1168a058f2efe2f2d7aa9d05f4643e08ff040e28cb->leave($__internal_20727ff71297810982aeee1168a058f2efe2f2d7aa9d05f4643e08ff040e28cb_prof);

        
        $__internal_937dfef4a3ccbe7c54c10fa0250e172fc46d1ce30703efc28545de00a9671ad1->leave($__internal_937dfef4a3ccbe7c54c10fa0250e172fc46d1ce30703efc28545de00a9671ad1_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_757899ca295e95a63e728a5cf7fd54c0dd891137f6d88935b0f62d241fb651e1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_757899ca295e95a63e728a5cf7fd54c0dd891137f6d88935b0f62d241fb651e1->enter($__internal_757899ca295e95a63e728a5cf7fd54c0dd891137f6d88935b0f62d241fb651e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_36b2f964604a2cfb502648d11f96108d6004ed3ff665e200d89bbac5a5582a8b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36b2f964604a2cfb502648d11f96108d6004ed3ff665e200d89bbac5a5582a8b->enter($__internal_36b2f964604a2cfb502648d11f96108d6004ed3ff665e200d89bbac5a5582a8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.min.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>Article</h1>

    <table>
        <tbody>
            <tr>
                <th class=\"text-warning\">Id</th>
                <td>";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th class=\"text-warning\">Name</th>
                <td>";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "name", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th class=\"text-warning\">Content</th>
                <td>";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "content", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th class=\"text-warning\">Author</th>
                <td>";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "author", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th class=\"text-warning\">Date</th>
                <td>";
        // line 49
        if ($this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "date", array())) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "date", array()), "Y-m-d H:i:s"), "html", null, true);
        }
        echo "</td>
            </tr>
            <tr>
                <th class=\"text-warning\">Category</th>
                <td>";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "category", array()), "name", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>
            <a href=\"";
        // line 57
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("artcrud_index");
        echo "\" class=\"btn btn-default\">Back to the list</a>

            <a href=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("artcrud_edit", array("id" => $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-default\">Edit</a>

            ";
        // line 61
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            ";
        // line 63
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "

";
        
        $__internal_36b2f964604a2cfb502648d11f96108d6004ed3ff665e200d89bbac5a5582a8b->leave($__internal_36b2f964604a2cfb502648d11f96108d6004ed3ff665e200d89bbac5a5582a8b_prof);

        
        $__internal_757899ca295e95a63e728a5cf7fd54c0dd891137f6d88935b0f62d241fb651e1->leave($__internal_757899ca295e95a63e728a5cf7fd54c0dd891137f6d88935b0f62d241fb651e1_prof);

    }

    public function getTemplateName()
    {
        return "article/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 63,  137 => 61,  132 => 59,  127 => 57,  120 => 53,  111 => 49,  104 => 45,  97 => 41,  90 => 37,  83 => 33,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.min.css') }}\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>Article</h1>

    <table>
        <tbody>
            <tr>
                <th class=\"text-warning\">Id</th>
                <td>{{ article.id }}</td>
            </tr>
            <tr>
                <th class=\"text-warning\">Name</th>
                <td>{{ article.name }}</td>
            </tr>
            <tr>
                <th class=\"text-warning\">Content</th>
                <td>{{ article.content }}</td>
            </tr>
            <tr>
                <th class=\"text-warning\">Author</th>
                <td>{{ article.author }}</td>
            </tr>
            <tr>
                <th class=\"text-warning\">Date</th>
                <td>{% if article.date %}{{ article.date|date('Y-m-d H:i:s') }}{% endif %}</td>
            </tr>
            <tr>
                <th class=\"text-warning\">Category</th>
                <td>{{ article.category.name }}</td>
            </tr>
        </tbody>
    </table>
            <a href=\"{{ path('artcrud_index') }}\" class=\"btn btn-default\">Back to the list</a>

            <a href=\"{{ path('artcrud_edit', { 'id': article.id }) }}\" class=\"btn btn-default\">Edit</a>

            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            {{ form_end(delete_form) }}

{% endblock %}
", "article/show.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/article/show.html.twig");
    }
}
