<?php

/* genus/add.html.twig */
class __TwigTemplate_50591412420c50bf63ba01d0d2e555cfbef30eec026922d483be33677e26c69d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "genus/add.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c572317cc5b00ec64a85309998ef5b482b29aed1dfb2448a6e5f8c937fcf28d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c572317cc5b00ec64a85309998ef5b482b29aed1dfb2448a6e5f8c937fcf28d1->enter($__internal_c572317cc5b00ec64a85309998ef5b482b29aed1dfb2448a6e5f8c937fcf28d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/add.html.twig"));

        $__internal_d271d27757c5b3207ae987383c0a93d85f286e4a00c54c1b3c9ae45ceb60ce92 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d271d27757c5b3207ae987383c0a93d85f286e4a00c54c1b3c9ae45ceb60ce92->enter($__internal_d271d27757c5b3207ae987383c0a93d85f286e4a00c54c1b3c9ae45ceb60ce92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/add.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c572317cc5b00ec64a85309998ef5b482b29aed1dfb2448a6e5f8c937fcf28d1->leave($__internal_c572317cc5b00ec64a85309998ef5b482b29aed1dfb2448a6e5f8c937fcf28d1_prof);

        
        $__internal_d271d27757c5b3207ae987383c0a93d85f286e4a00c54c1b3c9ae45ceb60ce92->leave($__internal_d271d27757c5b3207ae987383c0a93d85f286e4a00c54c1b3c9ae45ceb60ce92_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_50546e78cf60ed787ff9c7c3481299459ba06dcefaf4714c715e3dc6a711bef4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_50546e78cf60ed787ff9c7c3481299459ba06dcefaf4714c715e3dc6a711bef4->enter($__internal_50546e78cf60ed787ff9c7c3481299459ba06dcefaf4714c715e3dc6a711bef4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_12dfb40c491fc8647af62e48467676c22876b8da1b2b73b0e7c517da70cc4c12 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_12dfb40c491fc8647af62e48467676c22876b8da1b2b73b0e7c517da70cc4c12->enter($__internal_12dfb40c491fc8647af62e48467676c22876b8da1b2b73b0e7c517da70cc4c12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Article Center";
        
        $__internal_12dfb40c491fc8647af62e48467676c22876b8da1b2b73b0e7c517da70cc4c12->leave($__internal_12dfb40c491fc8647af62e48467676c22876b8da1b2b73b0e7c517da70cc4c12_prof);

        
        $__internal_50546e78cf60ed787ff9c7c3481299459ba06dcefaf4714c715e3dc6a711bef4->leave($__internal_50546e78cf60ed787ff9c7c3481299459ba06dcefaf4714c715e3dc6a711bef4_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_cb6090a3473b25eb1ce560c978e74082f748b629cf7a43582db3f22fcd523b53 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cb6090a3473b25eb1ce560c978e74082f748b629cf7a43582db3f22fcd523b53->enter($__internal_cb6090a3473b25eb1ce560c978e74082f748b629cf7a43582db3f22fcd523b53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ab97ea42d2ab4c587dcf84593de646029c53b7a30220c0f6720f1876530cc086 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab97ea42d2ab4c587dcf84593de646029c53b7a30220c0f6720f1876530cc086->enter($__internal_ab97ea42d2ab4c587dcf84593de646029c53b7a30220c0f6720f1876530cc086_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"listcat\">Catégories</a></li>
                    <li class=\"dropdown\">
                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">Dropdown <span class=\"caret\"></span></a>
                        <ul class=\"dropdown-menu\" role=\"menu\">
                            <li><a href=\"#\">Action</a></li>
                            <li><a href=\"#\">Another action</a></li>
                            <li><a href=\"#\">Something else here</a></li>
                            <li class=\"divider\"></li>
                            <li><a href=\"#\">Separated link</a></li>
                            <li class=\"divider\"></li>
                            <li><a href=\"#\">One more separated link</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

        <h1>AddArticle :</h1>
        ";
        // line 43
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 44
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'label');
        echo "
        ";
        // line 45
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'widget');
        echo "
        <br>
        ";
        // line 47
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "content", array()), 'label');
        echo "
        ";
        // line 48
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "content", array()), 'widget');
        echo "
        <br>
        ";
        // line 50
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date", array()), 'label');
        echo "
        ";
        // line 51
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date", array()), 'widget');
        echo "
        <br>
        ";
        // line 53
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "author", array()), 'label');
        echo "
        ";
        // line 54
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "author", array()), 'widget');
        echo "
        <br>
        ";
        // line 56
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "image", array()), 'label');
        echo "
        ";
        // line 57
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "image", array()), 'widget');
        echo "
        <br>
        ";
        // line 59
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "category", array()), 'label');
        echo "
        ";
        // line 60
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "category", array()), 'widget');
        echo "
        <br>
        ";
        // line 62
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_ab97ea42d2ab4c587dcf84593de646029c53b7a30220c0f6720f1876530cc086->leave($__internal_ab97ea42d2ab4c587dcf84593de646029c53b7a30220c0f6720f1876530cc086_prof);

        
        $__internal_cb6090a3473b25eb1ce560c978e74082f748b629cf7a43582db3f22fcd523b53->leave($__internal_cb6090a3473b25eb1ce560c978e74082f748b629cf7a43582db3f22fcd523b53_prof);

    }

    public function getTemplateName()
    {
        return "genus/add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 62,  163 => 60,  159 => 59,  154 => 57,  150 => 56,  145 => 54,  141 => 53,  136 => 51,  132 => 50,  127 => 48,  123 => 47,  118 => 45,  114 => 44,  110 => 43,  71 => 7,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %}Article Center{% endblock %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.css') }}\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"listcat\">Catégories</a></li>
                    <li class=\"dropdown\">
                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">Dropdown <span class=\"caret\"></span></a>
                        <ul class=\"dropdown-menu\" role=\"menu\">
                            <li><a href=\"#\">Action</a></li>
                            <li><a href=\"#\">Another action</a></li>
                            <li><a href=\"#\">Something else here</a></li>
                            <li class=\"divider\"></li>
                            <li><a href=\"#\">Separated link</a></li>
                            <li class=\"divider\"></li>
                            <li><a href=\"#\">One more separated link</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

        <h1>AddArticle :</h1>
        {{ form_start(form) }}
        {{ form_label(form.name) }}
        {{ form_widget(form.name) }}
        <br>
        {{ form_label(form.content) }}
        {{ form_widget(form.content) }}
        <br>
        {{ form_label(form.date) }}
        {{ form_widget(form.date) }}
        <br>
        {{ form_label(form.author) }}
        {{ form_widget(form.author) }}
        <br>
        {{ form_label(form.image) }}
        {{ form_widget(form.image) }}
        <br>
        {{ form_label(form.category) }}
        {{ form_widget(form.category) }}
        <br>
        {{ form_end(form) }}
{% endblock %}", "genus/add.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/genus/add.html.twig");
    }
}
