<?php

/* genus/addcat.html.twig */
class __TwigTemplate_f5f4e0b082b4d37dc23c6d3e1a9647920ec681142aad886d1f785ec7a8ca6140 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "genus/addcat.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_193eddadc2a98fe8eb4133c45c173dadcfe3cd59866b8b7b6adb9aaabd990328 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_193eddadc2a98fe8eb4133c45c173dadcfe3cd59866b8b7b6adb9aaabd990328->enter($__internal_193eddadc2a98fe8eb4133c45c173dadcfe3cd59866b8b7b6adb9aaabd990328_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/addcat.html.twig"));

        $__internal_2da2824318ef91e8ac90f7cf4101f26b9b3014aff366fa4cae893031bbdabcf1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2da2824318ef91e8ac90f7cf4101f26b9b3014aff366fa4cae893031bbdabcf1->enter($__internal_2da2824318ef91e8ac90f7cf4101f26b9b3014aff366fa4cae893031bbdabcf1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/addcat.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_193eddadc2a98fe8eb4133c45c173dadcfe3cd59866b8b7b6adb9aaabd990328->leave($__internal_193eddadc2a98fe8eb4133c45c173dadcfe3cd59866b8b7b6adb9aaabd990328_prof);

        
        $__internal_2da2824318ef91e8ac90f7cf4101f26b9b3014aff366fa4cae893031bbdabcf1->leave($__internal_2da2824318ef91e8ac90f7cf4101f26b9b3014aff366fa4cae893031bbdabcf1_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_8a30d9d1d69c5636eb2835df36798c56dabe723375109a2e494b9e2b5c5d302d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8a30d9d1d69c5636eb2835df36798c56dabe723375109a2e494b9e2b5c5d302d->enter($__internal_8a30d9d1d69c5636eb2835df36798c56dabe723375109a2e494b9e2b5c5d302d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_298d395a45a650daa77ae975a82dc096797ae14329e0053aa7f32582138ee400 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_298d395a45a650daa77ae975a82dc096797ae14329e0053aa7f32582138ee400->enter($__internal_298d395a45a650daa77ae975a82dc096797ae14329e0053aa7f32582138ee400_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Article Center";
        
        $__internal_298d395a45a650daa77ae975a82dc096797ae14329e0053aa7f32582138ee400->leave($__internal_298d395a45a650daa77ae975a82dc096797ae14329e0053aa7f32582138ee400_prof);

        
        $__internal_8a30d9d1d69c5636eb2835df36798c56dabe723375109a2e494b9e2b5c5d302d->leave($__internal_8a30d9d1d69c5636eb2835df36798c56dabe723375109a2e494b9e2b5c5d302d_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_c189c4dc7590256b8e7e2c0565ba47bed08a672339a947831307cd7e96c5a6f5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c189c4dc7590256b8e7e2c0565ba47bed08a672339a947831307cd7e96c5a6f5->enter($__internal_c189c4dc7590256b8e7e2c0565ba47bed08a672339a947831307cd7e96c5a6f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_3c374c86f475b45699cb7f7d247e2b5e663396e1bc55dbab20dd1144e685c6bb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c374c86f475b45699cb7f7d247e2b5e663396e1bc55dbab20dd1144e685c6bb->enter($__internal_3c374c86f475b45699cb7f7d247e2b5e663396e1bc55dbab20dd1144e685c6bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <h1>AddCatégories :</h1>
    ";
        // line 32
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 33
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    ";
        // line 34
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    <h2>Voulez vous lire un Article ?</h2>
    <a href='/'>Pourquoi pas</a>

";
        
        $__internal_3c374c86f475b45699cb7f7d247e2b5e663396e1bc55dbab20dd1144e685c6bb->leave($__internal_3c374c86f475b45699cb7f7d247e2b5e663396e1bc55dbab20dd1144e685c6bb_prof);

        
        $__internal_c189c4dc7590256b8e7e2c0565ba47bed08a672339a947831307cd7e96c5a6f5->leave($__internal_c189c4dc7590256b8e7e2c0565ba47bed08a672339a947831307cd7e96c5a6f5_prof);

    }

    public function getTemplateName()
    {
        return "genus/addcat.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 34,  103 => 33,  99 => 32,  71 => 7,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %}Article Center{% endblock %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.css') }}\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <h1>AddCatégories :</h1>
    {{ form_start(form) }}
    {{ form_widget(form) }}
    {{ form_end(form) }}

    <h2>Voulez vous lire un Article ?</h2>
    <a href='/'>Pourquoi pas</a>

{% endblock %}", "genus/addcat.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/genus/addcat.html.twig");
    }
}
