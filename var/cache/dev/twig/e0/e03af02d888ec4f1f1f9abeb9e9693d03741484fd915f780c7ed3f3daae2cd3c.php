<?php

/* BraincraftedBootstrapBundle:Form:bootstrap.html.twig */
class __TwigTemplate_b8c942eaa82e4ea158c2c72970a01cfd57b138816ca6df5ce04da2ea26501493 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("form_div_layout.html.twig", "BraincraftedBootstrapBundle:Form:bootstrap.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."form_div_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_widget' => array($this, 'block_form_widget'),
                'form_widget_simple' => array($this, 'block_form_widget_simple'),
                'form_widget_compound' => array($this, 'block_form_widget_compound'),
                'collection_widget' => array($this, 'block_collection_widget'),
                'bootstrap_collection_widget' => array($this, 'block_bootstrap_collection_widget'),
                'textarea_widget' => array($this, 'block_textarea_widget'),
                'file_widget' => array($this, 'block_file_widget'),
                'choice_widget' => array($this, 'block_choice_widget'),
                'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
                'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
                'choice_widget_options' => array($this, 'block_choice_widget_options'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
                'radio_row' => array($this, 'block_radio_row'),
                'checkbox_widget' => array($this, 'block_checkbox_widget'),
                'radio_widget' => array($this, 'block_radio_widget'),
                'datetime_widget' => array($this, 'block_datetime_widget'),
                'date_widget' => array($this, 'block_date_widget'),
                'time_widget' => array($this, 'block_time_widget'),
                'number_widget' => array($this, 'block_number_widget'),
                'integer_widget' => array($this, 'block_integer_widget'),
                'money_widget' => array($this, 'block_money_widget'),
                'url_widget' => array($this, 'block_url_widget'),
                'search_widget' => array($this, 'block_search_widget'),
                'percent_widget' => array($this, 'block_percent_widget'),
                'password_widget' => array($this, 'block_password_widget'),
                'hidden_widget' => array($this, 'block_hidden_widget'),
                'email_widget' => array($this, 'block_email_widget'),
                'button_widget' => array($this, 'block_button_widget'),
                'submit_widget' => array($this, 'block_submit_widget'),
                'reset_widget' => array($this, 'block_reset_widget'),
                'form_actions_widget' => array($this, 'block_form_actions_widget'),
                'bs_static_widget' => array($this, 'block_bs_static_widget'),
                'form_label' => array($this, 'block_form_label'),
                'button_label' => array($this, 'block_button_label'),
                'repeated_row' => array($this, 'block_repeated_row'),
                'form_row' => array($this, 'block_form_row'),
                'form_input_group' => array($this, 'block_form_input_group'),
                'form_help' => array($this, 'block_form_help'),
                'button_row' => array($this, 'block_button_row'),
                'hidden_row' => array($this, 'block_hidden_row'),
                'form_actions_row' => array($this, 'block_form_actions_row'),
                'form' => array($this, 'block_form'),
                'form_start' => array($this, 'block_form_start'),
                'form_end' => array($this, 'block_form_end'),
                'form_enctype' => array($this, 'block_form_enctype'),
                'global_form_errors' => array($this, 'block_global_form_errors'),
                'form_errors' => array($this, 'block_form_errors'),
                'form_rest' => array($this, 'block_form_rest'),
                'form_rows' => array($this, 'block_form_rows'),
                'widget_attributes' => array($this, 'block_widget_attributes'),
                'widget_container_attributes' => array($this, 'block_widget_container_attributes'),
                'button_attributes' => array($this, 'block_button_attributes'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b817f5a64717d9a4338caa2cf48bcdd57021dab1bd384ced66da290d347471c0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b817f5a64717d9a4338caa2cf48bcdd57021dab1bd384ced66da290d347471c0->enter($__internal_b817f5a64717d9a4338caa2cf48bcdd57021dab1bd384ced66da290d347471c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "BraincraftedBootstrapBundle:Form:bootstrap.html.twig"));

        $__internal_1ad93d89f0b238b952eab1d56e004384710ac01b3fda02cf57e71f52b4723e8c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1ad93d89f0b238b952eab1d56e004384710ac01b3fda02cf57e71f52b4723e8c->enter($__internal_1ad93d89f0b238b952eab1d56e004384710ac01b3fda02cf57e71f52b4723e8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "BraincraftedBootstrapBundle:Form:bootstrap.html.twig"));

        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('form_widget', $context, $blocks);
        // line 14
        echo "
";
        // line 15
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 59
        echo "
";
        // line 60
        $this->displayBlock('form_widget_compound', $context, $blocks);
        // line 71
        echo "
";
        // line 72
        $this->displayBlock('collection_widget', $context, $blocks);
        // line 80
        echo "
";
        // line 81
        $this->displayBlock('bootstrap_collection_widget', $context, $blocks);
        // line 121
        echo "
";
        // line 122
        $this->displayBlock('textarea_widget', $context, $blocks);
        // line 146
        echo "
";
        // line 147
        $this->displayBlock('file_widget', $context, $blocks);
        // line 170
        echo "
";
        // line 171
        $this->displayBlock('choice_widget', $context, $blocks);
        // line 180
        echo "
";
        // line 181
        $this->displayBlock('choice_widget_expanded', $context, $blocks);
        // line 194
        echo "
";
        // line 195
        $this->displayBlock('choice_widget_collapsed', $context, $blocks);
        // line 215
        echo "
";
        // line 216
        $this->displayBlock('choice_widget_options', $context, $blocks);
        // line 230
        echo "
";
        // line 231
        $this->displayBlock('checkbox_row', $context, $blocks);
        // line 302
        echo "
";
        // line 303
        $this->displayBlock('radio_row', $context, $blocks);
        // line 369
        echo "
";
        // line 370
        $this->displayBlock('checkbox_widget', $context, $blocks);
        // line 375
        echo "
";
        // line 376
        $this->displayBlock('radio_widget', $context, $blocks);
        // line 381
        echo "
";
        // line 382
        $this->displayBlock('datetime_widget', $context, $blocks);
        // line 397
        echo "
";
        // line 398
        $this->displayBlock('date_widget', $context, $blocks);
        // line 414
        echo "
";
        // line 415
        $this->displayBlock('time_widget', $context, $blocks);
        // line 430
        echo "
";
        // line 431
        $this->displayBlock('number_widget', $context, $blocks);
        // line 438
        echo "
";
        // line 439
        $this->displayBlock('integer_widget', $context, $blocks);
        // line 445
        echo "
";
        // line 446
        $this->displayBlock('money_widget', $context, $blocks);
        // line 457
        echo "
";
        // line 458
        $this->displayBlock('url_widget', $context, $blocks);
        // line 464
        echo "
";
        // line 465
        $this->displayBlock('search_widget', $context, $blocks);
        // line 471
        echo "
";
        // line 472
        $this->displayBlock('percent_widget', $context, $blocks);
        // line 481
        echo "
";
        // line 482
        $this->displayBlock('password_widget', $context, $blocks);
        // line 488
        echo "
";
        // line 489
        $this->displayBlock('hidden_widget', $context, $blocks);
        // line 495
        echo "
";
        // line 496
        $this->displayBlock('email_widget', $context, $blocks);
        // line 502
        echo "
";
        // line 503
        $this->displayBlock('button_widget', $context, $blocks);
        // line 520
        echo "
";
        // line 521
        $this->displayBlock('submit_widget', $context, $blocks);
        // line 527
        echo "
";
        // line 528
        $this->displayBlock('reset_widget', $context, $blocks);
        // line 534
        echo "
";
        // line 535
        $this->displayBlock('form_actions_widget', $context, $blocks);
        // line 540
        echo "
";
        // line 541
        $this->displayBlock('bs_static_widget', $context, $blocks);
        // line 545
        echo "
";
        // line 547
        echo "
";
        // line 548
        $this->displayBlock('form_label', $context, $blocks);
        // line 590
        echo "
";
        // line 591
        $this->displayBlock('button_label', $context, $blocks);
        // line 592
        echo "
";
        // line 594
        echo "
";
        // line 595
        $this->displayBlock('repeated_row', $context, $blocks);
        // line 604
        echo "
";
        // line 605
        $this->displayBlock('form_row', $context, $blocks);
        // line 648
        echo "
";
        // line 649
        $this->displayBlock('form_input_group', $context, $blocks);
        // line 683
        echo "
";
        // line 684
        $this->displayBlock('form_help', $context, $blocks);
        // line 695
        echo "
";
        // line 696
        $this->displayBlock('button_row', $context, $blocks);
        // line 730
        echo "
";
        // line 731
        $this->displayBlock('hidden_row', $context, $blocks);
        // line 734
        echo "
";
        // line 735
        $this->displayBlock('form_actions_row', $context, $blocks);
        // line 738
        echo "
";
        // line 740
        echo "
";
        // line 741
        $this->displayBlock('form', $context, $blocks);
        // line 748
        echo "
";
        // line 749
        $this->displayBlock('form_start', $context, $blocks);
        // line 794
        echo "
";
        // line 795
        $this->displayBlock('form_end', $context, $blocks);
        // line 819
        echo "
";
        // line 820
        $this->displayBlock('form_enctype', $context, $blocks);
        // line 825
        echo "
";
        // line 826
        $this->displayBlock('global_form_errors', $context, $blocks);
        // line 832
        echo "
";
        // line 833
        $this->displayBlock('form_errors', $context, $blocks);
        // line 857
        echo "
";
        // line 858
        $this->displayBlock('form_rest', $context, $blocks);
        // line 867
        echo "
";
        // line 869
        echo "
";
        // line 870
        $this->displayBlock('form_rows', $context, $blocks);
        // line 893
        echo "
";
        // line 894
        $this->displayBlock('widget_attributes', $context, $blocks);
        // line 900
        echo "
";
        // line 901
        $this->displayBlock('widget_container_attributes', $context, $blocks);
        // line 911
        echo "
";
        // line 912
        $this->displayBlock('button_attributes', $context, $blocks);
        
        $__internal_b817f5a64717d9a4338caa2cf48bcdd57021dab1bd384ced66da290d347471c0->leave($__internal_b817f5a64717d9a4338caa2cf48bcdd57021dab1bd384ced66da290d347471c0_prof);

        
        $__internal_1ad93d89f0b238b952eab1d56e004384710ac01b3fda02cf57e71f52b4723e8c->leave($__internal_1ad93d89f0b238b952eab1d56e004384710ac01b3fda02cf57e71f52b4723e8c_prof);

    }

    // line 5
    public function block_form_widget($context, array $blocks = array())
    {
        $__internal_29408817f49aa28415470e11c098e3c3bc354266af415204aa949c0bad5bef37 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_29408817f49aa28415470e11c098e3c3bc354266af415204aa949c0bad5bef37->enter($__internal_29408817f49aa28415470e11c098e3c3bc354266af415204aa949c0bad5bef37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget"));

        $__internal_845a2991998ea92c49a5760ddb5d429e6cee635a74d452396d592ae5fc5f05f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_845a2991998ea92c49a5760ddb5d429e6cee635a74d452396d592ae5fc5f05f0->enter($__internal_845a2991998ea92c49a5760ddb5d429e6cee635a74d452396d592ae5fc5f05f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget"));

        // line 6
        echo "    ";
        ob_start();
        // line 7
        echo "        ";
        if ((isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound"))) {
            // line 8
            echo "            ";
            $this->displayBlock("form_widget_compound", $context, $blocks);
            echo "
        ";
        } else {
            // line 10
            echo "            ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
        ";
        }
        // line 12
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_845a2991998ea92c49a5760ddb5d429e6cee635a74d452396d592ae5fc5f05f0->leave($__internal_845a2991998ea92c49a5760ddb5d429e6cee635a74d452396d592ae5fc5f05f0_prof);

        
        $__internal_29408817f49aa28415470e11c098e3c3bc354266af415204aa949c0bad5bef37->leave($__internal_29408817f49aa28415470e11c098e3c3bc354266af415204aa949c0bad5bef37_prof);

    }

    // line 15
    public function block_form_widget_simple($context, array $blocks = array())
    {
        $__internal_01638e5b8f476266d6f3bafb1418a55319db96525a04c6cf7c41fe9199a4e1f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01638e5b8f476266d6f3bafb1418a55319db96525a04c6cf7c41fe9199a4e1f4->enter($__internal_01638e5b8f476266d6f3bafb1418a55319db96525a04c6cf7c41fe9199a4e1f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        $__internal_d60988128336f61607b52d9483e026ca01d0675b0c019bba724e2740d9fc0dfc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d60988128336f61607b52d9483e026ca01d0675b0c019bba724e2740d9fc0dfc->enter($__internal_d60988128336f61607b52d9483e026ca01d0675b0c019bba724e2740d9fc0dfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        // line 16
        echo "    ";
        ob_start();
        // line 17
        echo "        ";
        $context["style"] = ((array_key_exists("style", $context)) ? (_twig_default_filter((isset($context["style"]) ? $context["style"] : $this->getContext($context, "style")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getStyle())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getStyle()));
        // line 18
        echo "        ";
        $context["col_size"] = ((array_key_exists("col_size", $context)) ? (_twig_default_filter((isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize()));
        // line 19
        echo "
        ";
        // line 20
        if (( !array_key_exists("simple_col", $context) && $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getSimpleCol())) {
            // line 21
            echo "            ";
            $context["simple_col"] = $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getSimpleCol();
            // line 22
            echo "        ";
        }
        // line 23
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "simple_col", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "simple_col", array())))) {
            // line 24
            echo "            ";
            $context["simple_col"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "simple_col", array());
            // line 25
            echo "        ";
        }
        // line 26
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "col_size", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array())))) {
            // line 27
            echo "            ";
            $context["col_size"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array());
            // line 28
            echo "        ";
        }
        // line 29
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "style", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array())))) {
            // line 30
            echo "            ";
            $context["style"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array());
            // line 31
            echo "        ";
        }
        // line 32
        echo "
        ";
        // line 33
        if ((array_key_exists("simple_col", $context) && (isset($context["simple_col"]) ? $context["simple_col"] : $this->getContext($context, "simple_col")))) {
            // line 34
            echo "            <div class=\"col-";
            echo twig_escape_filter($this->env, (isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size")), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, (isset($context["simple_col"]) ? $context["simple_col"] : $this->getContext($context, "simple_col")), "html", null, true);
            echo "\">
        ";
        }
        // line 36
        echo "
        ";
        // line 37
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "text")) : ("text"));
        // line 38
        echo "
        ";
        // line 39
        if (((((isset($context["style"]) ? $context["style"] : $this->getContext($context, "style")) == "inline") && ( !$this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "placeholder", array(), "any", true, true) || twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "placeholder", array())))) &&  !((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false))) {
            // line 40
            echo "            ";
            if (twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")))) {
                // line 41
                echo "                ";
                $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("placeholder" => $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")))));
                // line 42
                echo "            ";
            } else {
                // line 43
                echo "                ";
                $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("placeholder" => (isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"))));
                // line 44
                echo "            ";
            }
            // line 45
            echo "        ";
        }
        // line 46
        echo "
        ";
        // line 47
        if ((array_key_exists("static_control", $context) && ((isset($context["static_control"]) ? $context["static_control"] : $this->getContext($context, "static_control")) == true))) {
            // line 48
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control-static"))));
            // line 49
            echo "            <p id=\"";
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
            echo "\"";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "</p>";
        } else {
            // line 51
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control"))));
            // line 52
            echo "            <input type=\"";
            echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "html", null, true);
            echo "\" ";
            $this->displayBlock("widget_attributes", $context, $blocks);
            echo " ";
            if ( !twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
                echo "value=\"";
                echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
                echo "\" ";
            }
            echo ">";
        }
        // line 54
        echo "        ";
        if (array_key_exists("simple_col", $context)) {
            // line 55
            echo "            </div>
        ";
        }
        // line 57
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_d60988128336f61607b52d9483e026ca01d0675b0c019bba724e2740d9fc0dfc->leave($__internal_d60988128336f61607b52d9483e026ca01d0675b0c019bba724e2740d9fc0dfc_prof);

        
        $__internal_01638e5b8f476266d6f3bafb1418a55319db96525a04c6cf7c41fe9199a4e1f4->leave($__internal_01638e5b8f476266d6f3bafb1418a55319db96525a04c6cf7c41fe9199a4e1f4_prof);

    }

    // line 60
    public function block_form_widget_compound($context, array $blocks = array())
    {
        $__internal_530a3a4acd2e22e02c39ffec27fc92a40a7189a36e8cee1542ef0ed2b2d6656f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_530a3a4acd2e22e02c39ffec27fc92a40a7189a36e8cee1542ef0ed2b2d6656f->enter($__internal_530a3a4acd2e22e02c39ffec27fc92a40a7189a36e8cee1542ef0ed2b2d6656f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_compound"));

        $__internal_0d52487a469d0d81da77a961be4ad43c462d7ccda3b32ee0f9a861f92eedc50e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0d52487a469d0d81da77a961be4ad43c462d7ccda3b32ee0f9a861f92eedc50e->enter($__internal_0d52487a469d0d81da77a961be4ad43c462d7ccda3b32ee0f9a861f92eedc50e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_compound"));

        // line 61
        echo "    ";
        ob_start();
        // line 62
        echo "        <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
            ";
        // line 63
        if (twig_test_empty($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array()))) {
            // line 64
            echo "                ";
            $this->displayBlock("global_form_errors", $context, $blocks);
            echo "
            ";
        }
        // line 66
        echo "            ";
        $this->displayBlock("form_rows", $context, $blocks);
        echo "
            ";
        // line 67
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
        </div>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_0d52487a469d0d81da77a961be4ad43c462d7ccda3b32ee0f9a861f92eedc50e->leave($__internal_0d52487a469d0d81da77a961be4ad43c462d7ccda3b32ee0f9a861f92eedc50e_prof);

        
        $__internal_530a3a4acd2e22e02c39ffec27fc92a40a7189a36e8cee1542ef0ed2b2d6656f->leave($__internal_530a3a4acd2e22e02c39ffec27fc92a40a7189a36e8cee1542ef0ed2b2d6656f_prof);

    }

    // line 72
    public function block_collection_widget($context, array $blocks = array())
    {
        $__internal_d28eed46c6768ac8571dc3de622c061489ffff644c943c58cd0e9d6a3aeebceb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d28eed46c6768ac8571dc3de622c061489ffff644c943c58cd0e9d6a3aeebceb->enter($__internal_d28eed46c6768ac8571dc3de622c061489ffff644c943c58cd0e9d6a3aeebceb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "collection_widget"));

        $__internal_1cf27f8e7127cc936301e83110d263c449746196f2ff2a991088a9a836c55b6d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1cf27f8e7127cc936301e83110d263c449746196f2ff2a991088a9a836c55b6d->enter($__internal_1cf27f8e7127cc936301e83110d263c449746196f2ff2a991088a9a836c55b6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "collection_widget"));

        // line 73
        echo "    ";
        ob_start();
        // line 74
        echo "        ";
        if (array_key_exists("prototype", $context)) {
            // line 75
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("data-prototype" => $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["prototype"]) ? $context["prototype"] : $this->getContext($context, "prototype")), 'row')));
            // line 76
            echo "        ";
        }
        // line 77
        echo "        ";
        $this->displayBlock("form_widget", $context, $blocks);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_1cf27f8e7127cc936301e83110d263c449746196f2ff2a991088a9a836c55b6d->leave($__internal_1cf27f8e7127cc936301e83110d263c449746196f2ff2a991088a9a836c55b6d_prof);

        
        $__internal_d28eed46c6768ac8571dc3de622c061489ffff644c943c58cd0e9d6a3aeebceb->leave($__internal_d28eed46c6768ac8571dc3de622c061489ffff644c943c58cd0e9d6a3aeebceb_prof);

    }

    // line 81
    public function block_bootstrap_collection_widget($context, array $blocks = array())
    {
        $__internal_e44cda3b43e5d1f140179f669e4e820659a800f7afc756e987888c7014c31592 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e44cda3b43e5d1f140179f669e4e820659a800f7afc756e987888c7014c31592->enter($__internal_e44cda3b43e5d1f140179f669e4e820659a800f7afc756e987888c7014c31592_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bootstrap_collection_widget"));

        $__internal_211e84bcddf021ea53e63a60ea32b97c9d0f3276d89ab6e952064156e269271f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_211e84bcddf021ea53e63a60ea32b97c9d0f3276d89ab6e952064156e269271f->enter($__internal_211e84bcddf021ea53e63a60ea32b97c9d0f3276d89ab6e952064156e269271f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bootstrap_collection_widget"));

        // line 82
        echo "    ";
        ob_start();
        // line 83
        echo "        ";
        if (array_key_exists("prototype", $context)) {
            // line 84
            echo "            ";
            $context["prototype_vars"] = array();
            // line 85
            echo "            ";
            if (array_key_exists("style", $context)) {
                // line 86
                echo "                ";
                $context["prototype_vars"] = twig_array_merge((isset($context["prototype_vars"]) ? $context["prototype_vars"] : $this->getContext($context, "prototype_vars")), array("style" => (isset($context["style"]) ? $context["style"] : $this->getContext($context, "style"))));
                // line 87
                echo "            ";
            }
            // line 88
            echo "            ";
            $context["prototype_html"] = (((("<div class=\"col-xs-" . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "sub_widget_col", array())) . "\">") . $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["prototype"]) ? $context["prototype"] : $this->getContext($context, "prototype")), 'widget', (isset($context["prototype_vars"]) ? $context["prototype_vars"] : $this->getContext($context, "prototype_vars")))) . "</div>");
            // line 89
            echo "            ";
            if ($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "allow_delete", array())) {
                // line 90
                echo "                ";
                $context["prototype_html"] = ((((((isset($context["prototype_html"]) ? $context["prototype_html"] : $this->getContext($context, "prototype_html")) . "<div class=\"col-xs-") . $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "button_col", array())) . "\"><a href=\"#\" class=\"btn btn-danger btn-sm\" data-removefield=\"collection\" data-field=\"__id__\">") . $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapIconExtension')->parseIconsFilter(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "delete_button_text", array()), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true))) . "</a></div>");
                // line 91
                echo "            ";
            }
            // line 92
            echo "            ";
            $context["prototype_html"] = (("<div class=\"row\">" . (isset($context["prototype_html"]) ? $context["prototype_html"] : $this->getContext($context, "prototype_html"))) . "</div>");
            // line 93
            echo "
            ";
            // line 94
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("data-prototype" => (isset($context["prototype_html"]) ? $context["prototype_html"] : $this->getContext($context, "prototype_html"))));
            // line 95
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("data-prototype-name" => (isset($context["prototype_name"]) ? $context["prototype_name"] : $this->getContext($context, "prototype_name"))));
            // line 96
            echo "        ";
        }
        // line 97
        echo "        <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
            <ul class=\"bc-collection list-unstyled\">
                ";
        // line 99
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 100
            echo "                    <li>
                        <div class=\"row\">
                            <div class=\"col-xs-";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "sub_widget_col", array()), "html", null, true);
            echo "\">
                                ";
            // line 103
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["field"], 'widget');
            echo "
                                ";
            // line 104
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["field"], 'errors');
            echo "
                            </div>
                            ";
            // line 106
            if ($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "allow_delete", array())) {
                // line 107
                echo "                                <div class=\"col-xs-";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "button_col", array()), "html", null, true);
                echo "\">
                                    <a href=\"#\" class=\"btn btn-danger btn-sm\" data-removefield=\"collection\" data-field=\"";
                // line 108
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "id", array()), "html", null, true);
                echo "\">";
                echo $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapIconExtension')->parseIconsFilter(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "delete_button_text", array()), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true));
                echo "</a>
                                </div>
                            ";
            }
            // line 111
            echo "                        </div>
                    </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 114
        echo "            </ul>
            ";
        // line 115
        if ($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "allow_add", array())) {
            // line 116
            echo "                <a href=\"#\" class=\"btn btn-primary btn-sm\" data-addfield=\"collection\" data-collection=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "id", array()), "html", null, true);
            echo "\" data-prototype-name=\"";
            echo twig_escape_filter($this->env, (isset($context["prototype_name"]) ? $context["prototype_name"] : $this->getContext($context, "prototype_name")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapIconExtension')->parseIconsFilter(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "add_button_text", array()), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true));
            echo "</a>
            ";
        }
        // line 118
        echo "        </div>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_211e84bcddf021ea53e63a60ea32b97c9d0f3276d89ab6e952064156e269271f->leave($__internal_211e84bcddf021ea53e63a60ea32b97c9d0f3276d89ab6e952064156e269271f_prof);

        
        $__internal_e44cda3b43e5d1f140179f669e4e820659a800f7afc756e987888c7014c31592->leave($__internal_e44cda3b43e5d1f140179f669e4e820659a800f7afc756e987888c7014c31592_prof);

    }

    // line 122
    public function block_textarea_widget($context, array $blocks = array())
    {
        $__internal_86ff625083f4595bcbb427e2a8e80205e558d177de479f7706d938bfff26bfae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86ff625083f4595bcbb427e2a8e80205e558d177de479f7706d938bfff26bfae->enter($__internal_86ff625083f4595bcbb427e2a8e80205e558d177de479f7706d938bfff26bfae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        $__internal_ea32d1b08a6a6104e72f0dcc7a9bfa29cb678d859161a55a3728e66690910936 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ea32d1b08a6a6104e72f0dcc7a9bfa29cb678d859161a55a3728e66690910936->enter($__internal_ea32d1b08a6a6104e72f0dcc7a9bfa29cb678d859161a55a3728e66690910936_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        // line 123
        echo "    ";
        ob_start();
        // line 124
        echo "        ";
        $context["col_size"] = ((array_key_exists("col_size", $context)) ? (_twig_default_filter((isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize()));
        // line 125
        echo "
        ";
        // line 126
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "simple_col", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "simple_col", array())))) {
            // line 127
            echo "            ";
            $context["simple_col"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "simple_col", array());
            // line 128
            echo "        ";
        }
        // line 129
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "col_size", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array())))) {
            // line 130
            echo "            ";
            $context["col_size"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array());
            // line 131
            echo "        ";
        }
        // line 132
        echo "
        ";
        // line 133
        if (array_key_exists("simple_col", $context)) {
            // line 134
            echo "            <div class=\"col-";
            echo twig_escape_filter($this->env, (isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size")), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, (isset($context["simple_col"]) ? $context["simple_col"] : $this->getContext($context, "simple_col")), "html", null, true);
            echo "\">
        ";
        }
        // line 136
        echo "
        ";
        // line 137
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control"))));
        // line 138
        echo "
        <textarea ";
        // line 139
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo ">";
        echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
        echo "</textarea>

        ";
        // line 141
        if (array_key_exists("simple_col", $context)) {
            // line 142
            echo "            </div>
        ";
        }
        // line 144
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_ea32d1b08a6a6104e72f0dcc7a9bfa29cb678d859161a55a3728e66690910936->leave($__internal_ea32d1b08a6a6104e72f0dcc7a9bfa29cb678d859161a55a3728e66690910936_prof);

        
        $__internal_86ff625083f4595bcbb427e2a8e80205e558d177de479f7706d938bfff26bfae->leave($__internal_86ff625083f4595bcbb427e2a8e80205e558d177de479f7706d938bfff26bfae_prof);

    }

    // line 147
    public function block_file_widget($context, array $blocks = array())
    {
        $__internal_9dd3ce4711c122264e1e0c9ec9ce5101544bb0fa029267bf2f65549463029e18 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9dd3ce4711c122264e1e0c9ec9ce5101544bb0fa029267bf2f65549463029e18->enter($__internal_9dd3ce4711c122264e1e0c9ec9ce5101544bb0fa029267bf2f65549463029e18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "file_widget"));

        $__internal_447b3437f44f78dd390285fed851b7eff877a891c514abf16d9ca3290b56cc75 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_447b3437f44f78dd390285fed851b7eff877a891c514abf16d9ca3290b56cc75->enter($__internal_447b3437f44f78dd390285fed851b7eff877a891c514abf16d9ca3290b56cc75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "file_widget"));

        // line 148
        echo "    ";
        ob_start();
        // line 149
        echo "        ";
        $context["col_size"] = ((array_key_exists("col_size", $context)) ? (_twig_default_filter((isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize()));
        // line 150
        echo "
        ";
        // line 151
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "simple_col", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "simple_col", array())))) {
            // line 152
            echo "            ";
            $context["simple_col"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "simple_col", array());
            // line 153
            echo "        ";
        }
        // line 154
        echo "
        ";
        // line 155
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "col_size", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array())))) {
            // line 156
            echo "            ";
            $context["col_size"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array());
            // line 157
            echo "        ";
        }
        // line 158
        echo "
        ";
        // line 159
        if (array_key_exists("simple_col", $context)) {
            // line 160
            echo "            <div class=\"col-";
            echo twig_escape_filter($this->env, (isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size")), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, (isset($context["simple_col"]) ? $context["simple_col"] : $this->getContext($context, "simple_col")), "html", null, true);
            echo "\">
        ";
        }
        // line 162
        echo "
        <input type=\"file\" ";
        // line 163
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo ">

        ";
        // line 165
        if (array_key_exists("simple_col", $context)) {
            // line 166
            echo "            </div>
        ";
        }
        // line 168
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_447b3437f44f78dd390285fed851b7eff877a891c514abf16d9ca3290b56cc75->leave($__internal_447b3437f44f78dd390285fed851b7eff877a891c514abf16d9ca3290b56cc75_prof);

        
        $__internal_9dd3ce4711c122264e1e0c9ec9ce5101544bb0fa029267bf2f65549463029e18->leave($__internal_9dd3ce4711c122264e1e0c9ec9ce5101544bb0fa029267bf2f65549463029e18_prof);

    }

    // line 171
    public function block_choice_widget($context, array $blocks = array())
    {
        $__internal_786b6c617ee8d3a925f6debf835ac0004251a3abde848ae87d956be5cb4b3d03 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_786b6c617ee8d3a925f6debf835ac0004251a3abde848ae87d956be5cb4b3d03->enter($__internal_786b6c617ee8d3a925f6debf835ac0004251a3abde848ae87d956be5cb4b3d03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget"));

        $__internal_185a4010a8972ec2f4d9f484a8ac9f21c8c591c34341ef5915aec4ca2ce5d3d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_185a4010a8972ec2f4d9f484a8ac9f21c8c591c34341ef5915aec4ca2ce5d3d9->enter($__internal_185a4010a8972ec2f4d9f484a8ac9f21c8c591c34341ef5915aec4ca2ce5d3d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget"));

        // line 172
        echo "    ";
        ob_start();
        // line 173
        echo "        ";
        if ((isset($context["expanded"]) ? $context["expanded"] : $this->getContext($context, "expanded"))) {
            // line 174
            echo "            ";
            $this->displayBlock("choice_widget_expanded", $context, $blocks);
            echo "
        ";
        } else {
            // line 176
            echo "            ";
            $this->displayBlock("choice_widget_collapsed", $context, $blocks);
            echo "
        ";
        }
        // line 178
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_185a4010a8972ec2f4d9f484a8ac9f21c8c591c34341ef5915aec4ca2ce5d3d9->leave($__internal_185a4010a8972ec2f4d9f484a8ac9f21c8c591c34341ef5915aec4ca2ce5d3d9_prof);

        
        $__internal_786b6c617ee8d3a925f6debf835ac0004251a3abde848ae87d956be5cb4b3d03->leave($__internal_786b6c617ee8d3a925f6debf835ac0004251a3abde848ae87d956be5cb4b3d03_prof);

    }

    // line 181
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        $__internal_aa89979b830c91179f6320ea0be7d0587aa9ef487ec269caeb18669595af67dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa89979b830c91179f6320ea0be7d0587aa9ef487ec269caeb18669595af67dc->enter($__internal_aa89979b830c91179f6320ea0be7d0587aa9ef487ec269caeb18669595af67dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        $__internal_63c0735c956c19c994e2bb43457cb9f10c964624bcca55fbca80faee589d4a68 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63c0735c956c19c994e2bb43457cb9f10c964624bcca55fbca80faee589d4a68->enter($__internal_63c0735c956c19c994e2bb43457cb9f10c964624bcca55fbca80faee589d4a68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        // line 182
        echo "    ";
        ob_start();
        // line 183
        echo "        <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
            ";
        // line 184
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 185
            echo "                ";
            if ((isset($context["multiple"]) ? $context["multiple"] : $this->getContext($context, "multiple"))) {
                // line 186
                echo "                    ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'row', array("no_form_group" => true, "inline" => ($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "inline", array(), "any", true, true) && $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "inline", array())), "label_attr" => (isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr"))));
                echo "
                ";
            } else {
                // line 188
                echo "                    ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'row', array("no_form_group" => true, "inline" => ($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "inline", array(), "any", true, true) && $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "inline", array())), "label_attr" => (isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr"))));
                echo "
                ";
            }
            // line 190
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 191
        echo "        </div>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_63c0735c956c19c994e2bb43457cb9f10c964624bcca55fbca80faee589d4a68->leave($__internal_63c0735c956c19c994e2bb43457cb9f10c964624bcca55fbca80faee589d4a68_prof);

        
        $__internal_aa89979b830c91179f6320ea0be7d0587aa9ef487ec269caeb18669595af67dc->leave($__internal_aa89979b830c91179f6320ea0be7d0587aa9ef487ec269caeb18669595af67dc_prof);

    }

    // line 195
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        $__internal_8b8666c336b4c8712e2fe971b6cf8564882a848e84522662672e7c739fd6cf58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8b8666c336b4c8712e2fe971b6cf8564882a848e84522662672e7c739fd6cf58->enter($__internal_8b8666c336b4c8712e2fe971b6cf8564882a848e84522662672e7c739fd6cf58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        $__internal_78df1442c9160fe6a29e8587b90b62996df4a7c3883452cb38d044ed707e41ea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78df1442c9160fe6a29e8587b90b62996df4a7c3883452cb38d044ed707e41ea->enter($__internal_78df1442c9160fe6a29e8587b90b62996df4a7c3883452cb38d044ed707e41ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        // line 196
        echo "    ";
        ob_start();
        // line 197
        echo "        ";
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control"))));
        // line 198
        echo "
        <select ";
        // line 199
        $this->displayBlock("widget_attributes", $context, $blocks);
        if ((isset($context["multiple"]) ? $context["multiple"] : $this->getContext($context, "multiple"))) {
            echo " multiple=\"multiple\"";
        }
        echo ">
            ";
        // line 200
        if ( !(null === (isset($context["placeholder"]) ? $context["placeholder"] : $this->getContext($context, "placeholder")))) {
            // line 201
            echo "                <option ";
            if ((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) {
                echo " disabled=\"disabled\"";
                if (twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
                    echo " selected=\"selected\"";
                }
            }
            echo " value=\"\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["placeholder"]) ? $context["placeholder"] : $this->getContext($context, "placeholder")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
            echo "</option>
            ";
        }
        // line 203
        echo "            ";
        if ((twig_length_filter($this->env, (isset($context["preferred_choices"]) ? $context["preferred_choices"] : $this->getContext($context, "preferred_choices"))) > 0)) {
            // line 204
            echo "                ";
            $context["options"] = (isset($context["preferred_choices"]) ? $context["preferred_choices"] : $this->getContext($context, "preferred_choices"));
            // line 205
            echo "                ";
            $this->displayBlock("choice_widget_options", $context, $blocks);
            echo "
                ";
            // line 206
            if (((twig_length_filter($this->env, (isset($context["choices"]) ? $context["choices"] : $this->getContext($context, "choices"))) > 0) &&  !(null === (isset($context["separator"]) ? $context["separator"] : $this->getContext($context, "separator"))))) {
                // line 207
                echo "                    <option disabled=\"disabled\">";
                echo twig_escape_filter($this->env, (isset($context["separator"]) ? $context["separator"] : $this->getContext($context, "separator")), "html", null, true);
                echo "</option>
                ";
            }
            // line 209
            echo "            ";
        }
        // line 210
        echo "            ";
        $context["options"] = (isset($context["choices"]) ? $context["choices"] : $this->getContext($context, "choices"));
        // line 211
        echo "            ";
        $this->displayBlock("choice_widget_options", $context, $blocks);
        echo "
        </select>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_78df1442c9160fe6a29e8587b90b62996df4a7c3883452cb38d044ed707e41ea->leave($__internal_78df1442c9160fe6a29e8587b90b62996df4a7c3883452cb38d044ed707e41ea_prof);

        
        $__internal_8b8666c336b4c8712e2fe971b6cf8564882a848e84522662672e7c739fd6cf58->leave($__internal_8b8666c336b4c8712e2fe971b6cf8564882a848e84522662672e7c739fd6cf58_prof);

    }

    // line 216
    public function block_choice_widget_options($context, array $blocks = array())
    {
        $__internal_594d29a316b6c0dcb9d4ef55fd8e84fcb5bf3b1f1558b5c6c4d240177faeb826 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_594d29a316b6c0dcb9d4ef55fd8e84fcb5bf3b1f1558b5c6c4d240177faeb826->enter($__internal_594d29a316b6c0dcb9d4ef55fd8e84fcb5bf3b1f1558b5c6c4d240177faeb826_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_options"));

        $__internal_05596f993cced2b46adf3c0d3a4b8049562ecb04a6c6e98be01947c835bc74dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_05596f993cced2b46adf3c0d3a4b8049562ecb04a6c6e98be01947c835bc74dc->enter($__internal_05596f993cced2b46adf3c0d3a4b8049562ecb04a6c6e98be01947c835bc74dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_options"));

        // line 217
        echo "    ";
        ob_start();
        // line 218
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["group_label"] => $context["choice"]) {
            // line 219
            echo "            ";
            if (twig_test_iterable($context["choice"])) {
                // line 220
                echo "                <optgroup label=\"";
                echo twig_escape_filter($this->env, ((array_key_exists("choice_translation_domain", $context)) ? (((((isset($context["choice_translation_domain"]) ? $context["choice_translation_domain"] : $this->getContext($context, "choice_translation_domain")) === false)) ? ($context["group_label"]) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["group_label"], array(), (isset($context["choice_translation_domain"]) ? $context["choice_translation_domain"] : $this->getContext($context, "choice_translation_domain")))))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["group_label"], array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))))), "html", null, true);
                echo "\">
                    ";
                // line 221
                $context["options"] = $context["choice"];
                // line 222
                echo "                    ";
                $this->displayBlock("choice_widget_options", $context, $blocks);
                echo "
                </optgroup>
            ";
            } else {
                // line 225
                echo "                <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["choice"], "value", array()), "html", null, true);
                echo "\"";
                if (Symfony\Bridge\Twig\Extension\twig_is_selected_choice($context["choice"], (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
                    echo " selected=\"selected\"";
                }
                echo ">";
                echo twig_escape_filter($this->env, ((array_key_exists("choice_translation_domain", $context)) ? (((((isset($context["choice_translation_domain"]) ? $context["choice_translation_domain"] : $this->getContext($context, "choice_translation_domain")) === false)) ? ($this->getAttribute($context["choice"], "label", array())) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($context["choice"], "label", array()), array(), (isset($context["choice_translation_domain"]) ? $context["choice_translation_domain"] : $this->getContext($context, "choice_translation_domain")))))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($context["choice"], "label", array()), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))))), "html", null, true);
                echo "</option>
            ";
            }
            // line 227
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['group_label'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 228
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_05596f993cced2b46adf3c0d3a4b8049562ecb04a6c6e98be01947c835bc74dc->leave($__internal_05596f993cced2b46adf3c0d3a4b8049562ecb04a6c6e98be01947c835bc74dc_prof);

        
        $__internal_594d29a316b6c0dcb9d4ef55fd8e84fcb5bf3b1f1558b5c6c4d240177faeb826->leave($__internal_594d29a316b6c0dcb9d4ef55fd8e84fcb5bf3b1f1558b5c6c4d240177faeb826_prof);

    }

    // line 231
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_4dfe2537989ddc8c977e34ea95ca15c64f8b65bc33a969195e6185c8edf931b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4dfe2537989ddc8c977e34ea95ca15c64f8b65bc33a969195e6185c8edf931b0->enter($__internal_4dfe2537989ddc8c977e34ea95ca15c64f8b65bc33a969195e6185c8edf931b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_080d2e4d513b790d093d845568d1580c9fad7cbd3c86ec4f11d9456c77d83308 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_080d2e4d513b790d093d845568d1580c9fad7cbd3c86ec4f11d9456c77d83308->enter($__internal_080d2e4d513b790d093d845568d1580c9fad7cbd3c86ec4f11d9456c77d83308_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 232
        echo "    ";
        ob_start();
        // line 233
        echo "        ";
        $context["style"] = ((array_key_exists("style", $context)) ? (_twig_default_filter((isset($context["style"]) ? $context["style"] : $this->getContext($context, "style")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getStyle())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getStyle()));
        // line 234
        echo "        ";
        $context["col_size"] = ((array_key_exists("col_size", $context)) ? (_twig_default_filter((isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize()));
        // line 235
        echo "
        ";
        // line 236
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "label_col", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "label_col", array())))) {
            // line 237
            echo "            ";
            $context["label_col"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "label_col", array());
            // line 238
            echo "        ";
        }
        // line 239
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_col", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "widget_col", array())))) {
            // line 240
            echo "            ";
            $context["widget_col"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "widget_col", array());
            // line 241
            echo "        ";
        }
        // line 242
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "col_size", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array())))) {
            // line 243
            echo "            ";
            $context["col_size"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array());
            // line 244
            echo "        ";
        }
        // line 245
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "style", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array())))) {
            // line 246
            echo "            ";
            $context["style"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array());
            // line 247
            echo "        ";
        }
        // line 248
        echo "
        ";
        // line 249
        $context["class"] = "";
        // line 250
        echo "        ";
        if ((array_key_exists("align_with_widget", $context) || $this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "align_with_widget", array(), "any", true, true))) {
            // line 251
            echo "            ";
            $context["widget_col"] = ((array_key_exists("widget_col", $context)) ? (_twig_default_filter((isset($context["widget_col"]) ? $context["widget_col"] : $this->getContext($context, "widget_col")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getWidgetCol())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getWidgetCol()));
            // line 252
            echo "            ";
            $context["label_col"] = ((array_key_exists("label_col", $context)) ? (_twig_default_filter((isset($context["label_col"]) ? $context["label_col"] : $this->getContext($context, "label_col")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getLabelCol())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getLabelCol()));
            // line 253
            echo "            ";
            $context["class"] = ((((((("col-" . (isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size"))) . "-") . (isset($context["widget_col"]) ? $context["widget_col"] : $this->getContext($context, "widget_col"))) . " col-") . (isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size"))) . "-offset-") . (isset($context["label_col"]) ? $context["label_col"] : $this->getContext($context, "label_col")));
            // line 254
            echo "            <div class=\"form-group ";
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "errors", array())) > 0)) {
                echo " has-error";
            }
            echo "\">
            <div class=\"";
            // line 255
            echo twig_escape_filter($this->env, (isset($context["class"]) ? $context["class"] : $this->getContext($context, "class")), "html", null, true);
            echo "\">
        ";
        } elseif (( !        // line 256
array_key_exists("no_form_group", $context) || ((isset($context["no_form_group"]) ? $context["no_form_group"] : $this->getContext($context, "no_form_group")) == false))) {
            // line 257
            echo "            <div class=\"form-group";
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "errors", array())) > 0)) {
                echo " has-error";
            }
            echo "\">
        ";
        }
        // line 259
        echo "
        ";
        // line 260
        ob_start();
        // line 261
        echo "        ";
        if ( !((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false)) {
            // line 262
            echo "            ";
            if ( !(isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound"))) {
                // line 263
                echo "                ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("for" => (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))));
                // line 264
                echo "            ";
            }
            // line 265
            echo "            ";
            if ((array_key_exists("inline", $context) && (isset($context["inline"]) ? $context["inline"] : $this->getContext($context, "inline")))) {
                // line 266
                echo "                ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " checkbox-inline"))));
                // line 267
                echo "            ";
            }
            // line 268
            echo "            ";
            if ((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) {
                // line 269
                echo "                ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " required"))));
                // line 270
                echo "            ";
            }
            // line 271
            echo "            ";
            if (twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")))) {
                // line 272
                echo "                ";
                $context["label"] = $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")));
                // line 273
                echo "            ";
            }
            // line 274
            echo "            <label";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">
            ";
            // line 275
            $this->displayBlock("checkbox_widget", $context, $blocks);
            echo "
            ";
            // line 276
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")));
            // line 277
            echo "</label>
        ";
        } else {
            // line 279
            echo "            ";
            $this->displayBlock("checkbox_widget", $context, $blocks);
            echo "
        ";
        }
        // line 281
        echo "        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
        ";
        $context["checkboxdata"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 283
        echo "
        ";
        // line 284
        if ((array_key_exists("inline", $context) && (isset($context["inline"]) ? $context["inline"] : $this->getContext($context, "inline")))) {
            // line 285
            echo "            ";
            echo (isset($context["checkboxdata"]) ? $context["checkboxdata"] : $this->getContext($context, "checkboxdata"));
            echo "
        ";
        } else {
            // line 287
            echo "            <div class=\"checkbox\">";
            echo (isset($context["checkboxdata"]) ? $context["checkboxdata"] : $this->getContext($context, "checkboxdata"));
            echo "</div>
        ";
        }
        // line 289
        echo "
        ";
        // line 290
        $this->displayBlock("form_help", $context, $blocks);
        echo "

        ";
        // line 292
        if ((array_key_exists("align_with_widget", $context) || $this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "align_with_widget", array(), "any", true, true))) {
            // line 293
            echo "            </div>
            </div>
        ";
        } elseif (( !        // line 295
array_key_exists("no_form_group", $context) || ((isset($context["no_form_group"]) ? $context["no_form_group"] : $this->getContext($context, "no_form_group")) == false))) {
            // line 296
            echo "            </div>
        ";
        }
        // line 298
        echo "
        ";
        // line 299
        if (((isset($context["style"]) ? $context["style"] : $this->getContext($context, "style")) == "inline")) {
            echo "&nbsp;";
        }
        // line 300
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_080d2e4d513b790d093d845568d1580c9fad7cbd3c86ec4f11d9456c77d83308->leave($__internal_080d2e4d513b790d093d845568d1580c9fad7cbd3c86ec4f11d9456c77d83308_prof);

        
        $__internal_4dfe2537989ddc8c977e34ea95ca15c64f8b65bc33a969195e6185c8edf931b0->leave($__internal_4dfe2537989ddc8c977e34ea95ca15c64f8b65bc33a969195e6185c8edf931b0_prof);

    }

    // line 303
    public function block_radio_row($context, array $blocks = array())
    {
        $__internal_044f49e7fac3c1a8bbd3f2336f587c6327bb0368a4b93d3199dbe2167d1019aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_044f49e7fac3c1a8bbd3f2336f587c6327bb0368a4b93d3199dbe2167d1019aa->enter($__internal_044f49e7fac3c1a8bbd3f2336f587c6327bb0368a4b93d3199dbe2167d1019aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        $__internal_7d99c94d193fb6b0c4a06e2420855bdf1744cda4a49a6d7394d29dfd1f779ed4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d99c94d193fb6b0c4a06e2420855bdf1744cda4a49a6d7394d29dfd1f779ed4->enter($__internal_7d99c94d193fb6b0c4a06e2420855bdf1744cda4a49a6d7394d29dfd1f779ed4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        // line 304
        echo "    ";
        ob_start();
        // line 305
        echo "        ";
        $context["class"] = "";
        // line 306
        echo "
        ";
        // line 307
        $context["col_size"] = ((array_key_exists("col_size", $context)) ? (_twig_default_filter((isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize()));
        // line 308
        echo "
        ";
        // line 309
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "label_col", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "label_col", array())))) {
            // line 310
            echo "            ";
            $context["label_col"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "label_col", array());
            // line 311
            echo "        ";
        }
        // line 312
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_col", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "widget_col", array())))) {
            // line 313
            echo "            ";
            $context["widget_col"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "widget_col", array());
            // line 314
            echo "        ";
        }
        // line 315
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "col_size", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array())))) {
            // line 316
            echo "            ";
            $context["col_size"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array());
            // line 317
            echo "        ";
        }
        // line 318
        echo "
        ";
        // line 319
        if ((array_key_exists("align_with_widget", $context) || $this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "align_with_widget", array(), "any", true, true))) {
            // line 320
            echo "            ";
            $context["widget_col"] = ((array_key_exists("widget_col", $context)) ? (_twig_default_filter((isset($context["widget_col"]) ? $context["widget_col"] : $this->getContext($context, "widget_col")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getWidgetCol())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getWidgetCol()));
            // line 321
            echo "            ";
            $context["label_col"] = ((array_key_exists("label_col", $context)) ? (_twig_default_filter((isset($context["label_col"]) ? $context["label_col"] : $this->getContext($context, "label_col")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getLabelCol())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getLabelCol()));
            // line 322
            echo "            ";
            $context["class"] = (((((((" col-" . (isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size"))) . "-") . (isset($context["widget_col"]) ? $context["widget_col"] : $this->getContext($context, "widget_col"))) . " col-") . (isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size"))) . "-offset-") . (isset($context["label_col"]) ? $context["label_col"] : $this->getContext($context, "label_col")));
            // line 323
            echo "            <div class=\"form-group ";
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "errors", array())) > 0)) {
                echo " has-error";
            }
            echo "\">
            <div class=\"";
            // line 324
            echo twig_escape_filter($this->env, (isset($context["class"]) ? $context["class"] : $this->getContext($context, "class")), "html", null, true);
            echo "\">
        ";
        } elseif (( !        // line 325
array_key_exists("no_form_group", $context) || ((isset($context["no_form_group"]) ? $context["no_form_group"] : $this->getContext($context, "no_form_group")) == false))) {
            // line 326
            echo "            <div class=\"form-group";
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "errors", array())) > 0)) {
                echo " has-error";
            }
            echo "\">
        ";
        }
        // line 328
        echo "
        ";
        // line 329
        ob_start();
        // line 330
        echo "        ";
        if ( !((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false)) {
            // line 331
            echo "            ";
            if ( !(isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound"))) {
                // line 332
                echo "                ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("for" => (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))));
                // line 333
                echo "            ";
            }
            // line 334
            echo "            ";
            if ((array_key_exists("inline", $context) && (isset($context["inline"]) ? $context["inline"] : $this->getContext($context, "inline")))) {
                // line 335
                echo "                ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " radio-inline"))));
                // line 336
                echo "            ";
            }
            // line 337
            echo "            ";
            if ((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) {
                // line 338
                echo "                ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " required"))));
                // line 339
                echo "            ";
            }
            // line 340
            echo "            ";
            if (twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")))) {
                // line 341
                echo "                ";
                $context["label"] = $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")));
                // line 342
                echo "            ";
            }
            // line 343
            echo "            <label";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">
            ";
            // line 344
            $this->displayBlock("radio_widget", $context, $blocks);
            echo "
            ";
            // line 345
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")));
            // line 346
            echo "</label>
        ";
        } else {
            // line 348
            echo "            ";
            $this->displayBlock("radio_widget", $context, $blocks);
            echo "
        ";
        }
        // line 350
        echo "        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
        ";
        $context["radiodata"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 352
        echo "
        ";
        // line 353
        if ((array_key_exists("inline", $context) && (isset($context["inline"]) ? $context["inline"] : $this->getContext($context, "inline")))) {
            // line 354
            echo "            ";
            echo (isset($context["radiodata"]) ? $context["radiodata"] : $this->getContext($context, "radiodata"));
            echo "
        ";
        } else {
            // line 356
            echo "            <div class=\"radio\">";
            echo (isset($context["radiodata"]) ? $context["radiodata"] : $this->getContext($context, "radiodata"));
            echo "</div>
        ";
        }
        // line 358
        echo "
        ";
        // line 359
        $this->displayBlock("form_help", $context, $blocks);
        echo "

        ";
        // line 361
        if ((array_key_exists("align_with_widget", $context) || $this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "align_with_widget", array(), "any", true, true))) {
            // line 362
            echo "            </div>
            </div>
        ";
        } elseif (( !        // line 364
array_key_exists("no_form_group", $context) || ((isset($context["no_form_group"]) ? $context["no_form_group"] : $this->getContext($context, "no_form_group")) == false))) {
            // line 365
            echo "            </div>
        ";
        }
        // line 367
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_7d99c94d193fb6b0c4a06e2420855bdf1744cda4a49a6d7394d29dfd1f779ed4->leave($__internal_7d99c94d193fb6b0c4a06e2420855bdf1744cda4a49a6d7394d29dfd1f779ed4_prof);

        
        $__internal_044f49e7fac3c1a8bbd3f2336f587c6327bb0368a4b93d3199dbe2167d1019aa->leave($__internal_044f49e7fac3c1a8bbd3f2336f587c6327bb0368a4b93d3199dbe2167d1019aa_prof);

    }

    // line 370
    public function block_checkbox_widget($context, array $blocks = array())
    {
        $__internal_ec6ba3557f018c6611cb2ada28a573c7b44e50daa1d0ae7b271f27c6ac4e8b79 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec6ba3557f018c6611cb2ada28a573c7b44e50daa1d0ae7b271f27c6ac4e8b79->enter($__internal_ec6ba3557f018c6611cb2ada28a573c7b44e50daa1d0ae7b271f27c6ac4e8b79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        $__internal_ac207cb520609575a4d30f76b4778cbf999bf7c7b923b4d59c90062b22856fa4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ac207cb520609575a4d30f76b4778cbf999bf7c7b923b4d59c90062b22856fa4->enter($__internal_ac207cb520609575a4d30f76b4778cbf999bf7c7b923b4d59c90062b22856fa4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        // line 371
        echo "    ";
        ob_start();
        // line 372
        echo "        <input type=\"checkbox\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (array_key_exists("value", $context)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "\"";
        }
        if ((isset($context["checked"]) ? $context["checked"] : $this->getContext($context, "checked"))) {
            echo " checked=\"checked\"";
        }
        echo " />
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_ac207cb520609575a4d30f76b4778cbf999bf7c7b923b4d59c90062b22856fa4->leave($__internal_ac207cb520609575a4d30f76b4778cbf999bf7c7b923b4d59c90062b22856fa4_prof);

        
        $__internal_ec6ba3557f018c6611cb2ada28a573c7b44e50daa1d0ae7b271f27c6ac4e8b79->leave($__internal_ec6ba3557f018c6611cb2ada28a573c7b44e50daa1d0ae7b271f27c6ac4e8b79_prof);

    }

    // line 376
    public function block_radio_widget($context, array $blocks = array())
    {
        $__internal_c78e3a8d114508c32550c15e5eb0e975ea4f60098f2db066b70933ab1460fe72 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c78e3a8d114508c32550c15e5eb0e975ea4f60098f2db066b70933ab1460fe72->enter($__internal_c78e3a8d114508c32550c15e5eb0e975ea4f60098f2db066b70933ab1460fe72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        $__internal_7e2b82aeb77f17ec3bf668f9469f0441b5c8801a7a2b1fbc5a629afabc6b2f52 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e2b82aeb77f17ec3bf668f9469f0441b5c8801a7a2b1fbc5a629afabc6b2f52->enter($__internal_7e2b82aeb77f17ec3bf668f9469f0441b5c8801a7a2b1fbc5a629afabc6b2f52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        // line 377
        echo "    ";
        ob_start();
        // line 378
        echo "        <input type=\"radio\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (array_key_exists("value", $context)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "\"";
        }
        if ((isset($context["checked"]) ? $context["checked"] : $this->getContext($context, "checked"))) {
            echo " checked=\"checked\"";
        }
        echo " />
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_7e2b82aeb77f17ec3bf668f9469f0441b5c8801a7a2b1fbc5a629afabc6b2f52->leave($__internal_7e2b82aeb77f17ec3bf668f9469f0441b5c8801a7a2b1fbc5a629afabc6b2f52_prof);

        
        $__internal_c78e3a8d114508c32550c15e5eb0e975ea4f60098f2db066b70933ab1460fe72->leave($__internal_c78e3a8d114508c32550c15e5eb0e975ea4f60098f2db066b70933ab1460fe72_prof);

    }

    // line 382
    public function block_datetime_widget($context, array $blocks = array())
    {
        $__internal_e498c70b1735886b0c5db4227e8be28930300d8abd65a5199a10e1810bf131d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e498c70b1735886b0c5db4227e8be28930300d8abd65a5199a10e1810bf131d3->enter($__internal_e498c70b1735886b0c5db4227e8be28930300d8abd65a5199a10e1810bf131d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        $__internal_331ac0b7d925abf93fab1aee5f43e09da6a2a559caff22f0ee4b6b5eacfc9ae9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_331ac0b7d925abf93fab1aee5f43e09da6a2a559caff22f0ee4b6b5eacfc9ae9->enter($__internal_331ac0b7d925abf93fab1aee5f43e09da6a2a559caff22f0ee4b6b5eacfc9ae9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        // line 383
        echo "    ";
        ob_start();
        // line 384
        echo "        ";
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 385
            echo "            ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
        ";
        } else {
            // line 387
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => "bootstrap-datetime"));
            // line 388
            echo "            <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
                ";
            // line 389
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date", array()), 'widget');
            echo "
                ";
            // line 390
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "time", array()), 'widget');
            echo "
                ";
            // line 391
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date", array()), 'errors');
            echo "
                ";
            // line 392
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "time", array()), 'errors');
            echo "
            </div>
        ";
        }
        // line 395
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_331ac0b7d925abf93fab1aee5f43e09da6a2a559caff22f0ee4b6b5eacfc9ae9->leave($__internal_331ac0b7d925abf93fab1aee5f43e09da6a2a559caff22f0ee4b6b5eacfc9ae9_prof);

        
        $__internal_e498c70b1735886b0c5db4227e8be28930300d8abd65a5199a10e1810bf131d3->leave($__internal_e498c70b1735886b0c5db4227e8be28930300d8abd65a5199a10e1810bf131d3_prof);

    }

    // line 398
    public function block_date_widget($context, array $blocks = array())
    {
        $__internal_8d2db4853b7ab14943c5c902533dc32b8d515638de79facb8c9d59fc61db6a1d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d2db4853b7ab14943c5c902533dc32b8d515638de79facb8c9d59fc61db6a1d->enter($__internal_8d2db4853b7ab14943c5c902533dc32b8d515638de79facb8c9d59fc61db6a1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        $__internal_0c083c44499f16de8f33ee0e71d946f303fa5f334b57842a0a654984c3d44d26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0c083c44499f16de8f33ee0e71d946f303fa5f334b57842a0a654984c3d44d26->enter($__internal_0c083c44499f16de8f33ee0e71d946f303fa5f334b57842a0a654984c3d44d26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        // line 399
        echo "    ";
        ob_start();
        // line 400
        echo "        ";
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 401
            echo "            ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
        ";
        } else {
            // line 403
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => "bootstrap-date"));
            // line 404
            echo "            <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
                ";
            // line 405
            echo twig_replace_filter((isset($context["date_pattern"]) ? $context["date_pattern"] : $this->getContext($context, "date_pattern")), array("{{ year }}" =>             // line 406
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "year", array()), 'widget'), "{{ month }}" =>             // line 407
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "month", array()), 'widget'), "{{ day }}" =>             // line 408
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "day", array()), 'widget')));
            // line 409
            echo "
            </div>
        ";
        }
        // line 412
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_0c083c44499f16de8f33ee0e71d946f303fa5f334b57842a0a654984c3d44d26->leave($__internal_0c083c44499f16de8f33ee0e71d946f303fa5f334b57842a0a654984c3d44d26_prof);

        
        $__internal_8d2db4853b7ab14943c5c902533dc32b8d515638de79facb8c9d59fc61db6a1d->leave($__internal_8d2db4853b7ab14943c5c902533dc32b8d515638de79facb8c9d59fc61db6a1d_prof);

    }

    // line 415
    public function block_time_widget($context, array $blocks = array())
    {
        $__internal_7a691b9488d60a562df0d519034fd5f246772c46faa54e6860c44517480d37a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7a691b9488d60a562df0d519034fd5f246772c46faa54e6860c44517480d37a6->enter($__internal_7a691b9488d60a562df0d519034fd5f246772c46faa54e6860c44517480d37a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        $__internal_1580ff0cd476cfb531e759af772275b2f44e758e8cb8bdef04ced83f525e36fc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1580ff0cd476cfb531e759af772275b2f44e758e8cb8bdef04ced83f525e36fc->enter($__internal_1580ff0cd476cfb531e759af772275b2f44e758e8cb8bdef04ced83f525e36fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        // line 416
        echo "    ";
        ob_start();
        // line 417
        echo "        ";
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 418
            echo "            ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
        ";
        } else {
            // line 420
            echo "            ";
            $context["vars"] = ((((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "text")) ? (array("attr" => array("size" => 1))) : (array()));
            // line 421
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => "bootstrap-time"));
            // line 422
            echo "            <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
                ";
            // line 423
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "hour", array()), 'widget', (isset($context["vars"]) ? $context["vars"] : $this->getContext($context, "vars")));
            echo "
                ";
            // line 424
            if ((isset($context["with_minutes"]) ? $context["with_minutes"] : $this->getContext($context, "with_minutes"))) {
                echo ":";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "minute", array()), 'widget', (isset($context["vars"]) ? $context["vars"] : $this->getContext($context, "vars")));
            }
            // line 425
            echo "                ";
            if ((isset($context["with_seconds"]) ? $context["with_seconds"] : $this->getContext($context, "with_seconds"))) {
                echo ":";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "second", array()), 'widget', (isset($context["vars"]) ? $context["vars"] : $this->getContext($context, "vars")));
            }
            // line 426
            echo "            </div>
        ";
        }
        // line 428
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_1580ff0cd476cfb531e759af772275b2f44e758e8cb8bdef04ced83f525e36fc->leave($__internal_1580ff0cd476cfb531e759af772275b2f44e758e8cb8bdef04ced83f525e36fc_prof);

        
        $__internal_7a691b9488d60a562df0d519034fd5f246772c46faa54e6860c44517480d37a6->leave($__internal_7a691b9488d60a562df0d519034fd5f246772c46faa54e6860c44517480d37a6_prof);

    }

    // line 431
    public function block_number_widget($context, array $blocks = array())
    {
        $__internal_0ad6e7e4ca9ca869e7b782439fce9f2ee7bf8920408397cefcfd6c21a7fd0799 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ad6e7e4ca9ca869e7b782439fce9f2ee7bf8920408397cefcfd6c21a7fd0799->enter($__internal_0ad6e7e4ca9ca869e7b782439fce9f2ee7bf8920408397cefcfd6c21a7fd0799_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "number_widget"));

        $__internal_0f02bf66f199b4a5e675e9383c7f32773c87c2b2c46a5c24316bb11803c9399e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0f02bf66f199b4a5e675e9383c7f32773c87c2b2c46a5c24316bb11803c9399e->enter($__internal_0f02bf66f199b4a5e675e9383c7f32773c87c2b2c46a5c24316bb11803c9399e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "number_widget"));

        // line 432
        echo "    ";
        ob_start();
        // line 433
        echo "        ";
        // line 434
        echo "        ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "text")) : ("text"));
        // line 435
        echo "        ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_0f02bf66f199b4a5e675e9383c7f32773c87c2b2c46a5c24316bb11803c9399e->leave($__internal_0f02bf66f199b4a5e675e9383c7f32773c87c2b2c46a5c24316bb11803c9399e_prof);

        
        $__internal_0ad6e7e4ca9ca869e7b782439fce9f2ee7bf8920408397cefcfd6c21a7fd0799->leave($__internal_0ad6e7e4ca9ca869e7b782439fce9f2ee7bf8920408397cefcfd6c21a7fd0799_prof);

    }

    // line 439
    public function block_integer_widget($context, array $blocks = array())
    {
        $__internal_d59a49bff6bd4c2dd3bfa10e6fffcf45daeb42120371bead173df7567f1ab850 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d59a49bff6bd4c2dd3bfa10e6fffcf45daeb42120371bead173df7567f1ab850->enter($__internal_d59a49bff6bd4c2dd3bfa10e6fffcf45daeb42120371bead173df7567f1ab850_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "integer_widget"));

        $__internal_a042221569d9a0d65ce3c9017aa00316d729859dc2ae10f184cc84a1f11bc258 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a042221569d9a0d65ce3c9017aa00316d729859dc2ae10f184cc84a1f11bc258->enter($__internal_a042221569d9a0d65ce3c9017aa00316d729859dc2ae10f184cc84a1f11bc258_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "integer_widget"));

        // line 440
        echo "    ";
        ob_start();
        // line 441
        echo "        ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "number")) : ("number"));
        // line 442
        echo "        ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_a042221569d9a0d65ce3c9017aa00316d729859dc2ae10f184cc84a1f11bc258->leave($__internal_a042221569d9a0d65ce3c9017aa00316d729859dc2ae10f184cc84a1f11bc258_prof);

        
        $__internal_d59a49bff6bd4c2dd3bfa10e6fffcf45daeb42120371bead173df7567f1ab850->leave($__internal_d59a49bff6bd4c2dd3bfa10e6fffcf45daeb42120371bead173df7567f1ab850_prof);

    }

    // line 446
    public function block_money_widget($context, array $blocks = array())
    {
        $__internal_e46341c76da3ac6f5790d1405bfc27a0e9d1adab54f14cf4797a38a76d98d4b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e46341c76da3ac6f5790d1405bfc27a0e9d1adab54f14cf4797a38a76d98d4b1->enter($__internal_e46341c76da3ac6f5790d1405bfc27a0e9d1adab54f14cf4797a38a76d98d4b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        $__internal_fd142e2fe1922d8ebf60886ca0919a429960a4dec3bef9dae270316a43ec28d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd142e2fe1922d8ebf60886ca0919a429960a4dec3bef9dae270316a43ec28d3->enter($__internal_fd142e2fe1922d8ebf60886ca0919a429960a4dec3bef9dae270316a43ec28d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        // line 447
        echo "    ";
        ob_start();
        // line 448
        echo "        <div class=\"input-group\">
            ";
        // line 449
        echo twig_replace_filter((isset($context["money_pattern"]) ? $context["money_pattern"] : $this->getContext($context, "money_pattern")), array("{{ widget }}" =>         $this->renderBlock("form_widget_simple", $context, $blocks), "{{ tag_start }}" => "<span class=\"input-group-addon\">", "{{ tag_end }}" => "</span>"));
        // line 453
        echo "
        </div>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_fd142e2fe1922d8ebf60886ca0919a429960a4dec3bef9dae270316a43ec28d3->leave($__internal_fd142e2fe1922d8ebf60886ca0919a429960a4dec3bef9dae270316a43ec28d3_prof);

        
        $__internal_e46341c76da3ac6f5790d1405bfc27a0e9d1adab54f14cf4797a38a76d98d4b1->leave($__internal_e46341c76da3ac6f5790d1405bfc27a0e9d1adab54f14cf4797a38a76d98d4b1_prof);

    }

    // line 458
    public function block_url_widget($context, array $blocks = array())
    {
        $__internal_852909fc5f9568411a7173fcc7705255cdc9f4b5e74f4208d55dd0205ec64270 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_852909fc5f9568411a7173fcc7705255cdc9f4b5e74f4208d55dd0205ec64270->enter($__internal_852909fc5f9568411a7173fcc7705255cdc9f4b5e74f4208d55dd0205ec64270_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "url_widget"));

        $__internal_17bb5ee912bb183230b8ec33fac738f39f971ced2bc071567c3cc4d1c9a39e89 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_17bb5ee912bb183230b8ec33fac738f39f971ced2bc071567c3cc4d1c9a39e89->enter($__internal_17bb5ee912bb183230b8ec33fac738f39f971ced2bc071567c3cc4d1c9a39e89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "url_widget"));

        // line 459
        echo "    ";
        ob_start();
        // line 460
        echo "        ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "url")) : ("url"));
        // line 461
        echo "        ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_17bb5ee912bb183230b8ec33fac738f39f971ced2bc071567c3cc4d1c9a39e89->leave($__internal_17bb5ee912bb183230b8ec33fac738f39f971ced2bc071567c3cc4d1c9a39e89_prof);

        
        $__internal_852909fc5f9568411a7173fcc7705255cdc9f4b5e74f4208d55dd0205ec64270->leave($__internal_852909fc5f9568411a7173fcc7705255cdc9f4b5e74f4208d55dd0205ec64270_prof);

    }

    // line 465
    public function block_search_widget($context, array $blocks = array())
    {
        $__internal_0b86336ed380bb0f0337b1ef5d53aa15764c2ae0394fa3d14673a9b943e27d8e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0b86336ed380bb0f0337b1ef5d53aa15764c2ae0394fa3d14673a9b943e27d8e->enter($__internal_0b86336ed380bb0f0337b1ef5d53aa15764c2ae0394fa3d14673a9b943e27d8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "search_widget"));

        $__internal_21deaa9e58ad860399072409623e9fdf721b8c6f2bdcbdd03dfa2d08c004c78f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_21deaa9e58ad860399072409623e9fdf721b8c6f2bdcbdd03dfa2d08c004c78f->enter($__internal_21deaa9e58ad860399072409623e9fdf721b8c6f2bdcbdd03dfa2d08c004c78f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "search_widget"));

        // line 466
        echo "    ";
        ob_start();
        // line 467
        echo "        ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "search")) : ("search"));
        // line 468
        echo "        ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_21deaa9e58ad860399072409623e9fdf721b8c6f2bdcbdd03dfa2d08c004c78f->leave($__internal_21deaa9e58ad860399072409623e9fdf721b8c6f2bdcbdd03dfa2d08c004c78f_prof);

        
        $__internal_0b86336ed380bb0f0337b1ef5d53aa15764c2ae0394fa3d14673a9b943e27d8e->leave($__internal_0b86336ed380bb0f0337b1ef5d53aa15764c2ae0394fa3d14673a9b943e27d8e_prof);

    }

    // line 472
    public function block_percent_widget($context, array $blocks = array())
    {
        $__internal_6ab6c92471baac3a1f8e84da2a0a393b9959a1e87b2d2afe88dfad75bc69476d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6ab6c92471baac3a1f8e84da2a0a393b9959a1e87b2d2afe88dfad75bc69476d->enter($__internal_6ab6c92471baac3a1f8e84da2a0a393b9959a1e87b2d2afe88dfad75bc69476d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        $__internal_f70e70bdef2dc164fad322531eb8db62b5d6b172a641d227fcd9aa4c3b7fbc5a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f70e70bdef2dc164fad322531eb8db62b5d6b172a641d227fcd9aa4c3b7fbc5a->enter($__internal_f70e70bdef2dc164fad322531eb8db62b5d6b172a641d227fcd9aa4c3b7fbc5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        // line 473
        echo "    ";
        ob_start();
        // line 474
        echo "        ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "text")) : ("text"));
        // line 475
        echo "        <div class=\"input-group\">
            ";
        // line 476
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
            <span class=\"input-group-addon\">%</span>
        </div>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_f70e70bdef2dc164fad322531eb8db62b5d6b172a641d227fcd9aa4c3b7fbc5a->leave($__internal_f70e70bdef2dc164fad322531eb8db62b5d6b172a641d227fcd9aa4c3b7fbc5a_prof);

        
        $__internal_6ab6c92471baac3a1f8e84da2a0a393b9959a1e87b2d2afe88dfad75bc69476d->leave($__internal_6ab6c92471baac3a1f8e84da2a0a393b9959a1e87b2d2afe88dfad75bc69476d_prof);

    }

    // line 482
    public function block_password_widget($context, array $blocks = array())
    {
        $__internal_8e6fdb9365ca2632de20ecff1f0101a37e2444e4fe975f71c8a0e42bddd95176 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e6fdb9365ca2632de20ecff1f0101a37e2444e4fe975f71c8a0e42bddd95176->enter($__internal_8e6fdb9365ca2632de20ecff1f0101a37e2444e4fe975f71c8a0e42bddd95176_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "password_widget"));

        $__internal_6d10e6bf01f34738aae2f388881709bbf42550876f20213070a182e3d094b14a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d10e6bf01f34738aae2f388881709bbf42550876f20213070a182e3d094b14a->enter($__internal_6d10e6bf01f34738aae2f388881709bbf42550876f20213070a182e3d094b14a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "password_widget"));

        // line 483
        echo "    ";
        ob_start();
        // line 484
        echo "        ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "password")) : ("password"));
        // line 485
        echo "        ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_6d10e6bf01f34738aae2f388881709bbf42550876f20213070a182e3d094b14a->leave($__internal_6d10e6bf01f34738aae2f388881709bbf42550876f20213070a182e3d094b14a_prof);

        
        $__internal_8e6fdb9365ca2632de20ecff1f0101a37e2444e4fe975f71c8a0e42bddd95176->leave($__internal_8e6fdb9365ca2632de20ecff1f0101a37e2444e4fe975f71c8a0e42bddd95176_prof);

    }

    // line 489
    public function block_hidden_widget($context, array $blocks = array())
    {
        $__internal_7fcb11e55f13927bc9fc90c2447e5282514380484f203aa6c934f278ca46ea36 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7fcb11e55f13927bc9fc90c2447e5282514380484f203aa6c934f278ca46ea36->enter($__internal_7fcb11e55f13927bc9fc90c2447e5282514380484f203aa6c934f278ca46ea36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_widget"));

        $__internal_a4dec3aaca0d0e6fecb718325a6423a135a71e873e92caca7a462dfdd557313d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4dec3aaca0d0e6fecb718325a6423a135a71e873e92caca7a462dfdd557313d->enter($__internal_a4dec3aaca0d0e6fecb718325a6423a135a71e873e92caca7a462dfdd557313d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_widget"));

        // line 490
        echo "    ";
        ob_start();
        // line 491
        echo "        ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "hidden")) : ("hidden"));
        // line 492
        echo "        ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_a4dec3aaca0d0e6fecb718325a6423a135a71e873e92caca7a462dfdd557313d->leave($__internal_a4dec3aaca0d0e6fecb718325a6423a135a71e873e92caca7a462dfdd557313d_prof);

        
        $__internal_7fcb11e55f13927bc9fc90c2447e5282514380484f203aa6c934f278ca46ea36->leave($__internal_7fcb11e55f13927bc9fc90c2447e5282514380484f203aa6c934f278ca46ea36_prof);

    }

    // line 496
    public function block_email_widget($context, array $blocks = array())
    {
        $__internal_cf893a303d82e8c066ffb70d6edff9f92cd8845f0c6524634008f149648496c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf893a303d82e8c066ffb70d6edff9f92cd8845f0c6524634008f149648496c4->enter($__internal_cf893a303d82e8c066ffb70d6edff9f92cd8845f0c6524634008f149648496c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "email_widget"));

        $__internal_631b0b0a7226bebfff64917811939b1e6450d36e86ad0493f672a2dd2777f170 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_631b0b0a7226bebfff64917811939b1e6450d36e86ad0493f672a2dd2777f170->enter($__internal_631b0b0a7226bebfff64917811939b1e6450d36e86ad0493f672a2dd2777f170_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "email_widget"));

        // line 497
        echo "    ";
        ob_start();
        // line 498
        echo "        ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "email")) : ("email"));
        // line 499
        echo "        ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_631b0b0a7226bebfff64917811939b1e6450d36e86ad0493f672a2dd2777f170->leave($__internal_631b0b0a7226bebfff64917811939b1e6450d36e86ad0493f672a2dd2777f170_prof);

        
        $__internal_cf893a303d82e8c066ffb70d6edff9f92cd8845f0c6524634008f149648496c4->leave($__internal_cf893a303d82e8c066ffb70d6edff9f92cd8845f0c6524634008f149648496c4_prof);

    }

    // line 503
    public function block_button_widget($context, array $blocks = array())
    {
        $__internal_9c0952b3b89febebc44e767d915a13da580f4772e2ff7d380849542e3b42baa6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9c0952b3b89febebc44e767d915a13da580f4772e2ff7d380849542e3b42baa6->enter($__internal_9c0952b3b89febebc44e767d915a13da580f4772e2ff7d380849542e3b42baa6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        $__internal_15f5449e9874c9f7ee7ce2a1feb8ac014ff52369a6f342bdc23af605c56c80ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15f5449e9874c9f7ee7ce2a1feb8ac014ff52369a6f342bdc23af605c56c80ef->enter($__internal_15f5449e9874c9f7ee7ce2a1feb8ac014ff52369a6f342bdc23af605c56c80ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        // line 504
        echo "    ";
        ob_start();
        // line 505
        echo "        ";
        if ((twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"))) &&  !((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false))) {
            // line 506
            echo "            ";
            $context["label"] = $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")));
            // line 507
            echo "        ";
        }
        // line 508
        echo "        ";
        if ((array_key_exists("type", $context) && ((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")) == "submit"))) {
            // line 509
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter((((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " btn btn-") . ((array_key_exists("button_class", $context)) ? (_twig_default_filter((isset($context["button_class"]) ? $context["button_class"] : $this->getContext($context, "button_class")), "primary")) : ("primary"))))));
            // line 510
            echo "        ";
        } else {
            // line 511
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter((((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " btn btn-") . ((array_key_exists("button_class", $context)) ? (_twig_default_filter((isset($context["button_class"]) ? $context["button_class"] : $this->getContext($context, "button_class")), "default")) : ("default"))))));
            // line 512
            echo "        ";
        }
        // line 513
        echo "        ";
        if ((array_key_exists("as_link", $context) && ((isset($context["as_link"]) ? $context["as_link"] : $this->getContext($context, "as_link")) == true))) {
            // line 514
            echo "            <a ";
            $this->displayBlock("button_attributes", $context, $blocks);
            echo ">";
            if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "icon", array(), "any", true, true) && ($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "icon", array()) != ""))) {
                echo $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapIconExtension')->iconFunction($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "icon", array()));
            }
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
            echo "</a>
        ";
        } else {
            // line 516
            echo "            <button type=\"";
            echo twig_escape_filter($this->env, ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "button")) : ("button")), "html", null, true);
            echo "\" ";
            $this->displayBlock("button_attributes", $context, $blocks);
            echo ">";
            if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "icon", array(), "any", true, true) && ($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "icon", array()) != ""))) {
                echo $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapIconExtension')->iconFunction($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "icon", array()));
            }
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
            echo "</button>
        ";
        }
        // line 518
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_15f5449e9874c9f7ee7ce2a1feb8ac014ff52369a6f342bdc23af605c56c80ef->leave($__internal_15f5449e9874c9f7ee7ce2a1feb8ac014ff52369a6f342bdc23af605c56c80ef_prof);

        
        $__internal_9c0952b3b89febebc44e767d915a13da580f4772e2ff7d380849542e3b42baa6->leave($__internal_9c0952b3b89febebc44e767d915a13da580f4772e2ff7d380849542e3b42baa6_prof);

    }

    // line 521
    public function block_submit_widget($context, array $blocks = array())
    {
        $__internal_48637ea5d5a9f5865f9abddf7b160ba9852279af9f7aac58adfb758e08efea64 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_48637ea5d5a9f5865f9abddf7b160ba9852279af9f7aac58adfb758e08efea64->enter($__internal_48637ea5d5a9f5865f9abddf7b160ba9852279af9f7aac58adfb758e08efea64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_widget"));

        $__internal_e769ee2d841980170b97ec2bda4ec0e130fcad4ba50c2d1f04721b0b027ad97c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e769ee2d841980170b97ec2bda4ec0e130fcad4ba50c2d1f04721b0b027ad97c->enter($__internal_e769ee2d841980170b97ec2bda4ec0e130fcad4ba50c2d1f04721b0b027ad97c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_widget"));

        // line 522
        echo "    ";
        ob_start();
        // line 523
        echo "        ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "submit")) : ("submit"));
        // line 524
        echo "        ";
        $this->displayBlock("button_widget", $context, $blocks);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_e769ee2d841980170b97ec2bda4ec0e130fcad4ba50c2d1f04721b0b027ad97c->leave($__internal_e769ee2d841980170b97ec2bda4ec0e130fcad4ba50c2d1f04721b0b027ad97c_prof);

        
        $__internal_48637ea5d5a9f5865f9abddf7b160ba9852279af9f7aac58adfb758e08efea64->leave($__internal_48637ea5d5a9f5865f9abddf7b160ba9852279af9f7aac58adfb758e08efea64_prof);

    }

    // line 528
    public function block_reset_widget($context, array $blocks = array())
    {
        $__internal_0bbcd7f0511608ef35d8e5110a3d6fdbdf51496a9a2b7980545b4994e7f5c35f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0bbcd7f0511608ef35d8e5110a3d6fdbdf51496a9a2b7980545b4994e7f5c35f->enter($__internal_0bbcd7f0511608ef35d8e5110a3d6fdbdf51496a9a2b7980545b4994e7f5c35f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_widget"));

        $__internal_686dfe4de04d30331a57996fff8c3b0eeaf65af97d7fcb16c2cefb839d1935f3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_686dfe4de04d30331a57996fff8c3b0eeaf65af97d7fcb16c2cefb839d1935f3->enter($__internal_686dfe4de04d30331a57996fff8c3b0eeaf65af97d7fcb16c2cefb839d1935f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_widget"));

        // line 529
        echo "    ";
        ob_start();
        // line 530
        echo "        ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "reset")) : ("reset"));
        // line 531
        echo "        ";
        $this->displayBlock("button_widget", $context, $blocks);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_686dfe4de04d30331a57996fff8c3b0eeaf65af97d7fcb16c2cefb839d1935f3->leave($__internal_686dfe4de04d30331a57996fff8c3b0eeaf65af97d7fcb16c2cefb839d1935f3_prof);

        
        $__internal_0bbcd7f0511608ef35d8e5110a3d6fdbdf51496a9a2b7980545b4994e7f5c35f->leave($__internal_0bbcd7f0511608ef35d8e5110a3d6fdbdf51496a9a2b7980545b4994e7f5c35f_prof);

    }

    // line 535
    public function block_form_actions_widget($context, array $blocks = array())
    {
        $__internal_1684a57764605742e6375c9bf3e67ee65e029044353852b189dda4a25234ea6c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1684a57764605742e6375c9bf3e67ee65e029044353852b189dda4a25234ea6c->enter($__internal_1684a57764605742e6375c9bf3e67ee65e029044353852b189dda4a25234ea6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_actions_widget"));

        $__internal_d339292ddd710af49a6ea4809dd81c14c09c4e4857e6960bd2cae987a8fc7073 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d339292ddd710af49a6ea4809dd81c14c09c4e4857e6960bd2cae987a8fc7073->enter($__internal_d339292ddd710af49a6ea4809dd81c14c09c4e4857e6960bd2cae987a8fc7073_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_actions_widget"));

        // line 536
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "children", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["button"]) {
            // line 537
            echo "        ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["button"], 'widget');
            echo "&nbsp; ";
            // line 538
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['button'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_d339292ddd710af49a6ea4809dd81c14c09c4e4857e6960bd2cae987a8fc7073->leave($__internal_d339292ddd710af49a6ea4809dd81c14c09c4e4857e6960bd2cae987a8fc7073_prof);

        
        $__internal_1684a57764605742e6375c9bf3e67ee65e029044353852b189dda4a25234ea6c->leave($__internal_1684a57764605742e6375c9bf3e67ee65e029044353852b189dda4a25234ea6c_prof);

    }

    // line 541
    public function block_bs_static_widget($context, array $blocks = array())
    {
        $__internal_eff80d84107f46713ad6d36956cb044c7d730cf3843e28666e3943aa0c3da922 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eff80d84107f46713ad6d36956cb044c7d730cf3843e28666e3943aa0c3da922->enter($__internal_eff80d84107f46713ad6d36956cb044c7d730cf3843e28666e3943aa0c3da922_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bs_static_widget"));

        $__internal_2319dc83a052132abdb71448d09dfc241a159913f617b3613665efb231a0111b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2319dc83a052132abdb71448d09dfc241a159913f617b3613665efb231a0111b->enter($__internal_2319dc83a052132abdb71448d09dfc241a159913f617b3613665efb231a0111b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bs_static_widget"));

        // line 542
        echo "    ";
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control-static"))));
        // line 543
        echo "    <p id=\"";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "\"";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">";
        echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
        echo "</p>
";
        
        $__internal_2319dc83a052132abdb71448d09dfc241a159913f617b3613665efb231a0111b->leave($__internal_2319dc83a052132abdb71448d09dfc241a159913f617b3613665efb231a0111b_prof);

        
        $__internal_eff80d84107f46713ad6d36956cb044c7d730cf3843e28666e3943aa0c3da922->leave($__internal_eff80d84107f46713ad6d36956cb044c7d730cf3843e28666e3943aa0c3da922_prof);

    }

    // line 548
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_25592f170f13d6220385c8ee7e4732934ea7321cb5c8fe5b9b030a6dc2b34eaa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_25592f170f13d6220385c8ee7e4732934ea7321cb5c8fe5b9b030a6dc2b34eaa->enter($__internal_25592f170f13d6220385c8ee7e4732934ea7321cb5c8fe5b9b030a6dc2b34eaa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_a0c290d5af4388ee33b0341746a8f6ae51a314dc81ec3e5410606c27bcaaeb42 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a0c290d5af4388ee33b0341746a8f6ae51a314dc81ec3e5410606c27bcaaeb42->enter($__internal_a0c290d5af4388ee33b0341746a8f6ae51a314dc81ec3e5410606c27bcaaeb42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 549
        echo "    ";
        ob_start();
        // line 550
        echo "        ";
        $context["col_size"] = ((array_key_exists("col_size", $context)) ? (_twig_default_filter((isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize()));
        // line 551
        echo "
        ";
        // line 552
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "label_col", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "label_col", array())))) {
            // line 553
            echo "            ";
            $context["label_col"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "label_col", array());
            // line 554
            echo "        ";
        }
        // line 555
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_col", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "widget_col", array())))) {
            // line 556
            echo "            ";
            $context["widget_col"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "widget_col", array());
            // line 557
            echo "        ";
        }
        // line 558
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "col_size", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array())))) {
            // line 559
            echo "            ";
            $context["col_size"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array());
            // line 560
            echo "        ";
        }
        // line 561
        echo "
        ";
        // line 562
        if ( !((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false)) {
            // line 563
            echo "            ";
            $context["style"] = ((array_key_exists("style", $context)) ? (_twig_default_filter((isset($context["style"]) ? $context["style"] : $this->getContext($context, "style")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getStyle())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getStyle()));
            // line 564
            echo "            ";
            $context["label_col"] = ((array_key_exists("label_col", $context)) ? (_twig_default_filter((isset($context["label_col"]) ? $context["label_col"] : $this->getContext($context, "label_col")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getLabelCol())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getLabelCol()));
            // line 565
            echo "
            ";
            // line 566
            if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "style", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array())))) {
                // line 567
                echo "                ";
                $context["style"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array());
                // line 568
                echo "            ";
            }
            // line 569
            echo "
            ";
            // line 570
            $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " control-label"))));
            // line 571
            echo "            ";
            if (((isset($context["style"]) ? $context["style"] : $this->getContext($context, "style")) == "horizontal")) {
                // line 572
                echo "                ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter((((((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " col-") . (isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size"))) . "-") . (isset($context["label_col"]) ? $context["label_col"] : $this->getContext($context, "label_col"))))));
                // line 573
                echo "            ";
            } elseif (((isset($context["style"]) ? $context["style"] : $this->getContext($context, "style")) == "inline")) {
                // line 574
                echo "                ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " sr-only"))));
                // line 575
                echo "            ";
            }
            // line 576
            echo "
            ";
            // line 577
            if ( !(isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound"))) {
                // line 578
                echo "                ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("for" => (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))));
                // line 579
                echo "            ";
            }
            // line 580
            echo "            ";
            if ((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) {
                // line 581
                echo "                ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " required"))));
                // line 582
                echo "            ";
            }
            // line 583
            echo "            ";
            if (twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")))) {
                // line 584
                echo "                ";
                $context["label"] = $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")));
                // line 585
                echo "            ";
            }
            // line 586
            echo "            <label";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")));
            echo "</label>
        ";
        }
        // line 588
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_a0c290d5af4388ee33b0341746a8f6ae51a314dc81ec3e5410606c27bcaaeb42->leave($__internal_a0c290d5af4388ee33b0341746a8f6ae51a314dc81ec3e5410606c27bcaaeb42_prof);

        
        $__internal_25592f170f13d6220385c8ee7e4732934ea7321cb5c8fe5b9b030a6dc2b34eaa->leave($__internal_25592f170f13d6220385c8ee7e4732934ea7321cb5c8fe5b9b030a6dc2b34eaa_prof);

    }

    // line 591
    public function block_button_label($context, array $blocks = array())
    {
        $__internal_fb54f3b7d54b07b35dcf933a3a240e543b1a7c4688802a51f884b95f7ddb5c58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb54f3b7d54b07b35dcf933a3a240e543b1a7c4688802a51f884b95f7ddb5c58->enter($__internal_fb54f3b7d54b07b35dcf933a3a240e543b1a7c4688802a51f884b95f7ddb5c58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_label"));

        $__internal_14a40511517a324c210cd02b868d905a4018830997b8f069f0f322b74166ab4e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_14a40511517a324c210cd02b868d905a4018830997b8f069f0f322b74166ab4e->enter($__internal_14a40511517a324c210cd02b868d905a4018830997b8f069f0f322b74166ab4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_label"));

        
        $__internal_14a40511517a324c210cd02b868d905a4018830997b8f069f0f322b74166ab4e->leave($__internal_14a40511517a324c210cd02b868d905a4018830997b8f069f0f322b74166ab4e_prof);

        
        $__internal_fb54f3b7d54b07b35dcf933a3a240e543b1a7c4688802a51f884b95f7ddb5c58->leave($__internal_fb54f3b7d54b07b35dcf933a3a240e543b1a7c4688802a51f884b95f7ddb5c58_prof);

    }

    // line 595
    public function block_repeated_row($context, array $blocks = array())
    {
        $__internal_6a1a2cb9c6ec45ed925345b0cbee8e841b2c3d740f3b3c331e2cd558ad80c61a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6a1a2cb9c6ec45ed925345b0cbee8e841b2c3d740f3b3c331e2cd558ad80c61a->enter($__internal_6a1a2cb9c6ec45ed925345b0cbee8e841b2c3d740f3b3c331e2cd558ad80c61a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "repeated_row"));

        $__internal_70bac39b6d0423a7fe2ea46eb64510a6e1d7703920ac3d206a1897092c9d6970 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_70bac39b6d0423a7fe2ea46eb64510a6e1d7703920ac3d206a1897092c9d6970->enter($__internal_70bac39b6d0423a7fe2ea46eb64510a6e1d7703920ac3d206a1897092c9d6970_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "repeated_row"));

        // line 596
        echo "    ";
        ob_start();
        // line 597
        echo "        ";
        // line 601
        echo "        ";
        $this->displayBlock("form_rows", $context, $blocks);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_70bac39b6d0423a7fe2ea46eb64510a6e1d7703920ac3d206a1897092c9d6970->leave($__internal_70bac39b6d0423a7fe2ea46eb64510a6e1d7703920ac3d206a1897092c9d6970_prof);

        
        $__internal_6a1a2cb9c6ec45ed925345b0cbee8e841b2c3d740f3b3c331e2cd558ad80c61a->leave($__internal_6a1a2cb9c6ec45ed925345b0cbee8e841b2c3d740f3b3c331e2cd558ad80c61a_prof);

    }

    // line 605
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_39ffbcdcd7ae613bcf35a0dfec3f191a5630ed2ad42f5de0e942152e64c22e3a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_39ffbcdcd7ae613bcf35a0dfec3f191a5630ed2ad42f5de0e942152e64c22e3a->enter($__internal_39ffbcdcd7ae613bcf35a0dfec3f191a5630ed2ad42f5de0e942152e64c22e3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_237363316713091f2d0b21d883f0db849c3a41c150c8d2772f90b986c6844ab4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_237363316713091f2d0b21d883f0db849c3a41c150c8d2772f90b986c6844ab4->enter($__internal_237363316713091f2d0b21d883f0db849c3a41c150c8d2772f90b986c6844ab4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 606
        echo "    ";
        ob_start();
        // line 607
        echo "        ";
        $context["style"] = ((array_key_exists("style", $context)) ? (_twig_default_filter((isset($context["style"]) ? $context["style"] : $this->getContext($context, "style")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getStyle())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getStyle()));
        // line 608
        echo "        ";
        $context["col_size"] = ((array_key_exists("col_size", $context)) ? (_twig_default_filter((isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize()));
        // line 609
        echo "
        ";
        // line 610
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "label_col", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "label_col", array())))) {
            // line 611
            echo "            ";
            $context["label_col"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "label_col", array());
            // line 612
            echo "        ";
        }
        // line 613
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_col", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "widget_col", array())))) {
            // line 614
            echo "            ";
            $context["widget_col"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "widget_col", array());
            // line 615
            echo "            ";
            if ((((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false) && array_key_exists("label_col", $context))) {
                // line 616
                echo "                ";
                $context["widget_col"] = ((isset($context["widget_col"]) ? $context["widget_col"] : $this->getContext($context, "widget_col")) + (isset($context["label_col"]) ? $context["label_col"] : $this->getContext($context, "label_col")));
                // line 617
                echo "            ";
            }
            // line 618
            echo "        ";
        }
        // line 619
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "col_size", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array())))) {
            // line 620
            echo "            ";
            $context["col_size"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array());
            // line 621
            echo "        ";
        }
        // line 622
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "style", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array())))) {
            // line 623
            echo "            ";
            $context["style"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array());
            // line 624
            echo "        ";
        }
        // line 625
        echo "
        ";
        // line 626
        $context["label_col"] = ((array_key_exists("label_col", $context)) ? (_twig_default_filter((isset($context["label_col"]) ? $context["label_col"] : $this->getContext($context, "label_col")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getLabelCol())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getLabelCol()));
        // line 627
        echo "        ";
        $context["widget_col"] = ((array_key_exists("widget_col", $context)) ? (_twig_default_filter((isset($context["widget_col"]) ? $context["widget_col"] : $this->getContext($context, "widget_col")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getWidgetCol())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getWidgetCol()));
        // line 628
        echo "
        <div class=\"form-group";
        // line 629
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "errors", array())) > 0)) {
            echo " has-error";
        }
        echo "\">
            ";
        // line 630
        if (((isset($context["style"]) ? $context["style"] : $this->getContext($context, "style")) == "horizontal")) {
            // line 631
            echo "                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label');
            echo "
                <div class=\"col-";
            // line 632
            echo twig_escape_filter($this->env, (isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size")), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, (isset($context["widget_col"]) ? $context["widget_col"] : $this->getContext($context, "widget_col")), "html", null, true);
            echo "\">
                    ";
            // line 633
            $this->displayBlock("form_input_group", $context, $blocks);
            echo "
                    ";
            // line 634
            $this->displayBlock("form_help", $context, $blocks);
            echo "
                    ";
            // line 635
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
            echo "
                </div>
            ";
        } else {
            // line 638
            echo "                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label');
            echo "
                ";
            // line 639
            $this->displayBlock("form_input_group", $context, $blocks);
            echo "
                ";
            // line 640
            $this->displayBlock("form_help", $context, $blocks);
            echo "
                ";
            // line 641
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
            echo "
            ";
        }
        // line 643
        echo "        </div>

        ";
        // line 645
        if (((isset($context["style"]) ? $context["style"] : $this->getContext($context, "style")) == "inline")) {
            echo "&nbsp;";
        }
        // line 646
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_237363316713091f2d0b21d883f0db849c3a41c150c8d2772f90b986c6844ab4->leave($__internal_237363316713091f2d0b21d883f0db849c3a41c150c8d2772f90b986c6844ab4_prof);

        
        $__internal_39ffbcdcd7ae613bcf35a0dfec3f191a5630ed2ad42f5de0e942152e64c22e3a->leave($__internal_39ffbcdcd7ae613bcf35a0dfec3f191a5630ed2ad42f5de0e942152e64c22e3a_prof);

    }

    // line 649
    public function block_form_input_group($context, array $blocks = array())
    {
        $__internal_183daa82533df70a8d930876849ef6feab083f342502f16959265f0293551158 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_183daa82533df70a8d930876849ef6feab083f342502f16959265f0293551158->enter($__internal_183daa82533df70a8d930876849ef6feab083f342502f16959265f0293551158_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_input_group"));

        $__internal_bb812412b038351fcf79738bca150c0a8e5b54af98cd68dd65980a73e5ee2ca6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bb812412b038351fcf79738bca150c0a8e5b54af98cd68dd65980a73e5ee2ca6->enter($__internal_bb812412b038351fcf79738bca150c0a8e5b54af98cd68dd65980a73e5ee2ca6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_input_group"));

        // line 650
        echo "    ";
        ob_start();
        // line 651
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "input_group", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "input_group", array())))) {
            // line 652
            echo "            ";
            $context["input_group"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "input_group", array());
            // line 653
            echo "        ";
        }
        // line 654
        echo "        ";
        $context["input_group"] = ((array_key_exists("input_group", $context)) ? (_twig_default_filter((isset($context["input_group"]) ? $context["input_group"] : $this->getContext($context, "input_group")), array())) : (array()));
        // line 655
        echo "        ";
        if ( !twig_test_empty((isset($context["input_group"]) ? $context["input_group"] : $this->getContext($context, "input_group")))) {
            // line 656
            echo "            ";
            $context["ig_size_class"] = "";
            // line 657
            echo "            ";
            if (($this->getAttribute((isset($context["input_group"]) ? $context["input_group"] : null), "size", array(), "any", true, true) && ($this->getAttribute((isset($context["input_group"]) ? $context["input_group"] : $this->getContext($context, "input_group")), "size", array()) == "large"))) {
                // line 658
                echo "                ";
                $context["ig_size_class"] = " input-group-lg";
                // line 659
                echo "            ";
            }
            // line 660
            echo "            ";
            if (($this->getAttribute((isset($context["input_group"]) ? $context["input_group"] : null), "size", array(), "any", true, true) && ($this->getAttribute((isset($context["input_group"]) ? $context["input_group"] : $this->getContext($context, "input_group")), "size", array()) == "small"))) {
                // line 661
                echo "                ";
                $context["ig_size_class"] = " input-group-sm";
                // line 662
                echo "            ";
            }
            // line 663
            echo "            <div class=\"input-group";
            echo twig_escape_filter($this->env, (isset($context["ig_size_class"]) ? $context["ig_size_class"] : $this->getContext($context, "ig_size_class")), "html", null, true);
            echo "\">
                ";
            // line 664
            if (($this->getAttribute((isset($context["input_group"]) ? $context["input_group"] : null), "prepend", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["input_group"]) ? $context["input_group"] : $this->getContext($context, "input_group")), "prepend", array())))) {
                // line 665
                echo "                    <span class=\"input-group-addon\">";
                echo $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapIconExtension')->parseIconsFilter($this->getAttribute((isset($context["input_group"]) ? $context["input_group"] : $this->getContext($context, "input_group")), "prepend", array()));
                echo "</span>
                ";
            }
            // line 667
            echo "                ";
            if (($this->getAttribute((isset($context["input_group"]) ? $context["input_group"] : null), "button_prepend", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["input_group"]) ? $context["input_group"] : $this->getContext($context, "input_group")), "button_prepend", array())))) {
                // line 668
                echo "                    <span class=\"input-group-btn\">";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["input_group_button_prepend"]) ? $context["input_group_button_prepend"] : $this->getContext($context, "input_group_button_prepend")), 'widget');
                echo "</span>
                ";
            }
            // line 670
            echo "                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
            echo "
                ";
            // line 671
            if (($this->getAttribute((isset($context["input_group"]) ? $context["input_group"] : null), "button_append", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["input_group"]) ? $context["input_group"] : $this->getContext($context, "input_group")), "button_append", array())))) {
                // line 672
                echo "                    <span class=\"input-group-btn\">";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["input_group_button_append"]) ? $context["input_group_button_append"] : $this->getContext($context, "input_group_button_append")), 'widget');
                echo "</span>
                ";
            }
            // line 674
            echo "                ";
            if (($this->getAttribute((isset($context["input_group"]) ? $context["input_group"] : null), "append", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["input_group"]) ? $context["input_group"] : $this->getContext($context, "input_group")), "append", array())))) {
                // line 675
                echo "                    <span class=\"input-group-addon\">";
                echo $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapIconExtension')->parseIconsFilter($this->getAttribute((isset($context["input_group"]) ? $context["input_group"] : $this->getContext($context, "input_group")), "append", array()));
                echo "</span>
                ";
            }
            // line 677
            echo "            </div>
        ";
        } else {
            // line 679
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
            echo "
        ";
        }
        // line 681
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_bb812412b038351fcf79738bca150c0a8e5b54af98cd68dd65980a73e5ee2ca6->leave($__internal_bb812412b038351fcf79738bca150c0a8e5b54af98cd68dd65980a73e5ee2ca6_prof);

        
        $__internal_183daa82533df70a8d930876849ef6feab083f342502f16959265f0293551158->leave($__internal_183daa82533df70a8d930876849ef6feab083f342502f16959265f0293551158_prof);

    }

    // line 684
    public function block_form_help($context, array $blocks = array())
    {
        $__internal_2e31a85077b1a78bed9f9f7b4beda48790e6a3fef0e62f63565f4a95697b66e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e31a85077b1a78bed9f9f7b4beda48790e6a3fef0e62f63565f4a95697b66e8->enter($__internal_2e31a85077b1a78bed9f9f7b4beda48790e6a3fef0e62f63565f4a95697b66e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_help"));

        $__internal_8eaf96f27d62548f24883cd85b9ca4100c0ecb6c2aa784eed624024e2f9a4d26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8eaf96f27d62548f24883cd85b9ca4100c0ecb6c2aa784eed624024e2f9a4d26->enter($__internal_8eaf96f27d62548f24883cd85b9ca4100c0ecb6c2aa784eed624024e2f9a4d26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_help"));

        // line 685
        echo "    ";
        ob_start();
        // line 686
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "help_text", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "help_text", array())))) {
            // line 687
            echo "            ";
            $context["help_text"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "help_text", array());
            // line 688
            echo "        ";
        }
        // line 689
        echo "        ";
        $context["help_text"] = ((array_key_exists("help_text", $context)) ? (_twig_default_filter((isset($context["help_text"]) ? $context["help_text"] : $this->getContext($context, "help_text")), "")) : (""));
        // line 690
        echo "        ";
        if ( !twig_test_empty((isset($context["help_text"]) ? $context["help_text"] : $this->getContext($context, "help_text")))) {
            // line 691
            echo "            <span class=\"help-block\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["help_text"]) ? $context["help_text"] : $this->getContext($context, "help_text")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
            echo "</span>
        ";
        }
        // line 693
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_8eaf96f27d62548f24883cd85b9ca4100c0ecb6c2aa784eed624024e2f9a4d26->leave($__internal_8eaf96f27d62548f24883cd85b9ca4100c0ecb6c2aa784eed624024e2f9a4d26_prof);

        
        $__internal_2e31a85077b1a78bed9f9f7b4beda48790e6a3fef0e62f63565f4a95697b66e8->leave($__internal_2e31a85077b1a78bed9f9f7b4beda48790e6a3fef0e62f63565f4a95697b66e8_prof);

    }

    // line 696
    public function block_button_row($context, array $blocks = array())
    {
        $__internal_ee0544198c22afc254a9de66df1b431891e81ad340e74f1bfd7658de76b12974 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ee0544198c22afc254a9de66df1b431891e81ad340e74f1bfd7658de76b12974->enter($__internal_ee0544198c22afc254a9de66df1b431891e81ad340e74f1bfd7658de76b12974_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        $__internal_f49d782b48f08fea13656dc41d60ddd438e899060866b2dd983d1aa13919420e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f49d782b48f08fea13656dc41d60ddd438e899060866b2dd983d1aa13919420e->enter($__internal_f49d782b48f08fea13656dc41d60ddd438e899060866b2dd983d1aa13919420e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        // line 697
        echo "    ";
        ob_start();
        // line 698
        echo "        ";
        $context["style"] = ((array_key_exists("style", $context)) ? (_twig_default_filter((isset($context["style"]) ? $context["style"] : $this->getContext($context, "style")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getStyle())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getStyle()));
        // line 699
        echo "        ";
        $context["col_size"] = ((array_key_exists("col_size", $context)) ? (_twig_default_filter((isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize()));
        // line 700
        echo "
        ";
        // line 701
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "label_col", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "label_col", array())))) {
            // line 702
            echo "            ";
            $context["label_col"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "label_col", array());
            // line 703
            echo "        ";
        }
        // line 704
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_col", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "widget_col", array())))) {
            // line 705
            echo "            ";
            $context["widget_col"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "widget_col", array());
            // line 706
            echo "        ";
        }
        // line 707
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "col_size", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array())))) {
            // line 708
            echo "            ";
            $context["col_size"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array());
            // line 709
            echo "        ";
        }
        // line 710
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "style", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array())))) {
            // line 711
            echo "            ";
            $context["style"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array());
            // line 712
            echo "        ";
        }
        // line 713
        echo "
        ";
        // line 714
        $context["label_col"] = ((array_key_exists("label_col", $context)) ? (_twig_default_filter((isset($context["label_col"]) ? $context["label_col"] : $this->getContext($context, "label_col")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getLabelCol())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getLabelCol()));
        // line 715
        echo "        ";
        $context["widget_col"] = ((array_key_exists("widget_col", $context)) ? (_twig_default_filter((isset($context["widget_col"]) ? $context["widget_col"] : $this->getContext($context, "widget_col")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getWidgetCol())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getWidgetCol()));
        // line 716
        echo "
        <div class=\"form-group\">
            ";
        // line 718
        if (((isset($context["style"]) ? $context["style"] : $this->getContext($context, "style")) == "horizontal")) {
            // line 719
            echo "                <div class=\"col-";
            echo twig_escape_filter($this->env, (isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size")), "html", null, true);
            echo "-offset-";
            echo twig_escape_filter($this->env, (isset($context["label_col"]) ? $context["label_col"] : $this->getContext($context, "label_col")), "html", null, true);
            echo " col-";
            echo twig_escape_filter($this->env, (isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size")), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, (isset($context["widget_col"]) ? $context["widget_col"] : $this->getContext($context, "widget_col")), "html", null, true);
            echo "\">
            ";
        }
        // line 721
        echo "
            ";
        // line 722
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "

            ";
        // line 724
        if (((isset($context["style"]) ? $context["style"] : $this->getContext($context, "style")) == "horizontal")) {
            // line 725
            echo "                </div>
            ";
        }
        // line 727
        echo "        </div>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_f49d782b48f08fea13656dc41d60ddd438e899060866b2dd983d1aa13919420e->leave($__internal_f49d782b48f08fea13656dc41d60ddd438e899060866b2dd983d1aa13919420e_prof);

        
        $__internal_ee0544198c22afc254a9de66df1b431891e81ad340e74f1bfd7658de76b12974->leave($__internal_ee0544198c22afc254a9de66df1b431891e81ad340e74f1bfd7658de76b12974_prof);

    }

    // line 731
    public function block_hidden_row($context, array $blocks = array())
    {
        $__internal_0af49311c62fbf6bc65e6e8b892911318073e7d1b0875c2dd85b330cafa58a79 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0af49311c62fbf6bc65e6e8b892911318073e7d1b0875c2dd85b330cafa58a79->enter($__internal_0af49311c62fbf6bc65e6e8b892911318073e7d1b0875c2dd85b330cafa58a79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_row"));

        $__internal_a730c3b3f575a1979566597ae162ae541c895c5941c64793f27fa440de8a4992 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a730c3b3f575a1979566597ae162ae541c895c5941c64793f27fa440de8a4992->enter($__internal_a730c3b3f575a1979566597ae162ae541c895c5941c64793f27fa440de8a4992_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_row"));

        // line 732
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
";
        
        $__internal_a730c3b3f575a1979566597ae162ae541c895c5941c64793f27fa440de8a4992->leave($__internal_a730c3b3f575a1979566597ae162ae541c895c5941c64793f27fa440de8a4992_prof);

        
        $__internal_0af49311c62fbf6bc65e6e8b892911318073e7d1b0875c2dd85b330cafa58a79->leave($__internal_0af49311c62fbf6bc65e6e8b892911318073e7d1b0875c2dd85b330cafa58a79_prof);

    }

    // line 735
    public function block_form_actions_row($context, array $blocks = array())
    {
        $__internal_eb76933a4e9dea7ecb2eb57103391401f319ebc3470bdda8b7e78c26235b9073 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eb76933a4e9dea7ecb2eb57103391401f319ebc3470bdda8b7e78c26235b9073->enter($__internal_eb76933a4e9dea7ecb2eb57103391401f319ebc3470bdda8b7e78c26235b9073_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_actions_row"));

        $__internal_9cf92d11765072bcf151bdaf4a661203915931930077a568534746b4f7422c25 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9cf92d11765072bcf151bdaf4a661203915931930077a568534746b4f7422c25->enter($__internal_9cf92d11765072bcf151bdaf4a661203915931930077a568534746b4f7422c25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_actions_row"));

        // line 736
        echo "    ";
        $this->displayBlock("button_row", $context, $blocks);
        echo "
";
        
        $__internal_9cf92d11765072bcf151bdaf4a661203915931930077a568534746b4f7422c25->leave($__internal_9cf92d11765072bcf151bdaf4a661203915931930077a568534746b4f7422c25_prof);

        
        $__internal_eb76933a4e9dea7ecb2eb57103391401f319ebc3470bdda8b7e78c26235b9073->leave($__internal_eb76933a4e9dea7ecb2eb57103391401f319ebc3470bdda8b7e78c26235b9073_prof);

    }

    // line 741
    public function block_form($context, array $blocks = array())
    {
        $__internal_fc5e28d5f5c16ba4bebfa0a10bd4fb11178292d5be174df9a396838e535a748c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fc5e28d5f5c16ba4bebfa0a10bd4fb11178292d5be174df9a396838e535a748c->enter($__internal_fc5e28d5f5c16ba4bebfa0a10bd4fb11178292d5be174df9a396838e535a748c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        $__internal_24714b879d43e442fe02920bbb6ab91d9289f5cc8be45406ad395206adb4b54f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24714b879d43e442fe02920bbb6ab91d9289f5cc8be45406ad395206adb4b54f->enter($__internal_24714b879d43e442fe02920bbb6ab91d9289f5cc8be45406ad395206adb4b54f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        // line 742
        echo "    ";
        ob_start();
        // line 743
        echo "        ";
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 744
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        ";
        // line 745
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_24714b879d43e442fe02920bbb6ab91d9289f5cc8be45406ad395206adb4b54f->leave($__internal_24714b879d43e442fe02920bbb6ab91d9289f5cc8be45406ad395206adb4b54f_prof);

        
        $__internal_fc5e28d5f5c16ba4bebfa0a10bd4fb11178292d5be174df9a396838e535a748c->leave($__internal_fc5e28d5f5c16ba4bebfa0a10bd4fb11178292d5be174df9a396838e535a748c_prof);

    }

    // line 749
    public function block_form_start($context, array $blocks = array())
    {
        $__internal_3ba6c84124315d644c2633c03d3a54e2dd95c898215ae95f8820716f56783cf1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3ba6c84124315d644c2633c03d3a54e2dd95c898215ae95f8820716f56783cf1->enter($__internal_3ba6c84124315d644c2633c03d3a54e2dd95c898215ae95f8820716f56783cf1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_start"));

        $__internal_9b7fbfdd803908241afb3e135058fef2e54a9b61e25855963da5401f5cdfa6f9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b7fbfdd803908241afb3e135058fef2e54a9b61e25855963da5401f5cdfa6f9->enter($__internal_9b7fbfdd803908241afb3e135058fef2e54a9b61e25855963da5401f5cdfa6f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_start"));

        // line 750
        echo "    ";
        ob_start();
        // line 751
        echo "        ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->backupFormSettings(), "html", null, true);
        echo "
        ";
        // line 752
        $context["method"] = twig_upper_filter($this->env, (isset($context["method"]) ? $context["method"] : $this->getContext($context, "method")));
        // line 753
        echo "        ";
        if (twig_in_filter((isset($context["method"]) ? $context["method"] : $this->getContext($context, "method")), array(0 => "GET", 1 => "POST"))) {
            // line 754
            echo "            ";
            $context["form_method"] = (isset($context["method"]) ? $context["method"] : $this->getContext($context, "method"));
            // line 755
            echo "        ";
        } else {
            // line 756
            echo "            ";
            $context["form_method"] = "POST";
            // line 757
            echo "        ";
        }
        // line 758
        echo "
        ";
        // line 759
        if (array_key_exists("style", $context)) {
            // line 760
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter((((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-") . (isset($context["style"]) ? $context["style"] : $this->getContext($context, "style"))))));
            // line 761
            echo "            ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->setStyle((isset($context["style"]) ? $context["style"] : $this->getContext($context, "style"))), "html", null, true);
            echo "
        ";
        }
        // line 763
        echo "
        ";
        // line 764
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "errors", array())) > 0)) {
            // line 765
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " has-global-errors"))));
            // line 766
            echo "        ";
        }
        // line 767
        echo "
        ";
        // line 768
        if (array_key_exists("col_size", $context)) {
            // line 769
            echo "            ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->setColSize((isset($context["col_size"]) ? $context["col_size"] : $this->getContext($context, "col_size"))), "html", null, true);
            echo "
        ";
        }
        // line 771
        echo "
        ";
        // line 772
        if (array_key_exists("widget_col", $context)) {
            // line 773
            echo "            ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->setWidgetCol((isset($context["widget_col"]) ? $context["widget_col"] : $this->getContext($context, "widget_col"))), "html", null, true);
            echo "
        ";
        }
        // line 775
        echo "
        ";
        // line 776
        if (array_key_exists("label_col", $context)) {
            // line 777
            echo "            ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->setLabelCol((isset($context["label_col"]) ? $context["label_col"] : $this->getContext($context, "label_col"))), "html", null, true);
            echo "
        ";
        }
        // line 779
        echo "
        ";
        // line 780
        if (array_key_exists("simple_col", $context)) {
            // line 781
            echo "            ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->setSimpleCol((isset($context["simple_col"]) ? $context["simple_col"] : $this->getContext($context, "simple_col"))), "html", null, true);
            echo "
        ";
        }
        // line 783
        echo "
        ";
        // line 784
        if (( !$this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "role", array(), "any", true, true) || twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "role", array())))) {
            // line 785
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("role" => "form"));
            // line 786
            echo "        ";
        }
        // line 787
        echo "
        <form  name=\"";
        // line 788
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "name", array()), "html", null, true);
        echo "\" method=\"";
        echo twig_escape_filter($this->env, twig_lower_filter($this->env, (isset($context["form_method"]) ? $context["form_method"] : $this->getContext($context, "form_method"))), "html", null, true);
        echo "\" action=\"";
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : $this->getContext($context, "action")), "html", null, true);
        echo "\"";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        if ((isset($context["multipart"]) ? $context["multipart"] : $this->getContext($context, "multipart"))) {
            echo " enctype=\"multipart/form-data\"";
        }
        echo ">
        ";
        // line 789
        if (((isset($context["form_method"]) ? $context["form_method"] : $this->getContext($context, "form_method")) != (isset($context["method"]) ? $context["method"] : $this->getContext($context, "method")))) {
            // line 790
            echo "            <input type=\"hidden\" name=\"_method\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["method"]) ? $context["method"] : $this->getContext($context, "method")), "html", null, true);
            echo "\" />
        ";
        }
        // line 792
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_9b7fbfdd803908241afb3e135058fef2e54a9b61e25855963da5401f5cdfa6f9->leave($__internal_9b7fbfdd803908241afb3e135058fef2e54a9b61e25855963da5401f5cdfa6f9_prof);

        
        $__internal_3ba6c84124315d644c2633c03d3a54e2dd95c898215ae95f8820716f56783cf1->leave($__internal_3ba6c84124315d644c2633c03d3a54e2dd95c898215ae95f8820716f56783cf1_prof);

    }

    // line 795
    public function block_form_end($context, array $blocks = array())
    {
        $__internal_1b419b7916b2e707dc679df10b26f9d9f9eb03b17506ff315995c5b7934143c3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1b419b7916b2e707dc679df10b26f9d9f9eb03b17506ff315995c5b7934143c3->enter($__internal_1b419b7916b2e707dc679df10b26f9d9f9eb03b17506ff315995c5b7934143c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_end"));

        $__internal_d5b3c6e16e99e3eca9249adaf24a84becb181cf43c210e568472fa527e0c3650 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5b3c6e16e99e3eca9249adaf24a84becb181cf43c210e568472fa527e0c3650->enter($__internal_d5b3c6e16e99e3eca9249adaf24a84becb181cf43c210e568472fa527e0c3650_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_end"));

        // line 796
        echo "    ";
        ob_start();
        // line 797
        echo "        ";
        if (( !array_key_exists("render_rest", $context) || (isset($context["render_rest"]) ? $context["render_rest"] : $this->getContext($context, "render_rest")))) {
            // line 798
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
            echo "
        ";
        }
        // line 800
        echo "        </form>
        ";
        // line 801
        if ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getStyle()) {
            // line 802
            echo "            ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->setStyle(""), "html", null, true);
            echo "
        ";
        }
        // line 804
        echo "        ";
        if ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getColSize()) {
            // line 805
            echo "            ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->setColSize("lg"), "html", null, true);
            echo "
        ";
        }
        // line 807
        echo "        ";
        if ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getWidgetCol()) {
            // line 808
            echo "            ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->setWidgetCol(10), "html", null, true);
            echo "
        ";
        }
        // line 810
        echo "        ";
        if ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getLabelCol()) {
            // line 811
            echo "            ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->setLabelCol(2), "html", null, true);
            echo "
        ";
        }
        // line 813
        echo "        ";
        if ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getSimpleCol()) {
            // line 814
            echo "            ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->setSimpleCol(false), "html", null, true);
            echo "
        ";
        }
        // line 816
        echo "    ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->restoreFormSettings(), "html", null, true);
        echo "
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_d5b3c6e16e99e3eca9249adaf24a84becb181cf43c210e568472fa527e0c3650->leave($__internal_d5b3c6e16e99e3eca9249adaf24a84becb181cf43c210e568472fa527e0c3650_prof);

        
        $__internal_1b419b7916b2e707dc679df10b26f9d9f9eb03b17506ff315995c5b7934143c3->leave($__internal_1b419b7916b2e707dc679df10b26f9d9f9eb03b17506ff315995c5b7934143c3_prof);

    }

    // line 820
    public function block_form_enctype($context, array $blocks = array())
    {
        $__internal_4b8ee4066d07dd6893115bbf7b71c2d6226cc1265753eb0f90179d4621a9881e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4b8ee4066d07dd6893115bbf7b71c2d6226cc1265753eb0f90179d4621a9881e->enter($__internal_4b8ee4066d07dd6893115bbf7b71c2d6226cc1265753eb0f90179d4621a9881e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_enctype"));

        $__internal_46caaaa06373be10246c76544880014b8af87bac0c09a10b12e5d8589d8c3ed9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46caaaa06373be10246c76544880014b8af87bac0c09a10b12e5d8589d8c3ed9->enter($__internal_46caaaa06373be10246c76544880014b8af87bac0c09a10b12e5d8589d8c3ed9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_enctype"));

        // line 821
        echo "    ";
        ob_start();
        // line 822
        echo "        ";
        if ((isset($context["multipart"]) ? $context["multipart"] : $this->getContext($context, "multipart"))) {
            echo "enctype=\"multipart/form-data\"";
        }
        // line 823
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_46caaaa06373be10246c76544880014b8af87bac0c09a10b12e5d8589d8c3ed9->leave($__internal_46caaaa06373be10246c76544880014b8af87bac0c09a10b12e5d8589d8c3ed9_prof);

        
        $__internal_4b8ee4066d07dd6893115bbf7b71c2d6226cc1265753eb0f90179d4621a9881e->leave($__internal_4b8ee4066d07dd6893115bbf7b71c2d6226cc1265753eb0f90179d4621a9881e_prof);

    }

    // line 826
    public function block_global_form_errors($context, array $blocks = array())
    {
        $__internal_76f4c11915aa719cc78339ff1b34596fd411ff9f75b183509567c7e0ad156cb2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_76f4c11915aa719cc78339ff1b34596fd411ff9f75b183509567c7e0ad156cb2->enter($__internal_76f4c11915aa719cc78339ff1b34596fd411ff9f75b183509567c7e0ad156cb2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "global_form_errors"));

        $__internal_5ee44da2045b574ec7c4728220fd854324f93cc00d9febbe39f258229d38f0c2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ee44da2045b574ec7c4728220fd854324f93cc00d9febbe39f258229d38f0c2->enter($__internal_5ee44da2045b574ec7c4728220fd854324f93cc00d9febbe39f258229d38f0c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "global_form_errors"));

        // line 827
        echo "    ";
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 828
            echo "        ";
            $context["global_errors"] = true;
            // line 829
            echo "        ";
            $this->displayBlock("form_errors", $context, $blocks);
            echo "
    ";
        }
        
        $__internal_5ee44da2045b574ec7c4728220fd854324f93cc00d9febbe39f258229d38f0c2->leave($__internal_5ee44da2045b574ec7c4728220fd854324f93cc00d9febbe39f258229d38f0c2_prof);

        
        $__internal_76f4c11915aa719cc78339ff1b34596fd411ff9f75b183509567c7e0ad156cb2->leave($__internal_76f4c11915aa719cc78339ff1b34596fd411ff9f75b183509567c7e0ad156cb2_prof);

    }

    // line 833
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_51f10fb29957d07814ecdc8899bd01ec882ea00cef8703eb7c9d39c1bce5b73b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_51f10fb29957d07814ecdc8899bd01ec882ea00cef8703eb7c9d39c1bce5b73b->enter($__internal_51f10fb29957d07814ecdc8899bd01ec882ea00cef8703eb7c9d39c1bce5b73b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_5b39b6265d87bdbdccd4a53292aab0004292bc45ebff75a5bac5b7b9cde56d67 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b39b6265d87bdbdccd4a53292aab0004292bc45ebff75a5bac5b7b9cde56d67->enter($__internal_5b39b6265d87bdbdccd4a53292aab0004292bc45ebff75a5bac5b7b9cde56d67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 834
        echo "    ";
        ob_start();
        // line 835
        echo "        ";
        $context["global_errors"] = ((array_key_exists("global_errors", $context)) ? (_twig_default_filter((isset($context["global_errors"]) ? $context["global_errors"] : $this->getContext($context, "global_errors")), false)) : (false));
        // line 836
        echo "        ";
        $context["style"] = ((array_key_exists("style", $context)) ? (_twig_default_filter((isset($context["style"]) ? $context["style"] : $this->getContext($context, "style")), $this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getStyle())) : ($this->env->getExtension('Braincrafted\Bundle\BootstrapBundle\Twig\BootstrapFormExtension')->getStyle()));
        // line 837
        echo "
        ";
        // line 838
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "style", array(), "any", true, true) &&  !twig_test_empty($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array())))) {
            // line 839
            echo "            ";
            $context["style"] = $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array());
            // line 840
            echo "        ";
        }
        // line 841
        echo "
        ";
        // line 842
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 843
            echo "            ";
            if ((isset($context["global_errors"]) ? $context["global_errors"] : $this->getContext($context, "global_errors"))) {
                // line 844
                echo "                <div class=\"alert alert-danger\">
            ";
            }
            // line 846
            echo "            <ul";
            if ( !(isset($context["global_errors"]) ? $context["global_errors"] : $this->getContext($context, "global_errors"))) {
                echo " class=\"help-block\"";
            }
            echo ">
                ";
            // line 847
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 848
                echo "                    <li>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 850
            echo "            </ul>
            ";
            // line 851
            if (((isset($context["global_errors"]) ? $context["global_errors"] : $this->getContext($context, "global_errors")) == true)) {
                // line 852
                echo "                </div>
            ";
            }
            // line 854
            echo "        ";
        }
        // line 855
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_5b39b6265d87bdbdccd4a53292aab0004292bc45ebff75a5bac5b7b9cde56d67->leave($__internal_5b39b6265d87bdbdccd4a53292aab0004292bc45ebff75a5bac5b7b9cde56d67_prof);

        
        $__internal_51f10fb29957d07814ecdc8899bd01ec882ea00cef8703eb7c9d39c1bce5b73b->leave($__internal_51f10fb29957d07814ecdc8899bd01ec882ea00cef8703eb7c9d39c1bce5b73b_prof);

    }

    // line 858
    public function block_form_rest($context, array $blocks = array())
    {
        $__internal_91c65645a01e60a388f14f6b36d736ea976e1d8b4af56db7e22963b66c810180 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_91c65645a01e60a388f14f6b36d736ea976e1d8b4af56db7e22963b66c810180->enter($__internal_91c65645a01e60a388f14f6b36d736ea976e1d8b4af56db7e22963b66c810180_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_rest"));

        $__internal_63240376e9027835a920d4de268c6734a651fcaf2fa78d1cc738228c4e97896d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63240376e9027835a920d4de268c6734a651fcaf2fa78d1cc738228c4e97896d->enter($__internal_63240376e9027835a920d4de268c6734a651fcaf2fa78d1cc738228c4e97896d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_rest"));

        // line 859
        echo "    ";
        ob_start();
        // line 860
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 861
            echo "            ";
            if ( !$this->getAttribute($context["child"], "rendered", array())) {
                // line 862
                echo "                ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'row');
                echo "
            ";
            }
            // line 864
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 865
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_63240376e9027835a920d4de268c6734a651fcaf2fa78d1cc738228c4e97896d->leave($__internal_63240376e9027835a920d4de268c6734a651fcaf2fa78d1cc738228c4e97896d_prof);

        
        $__internal_91c65645a01e60a388f14f6b36d736ea976e1d8b4af56db7e22963b66c810180->leave($__internal_91c65645a01e60a388f14f6b36d736ea976e1d8b4af56db7e22963b66c810180_prof);

    }

    // line 870
    public function block_form_rows($context, array $blocks = array())
    {
        $__internal_eaa621008bbdf7b50310a4e98aee425326703a89a74c05e68b2e8d42b1af9571 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eaa621008bbdf7b50310a4e98aee425326703a89a74c05e68b2e8d42b1af9571->enter($__internal_eaa621008bbdf7b50310a4e98aee425326703a89a74c05e68b2e8d42b1af9571_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_rows"));

        $__internal_da6c092aed38254d57fb03e0d036a304212944747fff015b0a9e8ac2f0850211 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da6c092aed38254d57fb03e0d036a304212944747fff015b0a9e8ac2f0850211->enter($__internal_da6c092aed38254d57fb03e0d036a304212944747fff015b0a9e8ac2f0850211_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_rows"));

        // line 871
        echo "    ";
        ob_start();
        // line 872
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 873
            echo "            ";
            $context["childAttr"] = array();
            // line 874
            echo "            ";
            if ($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "col_size", array(), "any", true, true)) {
                // line 875
                echo "                ";
                $context["childAttr"] = twig_array_merge((isset($context["childAttr"]) ? $context["childAttr"] : $this->getContext($context, "childAttr")), array("col_size" => $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "col_size", array())));
                // line 876
                echo "            ";
            }
            // line 877
            echo "            ";
            if ($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_col", array(), "any", true, true)) {
                // line 878
                echo "                ";
                $context["childAttr"] = twig_array_merge((isset($context["childAttr"]) ? $context["childAttr"] : $this->getContext($context, "childAttr")), array("widget_col" => $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "widget_col", array())));
                // line 879
                echo "            ";
            }
            // line 880
            echo "            ";
            if ($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "label_col", array(), "any", true, true)) {
                // line 881
                echo "                ";
                $context["childAttr"] = twig_array_merge((isset($context["childAttr"]) ? $context["childAttr"] : $this->getContext($context, "childAttr")), array("label_col" => $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "label_col", array())));
                // line 882
                echo "            ";
            }
            // line 883
            echo "            ";
            if ($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "simple_col", array(), "any", true, true)) {
                // line 884
                echo "                ";
                $context["childAttr"] = twig_array_merge((isset($context["childAttr"]) ? $context["childAttr"] : $this->getContext($context, "childAttr")), array("simple_col" => $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "simple_col", array())));
                // line 885
                echo "            ";
            }
            // line 886
            echo "            ";
            if ($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "style", array(), "any", true, true)) {
                // line 887
                echo "                ";
                $context["childAttr"] = twig_array_merge((isset($context["childAttr"]) ? $context["childAttr"] : $this->getContext($context, "childAttr")), array("style" => $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array())));
                // line 888
                echo "            ";
            }
            // line 889
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'row', (isset($context["childAttr"]) ? $context["childAttr"] : $this->getContext($context, "childAttr")));
            echo "
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 891
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_da6c092aed38254d57fb03e0d036a304212944747fff015b0a9e8ac2f0850211->leave($__internal_da6c092aed38254d57fb03e0d036a304212944747fff015b0a9e8ac2f0850211_prof);

        
        $__internal_eaa621008bbdf7b50310a4e98aee425326703a89a74c05e68b2e8d42b1af9571->leave($__internal_eaa621008bbdf7b50310a4e98aee425326703a89a74c05e68b2e8d42b1af9571_prof);

    }

    // line 894
    public function block_widget_attributes($context, array $blocks = array())
    {
        $__internal_31603ae5eb33674a137f03964b791fc3ea61d3d1b872dc5e72aac8a3ca28d0fa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_31603ae5eb33674a137f03964b791fc3ea61d3d1b872dc5e72aac8a3ca28d0fa->enter($__internal_31603ae5eb33674a137f03964b791fc3ea61d3d1b872dc5e72aac8a3ca28d0fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "widget_attributes"));

        $__internal_49b2478ba0dd074651ac203053498f7e36aeda0b9c5f21155573190df6f667d5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_49b2478ba0dd074651ac203053498f7e36aeda0b9c5f21155573190df6f667d5->enter($__internal_49b2478ba0dd074651ac203053498f7e36aeda0b9c5f21155573190df6f667d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "widget_attributes"));

        // line 895
        echo "    ";
        ob_start();
        // line 896
        echo "        id=\"";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, (isset($context["full_name"]) ? $context["full_name"] : $this->getContext($context, "full_name")), "html", null, true);
        echo "\" ";
        if ((isset($context["disabled"]) ? $context["disabled"] : $this->getContext($context, "disabled"))) {
            echo " disabled=\"disabled\"";
        }
        if ((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) {
            echo " required=\"required\"";
        }
        // line 897
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            if (twig_in_filter($context["attrname"], array(0 => "placeholder", 1 => "title"))) {
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["attrvalue"], array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
                echo "\" ";
            } elseif (twig_in_filter($context["attrname"], array(0 => "input_group"))) {
            } else {
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\" ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 898
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_49b2478ba0dd074651ac203053498f7e36aeda0b9c5f21155573190df6f667d5->leave($__internal_49b2478ba0dd074651ac203053498f7e36aeda0b9c5f21155573190df6f667d5_prof);

        
        $__internal_31603ae5eb33674a137f03964b791fc3ea61d3d1b872dc5e72aac8a3ca28d0fa->leave($__internal_31603ae5eb33674a137f03964b791fc3ea61d3d1b872dc5e72aac8a3ca28d0fa_prof);

    }

    // line 901
    public function block_widget_container_attributes($context, array $blocks = array())
    {
        $__internal_741b87d94bf191ba80084126cd49e679a5b192da1197a4edd1b0c75e73ad239f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_741b87d94bf191ba80084126cd49e679a5b192da1197a4edd1b0c75e73ad239f->enter($__internal_741b87d94bf191ba80084126cd49e679a5b192da1197a4edd1b0c75e73ad239f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "widget_container_attributes"));

        $__internal_57a9df1520b8dec819edd8c134122dbdc1d47cc8896a164617cd9520a8d91142 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_57a9df1520b8dec819edd8c134122dbdc1d47cc8896a164617cd9520a8d91142->enter($__internal_57a9df1520b8dec819edd8c134122dbdc1d47cc8896a164617cd9520a8d91142_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "widget_container_attributes"));

        // line 902
        echo "    ";
        ob_start();
        // line 903
        echo "        ";
        if (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "style", array(), "any", true, true) && (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array()) == "inline") || ($this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array()) == "horizontal")))) {
            // line 904
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((("form-" . $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "style", array())) . " ") . (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : (""))))));
            // line 905
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("style" => null));
            // line 906
            echo "        ";
        }
        // line 907
        echo "        ";
        if ( !twig_test_empty((isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")))) {
            echo "id=\"";
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
            echo "\" ";
        }
        // line 908
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            if (( !(null === $context["attrvalue"]) &&  !twig_test_iterable($context["attrvalue"]))) {
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\" ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 909
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_57a9df1520b8dec819edd8c134122dbdc1d47cc8896a164617cd9520a8d91142->leave($__internal_57a9df1520b8dec819edd8c134122dbdc1d47cc8896a164617cd9520a8d91142_prof);

        
        $__internal_741b87d94bf191ba80084126cd49e679a5b192da1197a4edd1b0c75e73ad239f->leave($__internal_741b87d94bf191ba80084126cd49e679a5b192da1197a4edd1b0c75e73ad239f_prof);

    }

    // line 912
    public function block_button_attributes($context, array $blocks = array())
    {
        $__internal_bcbcb4adce8b473c1773e3a3ed8f2369ad4bab71cb1ef692ffe880bdc25e61f8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bcbcb4adce8b473c1773e3a3ed8f2369ad4bab71cb1ef692ffe880bdc25e61f8->enter($__internal_bcbcb4adce8b473c1773e3a3ed8f2369ad4bab71cb1ef692ffe880bdc25e61f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_attributes"));

        $__internal_c9bc4ee16fbc8c9421a6f44f068fed96b1750cd13169ca27d374d5c3951c72b5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c9bc4ee16fbc8c9421a6f44f068fed96b1750cd13169ca27d374d5c3951c72b5->enter($__internal_c9bc4ee16fbc8c9421a6f44f068fed96b1750cd13169ca27d374d5c3951c72b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_attributes"));

        // line 913
        echo "id=\"";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, (isset($context["full_name"]) ? $context["full_name"] : $this->getContext($context, "full_name")), "html", null, true);
        echo "\"";
        if ((isset($context["disabled"]) ? $context["disabled"] : $this->getContext($context, "disabled"))) {
            echo " disabled=\"disabled\"";
        }
        // line 914
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_c9bc4ee16fbc8c9421a6f44f068fed96b1750cd13169ca27d374d5c3951c72b5->leave($__internal_c9bc4ee16fbc8c9421a6f44f068fed96b1750cd13169ca27d374d5c3951c72b5_prof);

        
        $__internal_bcbcb4adce8b473c1773e3a3ed8f2369ad4bab71cb1ef692ffe880bdc25e61f8->leave($__internal_bcbcb4adce8b473c1773e3a3ed8f2369ad4bab71cb1ef692ffe880bdc25e61f8_prof);

    }

    public function getTemplateName()
    {
        return "BraincraftedBootstrapBundle:Form:bootstrap.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  3664 => 914,  3655 => 913,  3646 => 912,  3635 => 909,  3620 => 908,  3613 => 907,  3610 => 906,  3607 => 905,  3604 => 904,  3601 => 903,  3598 => 902,  3589 => 901,  3578 => 898,  3557 => 897,  3545 => 896,  3542 => 895,  3533 => 894,  3522 => 891,  3513 => 889,  3510 => 888,  3507 => 887,  3504 => 886,  3501 => 885,  3498 => 884,  3495 => 883,  3492 => 882,  3489 => 881,  3486 => 880,  3483 => 879,  3480 => 878,  3477 => 877,  3474 => 876,  3471 => 875,  3468 => 874,  3465 => 873,  3460 => 872,  3457 => 871,  3448 => 870,  3437 => 865,  3431 => 864,  3425 => 862,  3422 => 861,  3417 => 860,  3414 => 859,  3405 => 858,  3394 => 855,  3391 => 854,  3387 => 852,  3385 => 851,  3382 => 850,  3373 => 848,  3369 => 847,  3362 => 846,  3358 => 844,  3355 => 843,  3353 => 842,  3350 => 841,  3347 => 840,  3344 => 839,  3342 => 838,  3339 => 837,  3336 => 836,  3333 => 835,  3330 => 834,  3321 => 833,  3307 => 829,  3304 => 828,  3301 => 827,  3292 => 826,  3281 => 823,  3276 => 822,  3273 => 821,  3264 => 820,  3250 => 816,  3244 => 814,  3241 => 813,  3235 => 811,  3232 => 810,  3226 => 808,  3223 => 807,  3217 => 805,  3214 => 804,  3208 => 802,  3206 => 801,  3203 => 800,  3197 => 798,  3194 => 797,  3191 => 796,  3182 => 795,  3171 => 792,  3165 => 790,  3163 => 789,  3139 => 788,  3136 => 787,  3133 => 786,  3130 => 785,  3128 => 784,  3125 => 783,  3119 => 781,  3117 => 780,  3114 => 779,  3108 => 777,  3106 => 776,  3103 => 775,  3097 => 773,  3095 => 772,  3092 => 771,  3086 => 769,  3084 => 768,  3081 => 767,  3078 => 766,  3075 => 765,  3073 => 764,  3070 => 763,  3064 => 761,  3061 => 760,  3059 => 759,  3056 => 758,  3053 => 757,  3050 => 756,  3047 => 755,  3044 => 754,  3041 => 753,  3039 => 752,  3034 => 751,  3031 => 750,  3022 => 749,  3009 => 745,  3005 => 744,  3000 => 743,  2997 => 742,  2988 => 741,  2975 => 736,  2966 => 735,  2953 => 732,  2944 => 731,  2932 => 727,  2928 => 725,  2926 => 724,  2921 => 722,  2918 => 721,  2906 => 719,  2904 => 718,  2900 => 716,  2897 => 715,  2895 => 714,  2892 => 713,  2889 => 712,  2886 => 711,  2883 => 710,  2880 => 709,  2877 => 708,  2874 => 707,  2871 => 706,  2868 => 705,  2865 => 704,  2862 => 703,  2859 => 702,  2857 => 701,  2854 => 700,  2851 => 699,  2848 => 698,  2845 => 697,  2836 => 696,  2825 => 693,  2819 => 691,  2816 => 690,  2813 => 689,  2810 => 688,  2807 => 687,  2804 => 686,  2801 => 685,  2792 => 684,  2781 => 681,  2775 => 679,  2771 => 677,  2765 => 675,  2762 => 674,  2756 => 672,  2754 => 671,  2749 => 670,  2743 => 668,  2740 => 667,  2734 => 665,  2732 => 664,  2727 => 663,  2724 => 662,  2721 => 661,  2718 => 660,  2715 => 659,  2712 => 658,  2709 => 657,  2706 => 656,  2703 => 655,  2700 => 654,  2697 => 653,  2694 => 652,  2691 => 651,  2688 => 650,  2679 => 649,  2668 => 646,  2664 => 645,  2660 => 643,  2655 => 641,  2651 => 640,  2647 => 639,  2642 => 638,  2636 => 635,  2632 => 634,  2628 => 633,  2622 => 632,  2617 => 631,  2615 => 630,  2609 => 629,  2606 => 628,  2603 => 627,  2601 => 626,  2598 => 625,  2595 => 624,  2592 => 623,  2589 => 622,  2586 => 621,  2583 => 620,  2580 => 619,  2577 => 618,  2574 => 617,  2571 => 616,  2568 => 615,  2565 => 614,  2562 => 613,  2559 => 612,  2556 => 611,  2554 => 610,  2551 => 609,  2548 => 608,  2545 => 607,  2542 => 606,  2533 => 605,  2519 => 601,  2517 => 597,  2514 => 596,  2505 => 595,  2488 => 591,  2477 => 588,  2458 => 586,  2455 => 585,  2452 => 584,  2449 => 583,  2446 => 582,  2443 => 581,  2440 => 580,  2437 => 579,  2434 => 578,  2432 => 577,  2429 => 576,  2426 => 575,  2423 => 574,  2420 => 573,  2417 => 572,  2414 => 571,  2412 => 570,  2409 => 569,  2406 => 568,  2403 => 567,  2401 => 566,  2398 => 565,  2395 => 564,  2392 => 563,  2390 => 562,  2387 => 561,  2384 => 560,  2381 => 559,  2378 => 558,  2375 => 557,  2372 => 556,  2369 => 555,  2366 => 554,  2363 => 553,  2361 => 552,  2358 => 551,  2355 => 550,  2352 => 549,  2343 => 548,  2315 => 543,  2312 => 542,  2303 => 541,  2289 => 538,  2285 => 537,  2280 => 536,  2271 => 535,  2257 => 531,  2254 => 530,  2251 => 529,  2242 => 528,  2228 => 524,  2225 => 523,  2222 => 522,  2213 => 521,  2202 => 518,  2189 => 516,  2178 => 514,  2175 => 513,  2172 => 512,  2169 => 511,  2166 => 510,  2163 => 509,  2160 => 508,  2157 => 507,  2154 => 506,  2151 => 505,  2148 => 504,  2139 => 503,  2125 => 499,  2122 => 498,  2119 => 497,  2110 => 496,  2096 => 492,  2093 => 491,  2090 => 490,  2081 => 489,  2067 => 485,  2064 => 484,  2061 => 483,  2052 => 482,  2037 => 476,  2034 => 475,  2031 => 474,  2028 => 473,  2019 => 472,  2005 => 468,  2002 => 467,  1999 => 466,  1990 => 465,  1976 => 461,  1973 => 460,  1970 => 459,  1961 => 458,  1948 => 453,  1946 => 449,  1943 => 448,  1940 => 447,  1931 => 446,  1917 => 442,  1914 => 441,  1911 => 440,  1902 => 439,  1888 => 435,  1885 => 434,  1883 => 433,  1880 => 432,  1871 => 431,  1860 => 428,  1856 => 426,  1850 => 425,  1845 => 424,  1841 => 423,  1836 => 422,  1833 => 421,  1830 => 420,  1824 => 418,  1821 => 417,  1818 => 416,  1809 => 415,  1798 => 412,  1793 => 409,  1791 => 408,  1790 => 407,  1789 => 406,  1788 => 405,  1783 => 404,  1780 => 403,  1774 => 401,  1771 => 400,  1768 => 399,  1759 => 398,  1748 => 395,  1742 => 392,  1738 => 391,  1734 => 390,  1730 => 389,  1725 => 388,  1722 => 387,  1716 => 385,  1713 => 384,  1710 => 383,  1701 => 382,  1679 => 378,  1676 => 377,  1667 => 376,  1645 => 372,  1642 => 371,  1633 => 370,  1622 => 367,  1618 => 365,  1616 => 364,  1612 => 362,  1610 => 361,  1605 => 359,  1602 => 358,  1596 => 356,  1590 => 354,  1588 => 353,  1585 => 352,  1579 => 350,  1573 => 348,  1569 => 346,  1567 => 345,  1563 => 344,  1547 => 343,  1544 => 342,  1541 => 341,  1538 => 340,  1535 => 339,  1532 => 338,  1529 => 337,  1526 => 336,  1523 => 335,  1520 => 334,  1517 => 333,  1514 => 332,  1511 => 331,  1508 => 330,  1506 => 329,  1503 => 328,  1495 => 326,  1493 => 325,  1489 => 324,  1482 => 323,  1479 => 322,  1476 => 321,  1473 => 320,  1471 => 319,  1468 => 318,  1465 => 317,  1462 => 316,  1459 => 315,  1456 => 314,  1453 => 313,  1450 => 312,  1447 => 311,  1444 => 310,  1442 => 309,  1439 => 308,  1437 => 307,  1434 => 306,  1431 => 305,  1428 => 304,  1419 => 303,  1408 => 300,  1404 => 299,  1401 => 298,  1397 => 296,  1395 => 295,  1391 => 293,  1389 => 292,  1384 => 290,  1381 => 289,  1375 => 287,  1369 => 285,  1367 => 284,  1364 => 283,  1358 => 281,  1352 => 279,  1348 => 277,  1346 => 276,  1342 => 275,  1326 => 274,  1323 => 273,  1320 => 272,  1317 => 271,  1314 => 270,  1311 => 269,  1308 => 268,  1305 => 267,  1302 => 266,  1299 => 265,  1296 => 264,  1293 => 263,  1290 => 262,  1287 => 261,  1285 => 260,  1282 => 259,  1274 => 257,  1272 => 256,  1268 => 255,  1261 => 254,  1258 => 253,  1255 => 252,  1252 => 251,  1249 => 250,  1247 => 249,  1244 => 248,  1241 => 247,  1238 => 246,  1235 => 245,  1232 => 244,  1229 => 243,  1226 => 242,  1223 => 241,  1220 => 240,  1217 => 239,  1214 => 238,  1211 => 237,  1209 => 236,  1206 => 235,  1203 => 234,  1200 => 233,  1197 => 232,  1188 => 231,  1177 => 228,  1163 => 227,  1151 => 225,  1144 => 222,  1142 => 221,  1137 => 220,  1134 => 219,  1116 => 218,  1113 => 217,  1104 => 216,  1089 => 211,  1086 => 210,  1083 => 209,  1077 => 207,  1075 => 206,  1070 => 205,  1067 => 204,  1064 => 203,  1051 => 201,  1049 => 200,  1042 => 199,  1039 => 198,  1036 => 197,  1033 => 196,  1024 => 195,  1012 => 191,  1006 => 190,  1000 => 188,  994 => 186,  991 => 185,  987 => 184,  982 => 183,  979 => 182,  970 => 181,  959 => 178,  953 => 176,  947 => 174,  944 => 173,  941 => 172,  932 => 171,  921 => 168,  917 => 166,  915 => 165,  910 => 163,  907 => 162,  899 => 160,  897 => 159,  894 => 158,  891 => 157,  888 => 156,  886 => 155,  883 => 154,  880 => 153,  877 => 152,  875 => 151,  872 => 150,  869 => 149,  866 => 148,  857 => 147,  846 => 144,  842 => 142,  840 => 141,  833 => 139,  830 => 138,  828 => 137,  825 => 136,  817 => 134,  815 => 133,  812 => 132,  809 => 131,  806 => 130,  803 => 129,  800 => 128,  797 => 127,  795 => 126,  792 => 125,  789 => 124,  786 => 123,  777 => 122,  765 => 118,  755 => 116,  753 => 115,  750 => 114,  742 => 111,  734 => 108,  729 => 107,  727 => 106,  722 => 104,  718 => 103,  714 => 102,  710 => 100,  706 => 99,  700 => 97,  697 => 96,  694 => 95,  692 => 94,  689 => 93,  686 => 92,  683 => 91,  680 => 90,  677 => 89,  674 => 88,  671 => 87,  668 => 86,  665 => 85,  662 => 84,  659 => 83,  656 => 82,  647 => 81,  633 => 77,  630 => 76,  627 => 75,  624 => 74,  621 => 73,  612 => 72,  598 => 67,  593 => 66,  587 => 64,  585 => 63,  580 => 62,  577 => 61,  568 => 60,  557 => 57,  553 => 55,  550 => 54,  537 => 52,  535 => 51,  515 => 49,  512 => 48,  510 => 47,  507 => 46,  504 => 45,  501 => 44,  498 => 43,  495 => 42,  492 => 41,  489 => 40,  487 => 39,  484 => 38,  482 => 37,  479 => 36,  471 => 34,  469 => 33,  466 => 32,  463 => 31,  460 => 30,  457 => 29,  454 => 28,  451 => 27,  448 => 26,  445 => 25,  442 => 24,  439 => 23,  436 => 22,  433 => 21,  431 => 20,  428 => 19,  425 => 18,  422 => 17,  419 => 16,  410 => 15,  399 => 12,  393 => 10,  387 => 8,  384 => 7,  381 => 6,  372 => 5,  362 => 912,  359 => 911,  357 => 901,  354 => 900,  352 => 894,  349 => 893,  347 => 870,  344 => 869,  341 => 867,  339 => 858,  336 => 857,  334 => 833,  331 => 832,  329 => 826,  326 => 825,  324 => 820,  321 => 819,  319 => 795,  316 => 794,  314 => 749,  311 => 748,  309 => 741,  306 => 740,  303 => 738,  301 => 735,  298 => 734,  296 => 731,  293 => 730,  291 => 696,  288 => 695,  286 => 684,  283 => 683,  281 => 649,  278 => 648,  276 => 605,  273 => 604,  271 => 595,  268 => 594,  265 => 592,  263 => 591,  260 => 590,  258 => 548,  255 => 547,  252 => 545,  250 => 541,  247 => 540,  245 => 535,  242 => 534,  240 => 528,  237 => 527,  235 => 521,  232 => 520,  230 => 503,  227 => 502,  225 => 496,  222 => 495,  220 => 489,  217 => 488,  215 => 482,  212 => 481,  210 => 472,  207 => 471,  205 => 465,  202 => 464,  200 => 458,  197 => 457,  195 => 446,  192 => 445,  190 => 439,  187 => 438,  185 => 431,  182 => 430,  180 => 415,  177 => 414,  175 => 398,  172 => 397,  170 => 382,  167 => 381,  165 => 376,  162 => 375,  160 => 370,  157 => 369,  155 => 303,  152 => 302,  150 => 231,  147 => 230,  145 => 216,  142 => 215,  140 => 195,  137 => 194,  135 => 181,  132 => 180,  130 => 171,  127 => 170,  125 => 147,  122 => 146,  120 => 122,  117 => 121,  115 => 81,  112 => 80,  110 => 72,  107 => 71,  105 => 60,  102 => 59,  100 => 15,  97 => 14,  95 => 5,  92 => 4,  89 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"form_div_layout.html.twig\" %}

{# Widgets #}

{% block form_widget %}
    {% spaceless %}
        {% if compound %}
            {{ block('form_widget_compound') }}
        {% else %}
            {{ block('form_widget_simple') }}
        {% endif %}
    {% endspaceless %}
{% endblock form_widget %}

{% block form_widget_simple %}
    {% spaceless %}
        {% set style = style|default(bootstrap_get_style()) %}
        {% set col_size = col_size|default(bootstrap_get_col_size()) %}

        {% if simple_col is not defined and bootstrap_get_simple_col() %}
            {% set simple_col = bootstrap_get_simple_col() %}
        {% endif %}
        {% if attr.simple_col is defined and attr.simple_col is not empty %}
            {% set simple_col = attr.simple_col %}
        {% endif  %}
        {% if attr.col_size is defined and attr.col_size is not empty %}
            {% set col_size = attr.col_size %}
        {% endif %}
        {% if attr.style is defined and attr.style is not empty %}
            {% set style = attr.style %}
        {% endif %}

        {% if simple_col is defined and simple_col %}
            <div class=\"col-{{ col_size }}-{{ simple_col }}\">
        {% endif %}

        {% set type = type|default('text') %}

        {% if style == 'inline' and (attr.placeholder is not defined or attr.placeholder is empty)  and label is not same as (false) %}
            {% if label is empty %}
                {% set attr = attr|merge({ 'placeholder': name|humanize }) %}
            {% else %}
                {% set attr = attr|merge({ 'placeholder': label}) %}
            {% endif %}
        {% endif %}

        {% if static_control is defined and static_control == true %}
            {% set attr = attr|merge({ 'class': (attr.class|default('') ~ ' form-control-static')|trim }) %}
            <p id=\"{{ id }}\" {%- for attrname, attrvalue in attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>{{ value }}</p>
        {%- else -%}
            {% set attr = attr|merge({ 'class': (attr.class|default('') ~ ' form-control')|trim }) %}
            <input type=\"{{ type }}\" {{ block('widget_attributes') }} {% if value is not empty %}value=\"{{ value }}\" {% endif %}>
        {%- endif %}
        {% if simple_col is defined %}
            </div>
        {% endif %}
    {% endspaceless %}
{% endblock form_widget_simple %}

{% block form_widget_compound %}
    {% spaceless %}
        <div {{ block('widget_container_attributes') }}>
            {% if form.parent is empty %}
                {{ block('global_form_errors') }}
            {% endif %}
            {{ block('form_rows') }}
            {{ form_rest(form) }}
        </div>
    {% endspaceless %}
{% endblock form_widget_compound %}

{% block collection_widget %}
    {% spaceless %}
        {% if prototype is defined %}
            {% set attr = attr|merge({'data-prototype': form_row(prototype) }) %}
        {% endif %}
        {{ block('form_widget') }}
    {% endspaceless %}
{% endblock collection_widget %}

{% block bootstrap_collection_widget %}
    {% spaceless %}
        {% if prototype is defined %}
            {% set prototype_vars = {} %}
            {% if style is defined %}
                {% set prototype_vars = prototype_vars|merge({'style': style}) %}
            {% endif %}
            {% set prototype_html = '<div class=\"col-xs-' ~ form.vars.sub_widget_col ~ '\">' ~ form_widget(prototype, prototype_vars) ~ '</div>' %}
            {% if form.vars.allow_delete %}
                {% set prototype_html = prototype_html ~ '<div class=\"col-xs-' ~ form.vars.button_col ~ '\"><a href=\"#\" class=\"btn btn-danger btn-sm\" data-removefield=\"collection\" data-field=\"__id__\">' ~ form.vars.delete_button_text|trans({}, translation_domain)|parse_icons|raw ~ '</a></div>' %}
            {% endif %}
            {% set prototype_html = '<div class=\"row\">' ~ prototype_html ~ '</div>' %}

            {% set attr = attr|merge({'data-prototype': prototype_html }) %}
            {% set attr = attr|merge({'data-prototype-name': prototype_name }) %}
        {% endif %}
        <div {{ block('widget_container_attributes') }}>
            <ul class=\"bc-collection list-unstyled\">
                {% for field in form %}
                    <li>
                        <div class=\"row\">
                            <div class=\"col-xs-{{ form.vars.sub_widget_col }}\">
                                {{ form_widget(field) }}
                                {{ form_errors(field) }}
                            </div>
                            {% if form.vars.allow_delete %}
                                <div class=\"col-xs-{{ form.vars.button_col }}\">
                                    <a href=\"#\" class=\"btn btn-danger btn-sm\" data-removefield=\"collection\" data-field=\"{{ field.vars.id }}\">{{ form.vars.delete_button_text|trans({}, translation_domain)|parse_icons|raw }}</a>
                                </div>
                            {% endif %}
                        </div>
                    </li>
                {% endfor %}
            </ul>
            {% if form.vars.allow_add %}
                <a href=\"#\" class=\"btn btn-primary btn-sm\" data-addfield=\"collection\" data-collection=\"{{ form.vars.id }}\" data-prototype-name=\"{{ prototype_name }}\">{{ form.vars.add_button_text|trans({}, translation_domain)|parse_icons|raw }}</a>
            {% endif %}
        </div>
    {% endspaceless %}
{% endblock bootstrap_collection_widget %}

{% block textarea_widget %}
    {% spaceless %}
        {% set col_size = col_size|default(bootstrap_get_col_size()) %}

        {% if attr.simple_col is defined and attr.simple_col is not empty %}
            {% set simple_col = attr.simple_col %}
        {% endif  %}
        {% if attr.col_size is defined and attr.col_size is not empty %}
            {% set col_size = attr.col_size %}
        {% endif %}

        {% if simple_col is defined %}
            <div class=\"col-{{ col_size }}-{{ simple_col }}\">
        {% endif %}

        {% set attr = attr|merge({ 'class': (attr.class|default('') ~ ' form-control')|trim }) %}

        <textarea {{ block('widget_attributes') }}>{{ value }}</textarea>

        {% if simple_col is defined %}
            </div>
        {% endif %}
    {% endspaceless %}
{% endblock textarea_widget %}

{% block file_widget %}
    {% spaceless %}
        {% set col_size = col_size|default(bootstrap_get_col_size()) %}

        {% if attr.simple_col is defined and attr.simple_col is not empty %}
            {% set simple_col = attr.simple_col %}
        {% endif  %}

        {% if attr.col_size is defined and attr.col_size is not empty %}
            {% set col_size = attr.col_size %}
        {% endif %}

        {% if simple_col is defined %}
            <div class=\"col-{{ col_size }}-{{ simple_col }}\">
        {% endif %}

        <input type=\"file\" {{ block('widget_attributes') }}>

        {% if simple_col is defined %}
            </div>
        {% endif %}
    {% endspaceless %}
{% endblock file_widget %}

{% block choice_widget %}
    {% spaceless %}
        {% if expanded %}
            {{ block('choice_widget_expanded') }}
        {% else %}
            {{ block('choice_widget_collapsed') }}
        {% endif %}
    {% endspaceless %}
{% endblock choice_widget %}

{% block choice_widget_expanded %}
    {% spaceless %}
        <div {{ block('widget_container_attributes') }}>
            {% for child in form %}
                {% if multiple %}
                    {{ checkbox_row(child, { 'no_form_group': true, 'inline' : (attr.inline is defined and attr.inline), 'label_attr': label_attr }) }}
                {% else %}
                    {{ radio_row(child, { 'no_form_group': true, 'inline' : (attr.inline is defined and attr.inline), 'label_attr': label_attr  }) }}
                {% endif %}
            {% endfor %}
        </div>
    {% endspaceless %}
{% endblock choice_widget_expanded %}

{% block choice_widget_collapsed %}
    {% spaceless %}
        {% set attr = attr|merge({ 'class': (attr.class|default('') ~ ' form-control')|trim }) %}

        <select {{ block('widget_attributes') }}{% if multiple %} multiple=\"multiple\"{% endif %}>
            {% if placeholder is not none %}
                <option {% if required %} disabled=\"disabled\"{% if value is empty %} selected=\"selected\"{% endif %}{% endif %} value=\"\">{{ placeholder|trans({}, translation_domain) }}</option>
            {% endif %}
            {% if preferred_choices|length > 0 %}
                {% set options = preferred_choices %}
                {{ block('choice_widget_options') }}
                {% if choices|length > 0 and separator is not none %}
                    <option disabled=\"disabled\">{{ separator }}</option>
                {% endif %}
            {% endif %}
            {% set options = choices %}
            {{ block('choice_widget_options') }}
        </select>
    {% endspaceless %}
{% endblock choice_widget_collapsed %}

{% block choice_widget_options %}
    {% spaceless %}
        {% for group_label, choice in options %}
            {% if choice is iterable %}
                <optgroup label=\"{{ choice_translation_domain is defined ? (choice_translation_domain is same as (false) ? group_label : group_label|trans({}, choice_translation_domain)) : group_label|trans({}, translation_domain) }}\">
                    {% set options = choice %}
                    {{ block('choice_widget_options') }}
                </optgroup>
            {% else %}
                <option value=\"{{ choice.value }}\"{% if choice is selectedchoice(value) %} selected=\"selected\"{% endif %}>{{ choice_translation_domain is defined ? (choice_translation_domain is same as (false) ? choice.label : choice.label|trans({}, choice_translation_domain)) : choice.label|trans({}, translation_domain) }}</option>
            {% endif %}
        {% endfor %}
    {% endspaceless %}
{% endblock choice_widget_options %}

{% block checkbox_row %}
    {% spaceless %}
        {% set style = style|default(bootstrap_get_style()) %}
        {% set col_size = col_size|default(bootstrap_get_col_size()) %}

        {% if attr.label_col is defined and attr.label_col is not empty %}
            {% set label_col = attr.label_col %}
        {% endif %}
        {% if attr.widget_col is defined and attr.widget_col is not empty %}
            {% set widget_col = attr.widget_col %}
        {% endif %}
        {% if attr.col_size is defined and attr.col_size is not empty %}
            {% set col_size = attr.col_size %}
        {% endif %}
        {% if attr.style is defined and attr.style is not empty %}
            {% set style = attr.style %}
        {% endif %}

        {% set class = '' %}
        {% if align_with_widget is defined or attr.align_with_widget is defined %}
            {% set widget_col = widget_col|default(bootstrap_get_widget_col()) %}
            {% set label_col = label_col|default(bootstrap_get_label_col()) %}
            {% set class = 'col-' ~ col_size ~ '-' ~ widget_col ~ ' col-' ~ col_size ~ '-offset-' ~ label_col %}
            <div class=\"form-group {% if form.vars.errors|length > 0 %} has-error{% endif %}\">
            <div class=\"{{ class }}\">
        {% elseif no_form_group is not defined or no_form_group == false %}
            <div class=\"form-group{% if form.vars.errors|length > 0 %} has-error{% endif %}\">
        {% endif %}

        {%set checkboxdata %}
        {% if label is not same as (false) %}
            {% if not compound %}
                {% set label_attr = label_attr|merge({'for': id}) %}
            {% endif %}
            {% if inline is defined and inline %}
                {% set label_attr = label_attr|merge({'class': (label_attr.class|default('') ~ ' checkbox-inline')|trim}) %}
            {% endif %}
            {% if required %}
                {% set label_attr = label_attr|merge({'class': (label_attr.class|default('') ~ ' required')|trim}) %}
            {% endif %}
            {% if label is empty %}
                {% set label = name|humanize %}
            {% endif %}
            <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
            {{ block('checkbox_widget') }}
            {{ label|trans({}, translation_domain)|raw -}}
            </label>
        {% else %}
            {{ block('checkbox_widget') }}
        {% endif %}
        {{ form_errors(form) }}
        {% endset %}

        {% if inline is defined and inline %}
            {{ checkboxdata|raw }}
        {% else%}
            <div class=\"checkbox\">{{ checkboxdata|raw }}</div>
        {% endif %}

        {{ block('form_help') }}

        {% if align_with_widget is defined or attr.align_with_widget is defined %}
            </div>
            </div>
        {% elseif no_form_group is not defined or no_form_group == false %}
            </div>
        {% endif %}

        {% if style == 'inline' %}&nbsp;{% endif %}
    {% endspaceless %}
{% endblock checkbox_row %}

{% block radio_row %}
    {% spaceless %}
        {% set class = '' %}

        {% set col_size = col_size|default(bootstrap_get_col_size()) %}

        {% if attr.label_col is defined and attr.label_col is not empty %}
            {% set label_col = attr.label_col %}
        {% endif %}
        {% if attr.widget_col is defined and attr.widget_col is not empty %}
            {% set widget_col = attr.widget_col %}
        {% endif %}
        {% if attr.col_size is defined and attr.col_size is not empty %}
            {% set col_size = attr.col_size %}
        {% endif %}

        {% if align_with_widget is defined or attr.align_with_widget is defined %}
            {% set widget_col = widget_col|default(bootstrap_get_widget_col()) %}
            {% set label_col = label_col|default(bootstrap_get_label_col()) %}
            {% set class = ' col-'~ col_size ~ '-' ~ widget_col ~ ' col-' ~ col_size ~ '-offset-' ~ label_col %}
            <div class=\"form-group {% if form.vars.errors|length > 0 %} has-error{% endif %}\">
            <div class=\"{{ class }}\">
        {% elseif no_form_group is not defined or no_form_group == false %}
            <div class=\"form-group{% if form.vars.errors|length > 0 %} has-error{% endif %}\">
        {% endif %}

        {%set radiodata %}
        {% if label is not same as (false) %}
            {% if not compound %}
                {% set label_attr = label_attr|merge({'for': id}) %}
            {% endif %}
            {% if inline is defined and inline %}
                {% set label_attr = label_attr|merge({'class': (label_attr.class|default('') ~ ' radio-inline')|trim}) %}
            {% endif %}
            {% if required %}
                {% set label_attr = label_attr|merge({'class': (label_attr.class|default('') ~ ' required')|trim}) %}
            {% endif %}
            {% if label is empty %}
                {% set label = name|humanize %}
            {% endif %}
            <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
            {{ block('radio_widget') }}
            {{ label|trans({}, translation_domain)|raw -}}
            </label>
        {% else %}
            {{ block('radio_widget') }}
        {% endif %}
        {{ form_errors(form) }}
        {% endset %}

        {% if inline is defined and inline %}
            {{ radiodata|raw }}
        {% else%}
            <div class=\"radio\">{{ radiodata|raw }}</div>
        {% endif %}

        {{ block('form_help') }}

        {% if align_with_widget is defined or attr.align_with_widget is defined %}
            </div>
            </div>
        {% elseif no_form_group is not defined or no_form_group == false %}
            </div>
        {% endif %}
    {% endspaceless %}
{% endblock radio_row %}

{% block checkbox_widget %}
    {% spaceless %}
        <input type=\"checkbox\" {{ block('widget_attributes') }}{% if value is defined %} value=\"{{ value }}\"{% endif %}{% if checked %} checked=\"checked\"{% endif %} />
    {% endspaceless %}
{% endblock checkbox_widget %}

{% block radio_widget %}
    {% spaceless %}
        <input type=\"radio\" {{ block('widget_attributes') }}{% if value is defined %} value=\"{{ value }}\"{% endif %}{% if checked %} checked=\"checked\"{% endif %} />
    {% endspaceless %}
{% endblock radio_widget %}

{% block datetime_widget %}
    {% spaceless %}
        {% if widget == 'single_text' %}
            {{ block('form_widget_simple') }}
        {% else %}
            {% set attr = attr|merge({ 'class': 'bootstrap-datetime' }) %}
            <div {{ block('widget_container_attributes') }}>
                {{ form_widget(form.date) }}
                {{ form_widget(form.time) }}
                {{ form_errors(form.date) }}
                {{ form_errors(form.time) }}
            </div>
        {% endif %}
    {% endspaceless %}
{% endblock datetime_widget %}

{% block date_widget %}
    {% spaceless %}
        {% if widget == 'single_text' %}
            {{ block('form_widget_simple') }}
        {% else %}
            {% set attr = attr|merge({ 'class': 'bootstrap-date' }) %}
            <div {{ block('widget_container_attributes') }}>
                {{ date_pattern|replace({
                '{{ year }}':  form_widget(form.year),
                '{{ month }}': form_widget(form.month),
                '{{ day }}':   form_widget(form.day),
                })|raw }}
            </div>
        {% endif %}
    {% endspaceless %}
{% endblock date_widget %}

{% block time_widget %}
    {% spaceless %}
        {% if widget == 'single_text' %}
            {{ block('form_widget_simple') }}
        {% else %}
            {% set vars = widget == 'text' ? { 'attr': { 'size': 1 }} : {} %}
            {% set attr = attr|merge({ 'class': 'bootstrap-time' }) %}
            <div {{ block('widget_container_attributes') }}>
                {{ form_widget(form.hour, vars) }}
                {% if with_minutes %}:{{ form_widget(form.minute, vars) }}{% endif %}
                {% if with_seconds %}:{{ form_widget(form.second, vars) }}{% endif %}
            </div>
        {% endif %}
    {% endspaceless %}
{% endblock time_widget %}

{% block number_widget %}
    {% spaceless %}
        {# type=\"number\" doesn't work with floats #}
        {% set type = type|default('text') %}
        {{ block('form_widget_simple') }}
    {% endspaceless %}
{% endblock number_widget %}

{% block integer_widget %}
    {% spaceless %}
        {% set type = type|default('number') %}
        {{ block('form_widget_simple') }}
    {% endspaceless %}
{% endblock integer_widget %}

{% block money_widget %}
    {% spaceless %}
        <div class=\"input-group\">
            {{ money_pattern|replace({
            '{{ widget }}': block('form_widget_simple'),
            '{{ tag_start }}': '<span class=\"input-group-addon\">',
            '{{ tag_end }}': '</span>'
            })|raw }}
        </div>
    {% endspaceless %}
{% endblock money_widget %}

{% block url_widget %}
    {% spaceless %}
        {% set type = type|default('url') %}
        {{ block('form_widget_simple') }}
    {% endspaceless %}
{% endblock url_widget %}

{% block search_widget %}
    {% spaceless %}
        {% set type = type|default('search') %}
        {{ block('form_widget_simple') }}
    {% endspaceless %}
{% endblock search_widget %}

{% block percent_widget %}
    {% spaceless %}
        {% set type = type|default('text') %}
        <div class=\"input-group\">
            {{ block('form_widget_simple') }}
            <span class=\"input-group-addon\">%</span>
        </div>
    {% endspaceless %}
{% endblock percent_widget %}

{% block password_widget %}
    {% spaceless %}
        {% set type = type|default('password') %}
        {{ block('form_widget_simple') }}
    {% endspaceless %}
{% endblock password_widget %}

{% block hidden_widget %}
    {% spaceless %}
        {% set type = type|default('hidden') %}
        {{ block('form_widget_simple') }}
    {% endspaceless %}
{% endblock hidden_widget %}

{% block email_widget %}
    {% spaceless %}
        {% set type = type|default('email') %}
        {{ block('form_widget_simple') }}
    {% endspaceless %}
{% endblock email_widget %}

{% block button_widget %}
    {% spaceless %}
        {% if label is empty and label is not same as (false) %}
            {% set label = name|humanize %}
        {% endif %}
        {% if type is defined and type == 'submit' %}
            {% set attr = attr|merge({ 'class': (attr.class|default('') ~ ' btn btn-'~button_class|default('primary'))|trim }) %}
        {% else %}
            {% set attr = attr|merge({ 'class': (attr.class|default('') ~ ' btn btn-'~button_class|default('default'))|trim }) %}
        {% endif %}
        {% if as_link is defined and as_link == true %}
            <a {{ block('button_attributes') }}>{% if attr.icon is defined and attr.icon != '' %}{{ icon(attr.icon) }}{% endif %}{{ label|trans({}, translation_domain) }}</a>
        {% else %}
            <button type=\"{{ type|default('button') }}\" {{ block('button_attributes') }}>{% if attr.icon is defined and attr.icon != '' %}{{ icon(attr.icon) }}{% endif %}{{ label|trans({}, translation_domain) }}</button>
        {% endif %}
    {% endspaceless %}
{% endblock button_widget %}

{% block submit_widget %}
    {% spaceless %}
        {% set type = type|default('submit') %}
        {{ block('button_widget') }}
    {% endspaceless %}
{% endblock submit_widget %}

{% block reset_widget %}
    {% spaceless %}
        {% set type = type|default('reset') %}
        {{ block('button_widget') }}
    {% endspaceless %}
{% endblock reset_widget %}

{% block form_actions_widget %}
    {% for button in form.children %}
        {{ form_widget(button) }}&nbsp; {# this needs to be here due to https://github.com/twbs/bootstrap/issues/3245 #}
    {% endfor  %}
{% endblock %}

{% block bs_static_widget %}
    {% set attr = attr|merge({ 'class': (attr.class|default('') ~ ' form-control-static')|trim }) %}
    <p id=\"{{ id }}\" {%- for attrname, attrvalue in attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>{{ value }}</p>
{% endblock %}

{# Labels #}

{% block form_label %}
    {% spaceless %}
        {% set col_size = col_size|default(bootstrap_get_col_size()) %}

        {% if attr.label_col is defined and attr.label_col is not empty %}
            {% set label_col = attr.label_col %}
        {% endif %}
        {% if attr.widget_col is defined and attr.widget_col is not empty %}
            {% set widget_col = attr.widget_col %}
        {% endif %}
        {% if attr.col_size is defined and attr.col_size is not empty %}
            {% set col_size = attr.col_size %}
        {% endif %}

        {% if label is not same as (false) %}
            {% set style = style|default(bootstrap_get_style()) %}
            {% set label_col = label_col|default(bootstrap_get_label_col()) %}

            {% if attr.style is defined and attr.style is not empty %}
                {% set style = attr.style %}
            {% endif %}

            {% set label_attr = label_attr|merge({ 'class': (label_attr.class|default('') ~ ' control-label')|trim }) %}
            {% if style == 'horizontal' %}
                {% set label_attr = label_attr|merge({ 'class': (label_attr.class|default('') ~ ' col-' ~ col_size ~ '-' ~ label_col)|trim }) %}
            {% elseif style == 'inline' %}
                {% set label_attr = label_attr|merge({ 'class': (label_attr.class|default('') ~ ' sr-only')|trim }) %}
            {% endif %}

            {% if not compound %}
                {% set label_attr = label_attr|merge({'for': id}) %}
            {% endif %}
            {% if required %}
                {% set label_attr = label_attr|merge({'class': (label_attr.class|default('') ~ ' required')|trim}) %}
            {% endif %}
            {% if label is empty %}
                {% set label = name|humanize %}
            {% endif %}
            <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>{{ label|trans({}, translation_domain)|raw }}</label>
        {% endif %}
    {% endspaceless %}
{% endblock form_label %}

{% block button_label %}{% endblock %}

{# Rows #}

{% block repeated_row %}
    {% spaceless %}
        {#
        No need to render the errors here, as all errors are mapped
        to the first child (see RepeatedTypeValidatorExtension).
        #}
        {{ block('form_rows') }}
    {% endspaceless %}
{% endblock repeated_row %}

{% block form_row %}
    {% spaceless %}
        {% set style = style|default(bootstrap_get_style()) %}
        {% set col_size = col_size|default(bootstrap_get_col_size()) %}

        {% if attr.label_col is defined and attr.label_col is not empty %}
            {% set label_col = attr.label_col %}
        {% endif %}
        {% if attr.widget_col is defined and attr.widget_col is not empty %}
            {% set widget_col = attr.widget_col %}
            {% if label is same as (false) and label_col is defined %}
                {% set widget_col = widget_col + label_col %}
            {% endif %}
        {% endif %}
        {% if attr.col_size is defined and attr.col_size is not empty %}
            {% set col_size = attr.col_size %}
        {% endif %}
        {% if attr.style is defined and attr.style is not empty %}
            {% set style = attr.style %}
        {% endif %}

        {% set label_col = label_col|default(bootstrap_get_label_col()) %}
        {% set widget_col = widget_col|default(bootstrap_get_widget_col()) %}

        <div class=\"form-group{% if form.vars.errors|length > 0 %} has-error{% endif %}\">
            {% if style == 'horizontal' %}
                {{ form_label(form) }}
                <div class=\"col-{{ col_size }}-{{ widget_col }}\">
                    {{ block('form_input_group') }}
                    {{ block('form_help') }}
                    {{ form_errors(form) }}
                </div>
            {% else %}
                {{ form_label(form) }}
                {{ block('form_input_group') }}
                {{ block('form_help') }}
                {{ form_errors(form) }}
            {% endif %}
        </div>

        {% if style == 'inline' %}&nbsp;{% endif %}
    {% endspaceless %}
{% endblock form_row %}

{% block form_input_group %}
    {% spaceless %}
        {% if attr.input_group is defined and attr.input_group is not empty %}
            {% set input_group = attr.input_group %}
        {% endif %}
        {% set input_group = input_group|default({}) %}
        {% if input_group is not empty %}
            {% set ig_size_class = '' %}
            {% if input_group.size is defined and input_group.size == 'large' %}
                {% set ig_size_class = ' input-group-lg' %}
            {% endif  %}
            {% if input_group.size is defined and input_group.size == 'small' %}
                {% set ig_size_class = ' input-group-sm' %}
            {% endif  %}
            <div class=\"input-group{{ ig_size_class }}\">
                {% if input_group.prepend is defined and input_group.prepend is not empty %}
                    <span class=\"input-group-addon\">{{ input_group.prepend|raw|parse_icons }}</span>
                {% endif %}
                {% if input_group.button_prepend is defined and input_group.button_prepend is not empty %}
                    <span class=\"input-group-btn\">{{ form_widget(input_group_button_prepend) }}</span>
                {% endif %}
                {{ form_widget(form) }}
                {% if input_group.button_append is defined and input_group.button_append is not empty %}
                    <span class=\"input-group-btn\">{{ form_widget(input_group_button_append) }}</span>
                {% endif %}
                {% if input_group.append is defined and input_group.append is not empty %}
                    <span class=\"input-group-addon\">{{ input_group.append|raw|parse_icons }}</span>
                {% endif %}
            </div>
        {% else %}
            {{ form_widget(form) }}
        {% endif %}
    {% endspaceless %}
{% endblock form_input_group %}

{% block form_help %}
    {% spaceless %}
        {% if attr.help_text is defined and attr.help_text is not empty %}
            {% set help_text = attr.help_text %}
        {% endif %}
        {% set help_text = help_text|default('') %}
        {% if help_text is not empty %}
            <span class=\"help-block\">{{ help_text|trans({}, translation_domain) }}</span>
        {% endif %}
    {% endspaceless %}
{% endblock form_help %}

{% block button_row %}
    {% spaceless %}
        {% set style = style|default(bootstrap_get_style()) %}
        {% set col_size = col_size|default(bootstrap_get_col_size()) %}

        {% if attr.label_col is defined and attr.label_col is not empty %}
            {% set label_col = attr.label_col %}
        {% endif %}
        {% if attr.widget_col is defined and attr.widget_col is not empty %}
            {% set widget_col = attr.widget_col %}
        {% endif %}
        {% if attr.col_size is defined and attr.col_size is not empty %}
            {% set col_size = attr.col_size %}
        {% endif %}
        {% if attr.style is defined and attr.style is not empty %}
            {% set style = attr.style %}
        {% endif %}

        {% set label_col = label_col|default(bootstrap_get_label_col()) %}
        {% set widget_col = widget_col|default(bootstrap_get_widget_col()) %}

        <div class=\"form-group\">
            {% if style == 'horizontal' %}
                <div class=\"col-{{ col_size }}-offset-{{ label_col }} col-{{ col_size }}-{{ widget_col }}\">
            {% endif %}

            {{ form_widget(form) }}

            {% if style == 'horizontal' %}
                </div>
            {% endif %}
        </div>
    {% endspaceless %}
{% endblock button_row %}

{% block hidden_row %}
    {{ form_widget(form) }}
{% endblock hidden_row %}

{% block form_actions_row %}
    {{ block('button_row')  }}
{% endblock %}

{# Misc #}

{% block form %}
    {% spaceless %}
        {{ form_start(form) }}
        {{ form_widget(form) }}
        {{ form_end(form) }}
    {% endspaceless %}
{% endblock form %}

{% block form_start %}
    {% spaceless %}
        {{ bootstrap_backup_form_settings() }}
        {% set method = method|upper %}
        {% if method in [\"GET\", \"POST\"] %}
            {% set form_method = method %}
        {% else %}
            {% set form_method = \"POST\" %}
        {% endif %}

        {% if style is defined %}
            {% set attr = attr|merge({ 'class': (attr.class|default('') ~ ' form-' ~ style)|trim }) %}
            {{ bootstrap_set_style(style) }}
        {% endif %}

        {% if form.vars.errors|length > 0 %}
            {% set attr = attr|merge({ 'class': (attr.class|default('') ~ ' has-global-errors')|trim }) %}
        {% endif %}

        {% if col_size is defined %}
            {{ bootstrap_set_col_size(col_size) }}
        {% endif %}

        {% if widget_col is defined %}
            {{ bootstrap_set_widget_col(widget_col) }}
        {% endif %}

        {% if label_col is defined %}
            {{ bootstrap_set_label_col(label_col) }}
        {% endif %}

        {% if simple_col is defined %}
            {{ bootstrap_set_simple_col(simple_col) }}
        {% endif %}

        {% if attr.role is not defined or attr.role is empty %}
            {% set attr = attr|merge({ 'role': 'form' }) %}
        {% endif %}

        <form  name=\"{{ form.vars.name }}\" method=\"{{ form_method|lower }}\" action=\"{{ action }}\"{% for attrname, attrvalue in attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}{% if multipart %} enctype=\"multipart/form-data\"{% endif %}>
        {% if form_method != method %}
            <input type=\"hidden\" name=\"_method\" value=\"{{ method }}\" />
        {% endif %}
    {% endspaceless %}
{% endblock form_start %}

{% block form_end %}
    {% spaceless %}
        {% if not render_rest is defined or render_rest %}
            {{ form_rest(form) }}
        {% endif %}
        </form>
        {% if bootstrap_get_style() %}
            {{ bootstrap_set_style('') }}
        {% endif %}
        {% if bootstrap_get_col_size() %}
            {{ bootstrap_set_col_size('lg') }}
        {% endif %}
        {% if bootstrap_get_widget_col() %}
            {{ bootstrap_set_widget_col(10) }}
        {% endif %}
        {% if bootstrap_get_label_col() %}
            {{ bootstrap_set_label_col(2) }}
        {% endif %}
        {% if bootstrap_get_simple_col() %}
            {{ bootstrap_set_simple_col(false) }}
        {% endif %}
    {{ bootstrap_restore_form_settings() }}
    {% endspaceless %}
{% endblock form_end %}

{% block form_enctype %}
    {% spaceless %}
        {% if multipart %}enctype=\"multipart/form-data\"{% endif %}
    {% endspaceless %}
{% endblock form_enctype %}

{% block global_form_errors %}
    {% if errors|length > 0 %}
        {% set global_errors = true %}
        {{ block('form_errors') }}
    {% endif %}
{% endblock global_form_errors %}

{% block form_errors %}
    {% spaceless %}
        {% set global_errors = global_errors|default(false) %}
        {% set style = style|default(bootstrap_get_style()) %}

        {% if attr.style is defined and attr.style is not empty %}
            {% set style = attr.style %}
        {% endif %}

        {% if errors|length > 0 %}
            {% if global_errors %}
                <div class=\"alert alert-danger\">
            {% endif %}
            <ul{% if not global_errors %} class=\"help-block\"{% endif %}>
                {% for error in errors %}
                    <li>{{ error.message }}</li>
                {% endfor %}
            </ul>
            {% if global_errors == true %}
                </div>
            {% endif %}
        {% endif %}
    {% endspaceless %}
{% endblock form_errors %}

{% block form_rest %}
    {% spaceless %}
        {% for child in form %}
            {% if not child.rendered %}
                {{ form_row(child) }}
            {% endif %}
        {% endfor %}
    {% endspaceless %}
{% endblock form_rest %}

{# Support #}

{% block form_rows %}
    {% spaceless %}
        {% for child in form %}
            {% set childAttr = {} %}
            {% if attr.col_size is defined %}
                {% set childAttr = childAttr|merge({ 'col_size': attr.col_size }) %}
            {% endif %}
            {% if attr.widget_col is defined %}
                {% set childAttr = childAttr|merge({ 'widget_col': attr.widget_col }) %}
            {% endif %}
            {% if attr.label_col is defined %}
                {% set childAttr = childAttr|merge({ 'label_col': attr.label_col }) %}
            {% endif %}
            {% if attr.simple_col is defined %}
                {% set childAttr = childAttr|merge({ 'simple_col': attr.simple_col }) %}
            {% endif %}
            {% if attr.style is defined %}
                {% set childAttr = childAttr|merge({ 'style': attr.style }) %}
            {% endif %}
            {{ form_row(child, childAttr) }}
        {% endfor %}
    {% endspaceless %}
{% endblock form_rows %}

{% block widget_attributes %}
    {% spaceless %}
        id=\"{{ id }}\" name=\"{{ full_name }}\" {% if disabled %} disabled=\"disabled\"{% endif %}{% if required %} required=\"required\"{% endif %}
        {% for attrname, attrvalue in attr %}{% if attrname in ['placeholder', 'title'] %}{{ attrname }}=\"{{ attrvalue|trans({}, translation_domain) }}\" {% elseif attrname in ['input_group'] %}{% else %}{{ attrname }}=\"{{ attrvalue }}\" {% endif %}{% endfor %}
    {% endspaceless %}
{% endblock widget_attributes %}

{% block widget_container_attributes %}
    {% spaceless %}
        {% if attr.style is defined and (attr.style == 'inline' or attr.style == 'horizontal') %}
            {% set attr = attr|merge({ 'class': ('form-'~attr.style~' '~attr.class|default(''))|trim }) %}
            {% set attr = attr|merge({ 'style': null }) %}
        {% endif %}
        {% if id is not empty %}id=\"{{ id }}\" {% endif %}
        {% for attrname, attrvalue in attr %}{% if attrvalue is not null and (attrvalue is not iterable) %}{{ attrname }}=\"{{ attrvalue }}\" {% endif %}{% endfor %}
    {% endspaceless %}
{% endblock widget_container_attributes %}

{% block button_attributes -%}
        id=\"{{ id }}\" name=\"{{ full_name }}\"{% if disabled %} disabled=\"disabled\"{% endif %}
        {%- for attrname, attrvalue in attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}
{% endblock button_attributes %}
", "BraincraftedBootstrapBundle:Form:bootstrap.html.twig", "/Users/matt/Prog/Symfo/Mymfo/vendor/braincrafted/bootstrap-bundle/Braincrafted/Bundle/BootstrapBundle/Resources/views/Form/bootstrap.html.twig");
    }
}
