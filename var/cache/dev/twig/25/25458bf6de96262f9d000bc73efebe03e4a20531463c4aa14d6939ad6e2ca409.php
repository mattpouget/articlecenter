<?php

/* base.html.twig */
class __TwigTemplate_8148080fcc98ceacd9e8c757a522a07931a5ace051a3718fc8e60b99b528805b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dcfa7c81090d10804c89d156a862ac2e02522611e93f7ee165b5b8f6234b3feb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dcfa7c81090d10804c89d156a862ac2e02522611e93f7ee165b5b8f6234b3feb->enter($__internal_dcfa7c81090d10804c89d156a862ac2e02522611e93f7ee165b5b8f6234b3feb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_a3dcc52969a75b918d22d82dbe6ca151f0e2ac8226384f3b48a7988b78f13294 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3dcc52969a75b918d22d82dbe6ca151f0e2ac8226384f3b48a7988b78f13294->enter($__internal_a3dcc52969a75b918d22d82dbe6ca151f0e2ac8226384f3b48a7988b78f13294_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_dcfa7c81090d10804c89d156a862ac2e02522611e93f7ee165b5b8f6234b3feb->leave($__internal_dcfa7c81090d10804c89d156a862ac2e02522611e93f7ee165b5b8f6234b3feb_prof);

        
        $__internal_a3dcc52969a75b918d22d82dbe6ca151f0e2ac8226384f3b48a7988b78f13294->leave($__internal_a3dcc52969a75b918d22d82dbe6ca151f0e2ac8226384f3b48a7988b78f13294_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_10856d110d906bdc355803aca2c3e097ec353c39c17de0c3eec898f6f99bdcc9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_10856d110d906bdc355803aca2c3e097ec353c39c17de0c3eec898f6f99bdcc9->enter($__internal_10856d110d906bdc355803aca2c3e097ec353c39c17de0c3eec898f6f99bdcc9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_d828604ff11bd975c4c35f9a3b6a26d0a4b8232ead3c530a9b13e0f7588a571d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d828604ff11bd975c4c35f9a3b6a26d0a4b8232ead3c530a9b13e0f7588a571d->enter($__internal_d828604ff11bd975c4c35f9a3b6a26d0a4b8232ead3c530a9b13e0f7588a571d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_d828604ff11bd975c4c35f9a3b6a26d0a4b8232ead3c530a9b13e0f7588a571d->leave($__internal_d828604ff11bd975c4c35f9a3b6a26d0a4b8232ead3c530a9b13e0f7588a571d_prof);

        
        $__internal_10856d110d906bdc355803aca2c3e097ec353c39c17de0c3eec898f6f99bdcc9->leave($__internal_10856d110d906bdc355803aca2c3e097ec353c39c17de0c3eec898f6f99bdcc9_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_beb5fe414e5d26c1c73377ee8bce54b6093439076f908ff10b72f3ff4a6e341a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_beb5fe414e5d26c1c73377ee8bce54b6093439076f908ff10b72f3ff4a6e341a->enter($__internal_beb5fe414e5d26c1c73377ee8bce54b6093439076f908ff10b72f3ff4a6e341a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_fb0aa6c67bd1780edefdd97bfbc8dc538eca14a9f3d0097e1577e173339dbb95 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fb0aa6c67bd1780edefdd97bfbc8dc538eca14a9f3d0097e1577e173339dbb95->enter($__internal_fb0aa6c67bd1780edefdd97bfbc8dc538eca14a9f3d0097e1577e173339dbb95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_fb0aa6c67bd1780edefdd97bfbc8dc538eca14a9f3d0097e1577e173339dbb95->leave($__internal_fb0aa6c67bd1780edefdd97bfbc8dc538eca14a9f3d0097e1577e173339dbb95_prof);

        
        $__internal_beb5fe414e5d26c1c73377ee8bce54b6093439076f908ff10b72f3ff4a6e341a->leave($__internal_beb5fe414e5d26c1c73377ee8bce54b6093439076f908ff10b72f3ff4a6e341a_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_fd3f513afb856a8d1c7a5e693dfb376874474f2f9812d6efc26d21b253d46ced = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd3f513afb856a8d1c7a5e693dfb376874474f2f9812d6efc26d21b253d46ced->enter($__internal_fd3f513afb856a8d1c7a5e693dfb376874474f2f9812d6efc26d21b253d46ced_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_cfb97ac77fffb9393fa419b27434525422dad6920f69c2eafa8cf6cc8e724261 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cfb97ac77fffb9393fa419b27434525422dad6920f69c2eafa8cf6cc8e724261->enter($__internal_cfb97ac77fffb9393fa419b27434525422dad6920f69c2eafa8cf6cc8e724261_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_cfb97ac77fffb9393fa419b27434525422dad6920f69c2eafa8cf6cc8e724261->leave($__internal_cfb97ac77fffb9393fa419b27434525422dad6920f69c2eafa8cf6cc8e724261_prof);

        
        $__internal_fd3f513afb856a8d1c7a5e693dfb376874474f2f9812d6efc26d21b253d46ced->leave($__internal_fd3f513afb856a8d1c7a5e693dfb376874474f2f9812d6efc26d21b253d46ced_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_fd98d386fa2a975537e7d44e4edad674eda3e74dca10c2a4256e4536e0c9d7d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd98d386fa2a975537e7d44e4edad674eda3e74dca10c2a4256e4536e0c9d7d2->enter($__internal_fd98d386fa2a975537e7d44e4edad674eda3e74dca10c2a4256e4536e0c9d7d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_15cd283a97a90793bda7b485a5ecb7fce6d264a4c55a43471d95b95b20ecd960 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15cd283a97a90793bda7b485a5ecb7fce6d264a4c55a43471d95b95b20ecd960->enter($__internal_15cd283a97a90793bda7b485a5ecb7fce6d264a4c55a43471d95b95b20ecd960_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_15cd283a97a90793bda7b485a5ecb7fce6d264a4c55a43471d95b95b20ecd960->leave($__internal_15cd283a97a90793bda7b485a5ecb7fce6d264a4c55a43471d95b95b20ecd960_prof);

        
        $__internal_fd98d386fa2a975537e7d44e4edad674eda3e74dca10c2a4256e4536e0c9d7d2->leave($__internal_fd98d386fa2a975537e7d44e4edad674eda3e74dca10c2a4256e4536e0c9d7d2_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/base.html.twig");
    }
}
