<?php

/* genus/catlist.html.twig */
class __TwigTemplate_eb670a25e65ee4189554622f85478e0e3214abaaa3db88f863633639b0325ee5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "genus/catlist.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8390dbf50906d64d02baa5cad0f8ca24e6afd775310322b6033b303d155f294b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8390dbf50906d64d02baa5cad0f8ca24e6afd775310322b6033b303d155f294b->enter($__internal_8390dbf50906d64d02baa5cad0f8ca24e6afd775310322b6033b303d155f294b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/catlist.html.twig"));

        $__internal_d7157c5529becf95694ace9273f73099b5e96f7d2b0f13d3b77176509f5e0b31 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7157c5529becf95694ace9273f73099b5e96f7d2b0f13d3b77176509f5e0b31->enter($__internal_d7157c5529becf95694ace9273f73099b5e96f7d2b0f13d3b77176509f5e0b31_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "genus/catlist.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8390dbf50906d64d02baa5cad0f8ca24e6afd775310322b6033b303d155f294b->leave($__internal_8390dbf50906d64d02baa5cad0f8ca24e6afd775310322b6033b303d155f294b_prof);

        
        $__internal_d7157c5529becf95694ace9273f73099b5e96f7d2b0f13d3b77176509f5e0b31->leave($__internal_d7157c5529becf95694ace9273f73099b5e96f7d2b0f13d3b77176509f5e0b31_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_fbf2790e204cd4f6e4ff2067ddc5e50a76998a373320de2e78bacdd44e1af503 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fbf2790e204cd4f6e4ff2067ddc5e50a76998a373320de2e78bacdd44e1af503->enter($__internal_fbf2790e204cd4f6e4ff2067ddc5e50a76998a373320de2e78bacdd44e1af503_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_46c8256368bb47e601e3e25d399ba2468c7d930a89d4a41cf61fa567d3b47a77 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46c8256368bb47e601e3e25d399ba2468c7d930a89d4a41cf61fa567d3b47a77->enter($__internal_46c8256368bb47e601e3e25d399ba2468c7d930a89d4a41cf61fa567d3b47a77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Article Center";
        
        $__internal_46c8256368bb47e601e3e25d399ba2468c7d930a89d4a41cf61fa567d3b47a77->leave($__internal_46c8256368bb47e601e3e25d399ba2468c7d930a89d4a41cf61fa567d3b47a77_prof);

        
        $__internal_fbf2790e204cd4f6e4ff2067ddc5e50a76998a373320de2e78bacdd44e1af503->leave($__internal_fbf2790e204cd4f6e4ff2067ddc5e50a76998a373320de2e78bacdd44e1af503_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_4ada68b863c4d14d7231ff72b3603d312ae4f29ffb69a4f9cdfb181afe00b487 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ada68b863c4d14d7231ff72b3603d312ae4f29ffb69a4f9cdfb181afe00b487->enter($__internal_4ada68b863c4d14d7231ff72b3603d312ae4f29ffb69a4f9cdfb181afe00b487_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_31f9542b6bd68f256d26dc3e4092c4d5317aea3b5489243620c0ea73b328c388 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_31f9542b6bd68f256d26dc3e4092c4d5317aea3b5489243620c0ea73b328c388->enter($__internal_31f9542b6bd68f256d26dc3e4092c4d5317aea3b5489243620c0ea73b328c388_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.min.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"listcat\">Catégories</a></li>
                    ";
        // line 25
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 26
            echo "                        <li><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\" class=\"btn btn-danger\">Logout</a></li>
                    ";
        } else {
            // line 28
            echo "                        <li><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\" class=\"btn btn-success\">Login</a></li>
                    ";
        }
        // line 30
        echo "                    <li><a href=\"/register\" class=\"btn btn-info\">Sign up.</a></li>
                </ul>
                ";
        // line 32
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            // line 33
            echo "                    <ul class=\"nav navbar-nav navbar-right\">
                        <li><a href=\"/admin\">AdminBoard</a></li>
                    </ul>
                ";
        }
        // line 37
        echo "            </div>
        </div>
    </nav>

    <div class=\"jumbotron\">
        <h1>Toutes les catégories</h1>
        <table class=\"table table-striped table-hover \">
            <tbody>
            ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["category"]);
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 46
            echo "                <tr class=\"active\">
                    <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "</td>
                    <td><a href=\"/listartcat/";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
            echo "\" class=\"btn btn-primary btn-xs\">Voir les articles &rarr;</a></td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "            </tbody>
        </table>
    </div>


";
        
        $__internal_31f9542b6bd68f256d26dc3e4092c4d5317aea3b5489243620c0ea73b328c388->leave($__internal_31f9542b6bd68f256d26dc3e4092c4d5317aea3b5489243620c0ea73b328c388_prof);

        
        $__internal_4ada68b863c4d14d7231ff72b3603d312ae4f29ffb69a4f9cdfb181afe00b487->leave($__internal_4ada68b863c4d14d7231ff72b3603d312ae4f29ffb69a4f9cdfb181afe00b487_prof);

    }

    public function getTemplateName()
    {
        return "genus/catlist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 52,  143 => 49,  139 => 48,  135 => 47,  132 => 46,  128 => 45,  118 => 37,  112 => 33,  110 => 32,  106 => 30,  100 => 28,  94 => 26,  92 => 25,  71 => 7,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %}Article Center{% endblock %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.min.css') }}\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"listcat\">Catégories</a></li>
                    {% if is_granted('ROLE_USER') %}
                        <li><a href=\"{{ path('fos_user_security_logout') }}\" class=\"btn btn-danger\">Logout</a></li>
                    {% else %}
                        <li><a href=\"{{ path('fos_user_security_login') }}\" class=\"btn btn-success\">Login</a></li>
                    {% endif %}
                    <li><a href=\"/register\" class=\"btn btn-info\">Sign up.</a></li>
                </ul>
                {% if is_granted('ROLE_ADMIN') %}
                    <ul class=\"nav navbar-nav navbar-right\">
                        <li><a href=\"/admin\">AdminBoard</a></li>
                    </ul>
                {% endif %}
            </div>
        </div>
    </nav>

    <div class=\"jumbotron\">
        <h1>Toutes les catégories</h1>
        <table class=\"table table-striped table-hover \">
            <tbody>
            {% for category in category %}
                <tr class=\"active\">
                    <td>{{ category.id }}</td>
                    <td>{{ category.name }}</td>
                    <td><a href=\"/listartcat/{{ category.id }}\" class=\"btn btn-primary btn-xs\">Voir les articles &rarr;</a></td>
                </tr>
            {% endfor %}
            </tbody>
        </table>
    </div>


{% endblock %}", "genus/catlist.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/genus/catlist.html.twig");
    }
}
