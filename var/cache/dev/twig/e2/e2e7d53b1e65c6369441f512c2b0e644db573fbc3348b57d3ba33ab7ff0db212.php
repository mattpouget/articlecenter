<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_29f90a52b071e2072cb529b94fcce70bbb725998e4a548f86e92fa71778068f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b4d14f31877e35f9d1ee7b0aad53b9eec2ed78b4c28c5a10a82a286d4ba8d00 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7b4d14f31877e35f9d1ee7b0aad53b9eec2ed78b4c28c5a10a82a286d4ba8d00->enter($__internal_7b4d14f31877e35f9d1ee7b0aad53b9eec2ed78b4c28c5a10a82a286d4ba8d00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_376b1c0807d858d11c5944abc0451e384d2d96ed819ba1452e6859e8e6659b51 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_376b1c0807d858d11c5944abc0451e384d2d96ed819ba1452e6859e8e6659b51->enter($__internal_376b1c0807d858d11c5944abc0451e384d2d96ed819ba1452e6859e8e6659b51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7b4d14f31877e35f9d1ee7b0aad53b9eec2ed78b4c28c5a10a82a286d4ba8d00->leave($__internal_7b4d14f31877e35f9d1ee7b0aad53b9eec2ed78b4c28c5a10a82a286d4ba8d00_prof);

        
        $__internal_376b1c0807d858d11c5944abc0451e384d2d96ed819ba1452e6859e8e6659b51->leave($__internal_376b1c0807d858d11c5944abc0451e384d2d96ed819ba1452e6859e8e6659b51_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_0a6b17a5ba8936528a85e89471ae117872242485b1a374e8071a36441f56b8e2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a6b17a5ba8936528a85e89471ae117872242485b1a374e8071a36441f56b8e2->enter($__internal_0a6b17a5ba8936528a85e89471ae117872242485b1a374e8071a36441f56b8e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_e2d43e28bfed45860a7fe6cc329a951706ab9106e9d19f76ba045e86640c0beb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e2d43e28bfed45860a7fe6cc329a951706ab9106e9d19f76ba045e86640c0beb->enter($__internal_e2d43e28bfed45860a7fe6cc329a951706ab9106e9d19f76ba045e86640c0beb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_e2d43e28bfed45860a7fe6cc329a951706ab9106e9d19f76ba045e86640c0beb->leave($__internal_e2d43e28bfed45860a7fe6cc329a951706ab9106e9d19f76ba045e86640c0beb_prof);

        
        $__internal_0a6b17a5ba8936528a85e89471ae117872242485b1a374e8071a36441f56b8e2->leave($__internal_0a6b17a5ba8936528a85e89471ae117872242485b1a374e8071a36441f56b8e2_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_9fa9023d0a25a775c65eb7a0f1c15f3a8d9fb42219bbb64721cfcda0f5272476 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9fa9023d0a25a775c65eb7a0f1c15f3a8d9fb42219bbb64721cfcda0f5272476->enter($__internal_9fa9023d0a25a775c65eb7a0f1c15f3a8d9fb42219bbb64721cfcda0f5272476_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_4b6a80b4570b37be19782bc826a7aa325e13aa3af12f6848b1b5c56d68d6bf42 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4b6a80b4570b37be19782bc826a7aa325e13aa3af12f6848b1b5c56d68d6bf42->enter($__internal_4b6a80b4570b37be19782bc826a7aa325e13aa3af12f6848b1b5c56d68d6bf42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_4b6a80b4570b37be19782bc826a7aa325e13aa3af12f6848b1b5c56d68d6bf42->leave($__internal_4b6a80b4570b37be19782bc826a7aa325e13aa3af12f6848b1b5c56d68d6bf42_prof);

        
        $__internal_9fa9023d0a25a775c65eb7a0f1c15f3a8d9fb42219bbb64721cfcda0f5272476->leave($__internal_9fa9023d0a25a775c65eb7a0f1c15f3a8d9fb42219bbb64721cfcda0f5272476_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_7ed7824eaec810d65b40b76dc5f0045642f8dad93f10d172b4aca0e3eb6a069b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ed7824eaec810d65b40b76dc5f0045642f8dad93f10d172b4aca0e3eb6a069b->enter($__internal_7ed7824eaec810d65b40b76dc5f0045642f8dad93f10d172b4aca0e3eb6a069b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9d9a3ffd1744c85e8dd9906191e25d69df38b3e3f1f804df300f0afacceb3f40 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d9a3ffd1744c85e8dd9906191e25d69df38b3e3f1f804df300f0afacceb3f40->enter($__internal_9d9a3ffd1744c85e8dd9906191e25d69df38b3e3f1f804df300f0afacceb3f40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_9d9a3ffd1744c85e8dd9906191e25d69df38b3e3f1f804df300f0afacceb3f40->leave($__internal_9d9a3ffd1744c85e8dd9906191e25d69df38b3e3f1f804df300f0afacceb3f40_prof);

        
        $__internal_7ed7824eaec810d65b40b76dc5f0045642f8dad93f10d172b4aca0e3eb6a069b->leave($__internal_7ed7824eaec810d65b40b76dc5f0045642f8dad93f10d172b4aca0e3eb6a069b_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/Users/matt/Prog/Symfo/Mymfo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
