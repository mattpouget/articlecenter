<?php

/* article/index.html.twig */
class __TwigTemplate_7c39d2af90589fc5b3936e7d57befce1c726c983b9eabb7b716e6e98f446e1b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "article/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ca35c10d86064ac768be3c7f54cb745fe63116182c02e13bc38bffceb68b8356 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ca35c10d86064ac768be3c7f54cb745fe63116182c02e13bc38bffceb68b8356->enter($__internal_ca35c10d86064ac768be3c7f54cb745fe63116182c02e13bc38bffceb68b8356_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "article/index.html.twig"));

        $__internal_1da6a6849ffda40f3cc1641d024e305aaa67426ce1ff21a828129777509c0eee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1da6a6849ffda40f3cc1641d024e305aaa67426ce1ff21a828129777509c0eee->enter($__internal_1da6a6849ffda40f3cc1641d024e305aaa67426ce1ff21a828129777509c0eee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "article/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ca35c10d86064ac768be3c7f54cb745fe63116182c02e13bc38bffceb68b8356->leave($__internal_ca35c10d86064ac768be3c7f54cb745fe63116182c02e13bc38bffceb68b8356_prof);

        
        $__internal_1da6a6849ffda40f3cc1641d024e305aaa67426ce1ff21a828129777509c0eee->leave($__internal_1da6a6849ffda40f3cc1641d024e305aaa67426ce1ff21a828129777509c0eee_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_40b5e0cfe3c32d8d44c3268ed65d840a01de3f087ec4a53e9ea98a2b772da9b9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40b5e0cfe3c32d8d44c3268ed65d840a01de3f087ec4a53e9ea98a2b772da9b9->enter($__internal_40b5e0cfe3c32d8d44c3268ed65d840a01de3f087ec4a53e9ea98a2b772da9b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_109985c61f4fcb3f020debf573a6ec788c1a8530174933db36c18635ccc44480 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_109985c61f4fcb3f020debf573a6ec788c1a8530174933db36c18635ccc44480->enter($__internal_109985c61f4fcb3f020debf573a6ec788c1a8530174933db36c18635ccc44480_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.min.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>Articles list</h1>

    <table>
        <thead>
            <tr>
                <th class=\"text-warning\">Id</th>
                <th class=\"text-warning\">Name</th>
                <th class=\"text-warning\">Content</th>
                <th class=\"text-warning\">Author</th>
                <th class=\"text-warning\">Date</th>
                <th class=\"text-warning\">Category</th>
                <th class=\"text-warning\">Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["articles"]) ? $context["articles"] : $this->getContext($context, "articles")));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 43
            echo "            <tr>
                <td><a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("artcrud_show", array("id" => $this->getAttribute($context["article"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "name", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "content", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "author", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 48
            if ($this->getAttribute($context["article"], "date", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["article"], "date", array()), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["article"], "category", array()), "name", array()), "html", null, true);
            echo "</td>
                <td>
                    <a href=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("artcrud_show", array("id" => $this->getAttribute($context["article"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default\">show</a>
                    <a href=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("artcrud_edit", array("id" => $this->getAttribute($context["article"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default\">edit</a>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "        </tbody>
    </table>
    <a href=\"";
        // line 58
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("artcrud_new");
        echo "\" class=\"btn btn-default\">Create a new article</a>
";
        
        $__internal_109985c61f4fcb3f020debf573a6ec788c1a8530174933db36c18635ccc44480->leave($__internal_109985c61f4fcb3f020debf573a6ec788c1a8530174933db36c18635ccc44480_prof);

        
        $__internal_40b5e0cfe3c32d8d44c3268ed65d840a01de3f087ec4a53e9ea98a2b772da9b9->leave($__internal_40b5e0cfe3c32d8d44c3268ed65d840a01de3f087ec4a53e9ea98a2b772da9b9_prof);

    }

    public function getTemplateName()
    {
        return "article/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 58,  142 => 56,  132 => 52,  128 => 51,  123 => 49,  117 => 48,  113 => 47,  109 => 46,  105 => 45,  99 => 44,  96 => 43,  92 => 42,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.min.css') }}\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>Articles list</h1>

    <table>
        <thead>
            <tr>
                <th class=\"text-warning\">Id</th>
                <th class=\"text-warning\">Name</th>
                <th class=\"text-warning\">Content</th>
                <th class=\"text-warning\">Author</th>
                <th class=\"text-warning\">Date</th>
                <th class=\"text-warning\">Category</th>
                <th class=\"text-warning\">Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for article in articles %}
            <tr>
                <td><a href=\"{{ path('artcrud_show', { 'id': article.id }) }}\">{{ article.id }}</a></td>
                <td>{{ article.name }}</td>
                <td>{{ article.content }}</td>
                <td>{{ article.author }}</td>
                <td>{% if article.date %}{{ article.date|date('Y-m-d H:i:s') }}{% endif %}</td>
                <td>{{ article.category.name }}</td>
                <td>
                    <a href=\"{{ path('artcrud_show', { 'id': article.id }) }}\" class=\"btn btn-default\">show</a>
                    <a href=\"{{ path('artcrud_edit', { 'id': article.id }) }}\" class=\"btn btn-default\">edit</a>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
    <a href=\"{{ path('artcrud_new') }}\" class=\"btn btn-default\">Create a new article</a>
{% endblock %}
", "article/index.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/article/index.html.twig");
    }
}
