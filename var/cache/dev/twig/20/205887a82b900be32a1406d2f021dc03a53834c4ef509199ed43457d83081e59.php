<?php

/* user/edit.html.twig */
class __TwigTemplate_9b8a796cbd81041c93100836d4d68deed1b755d1e0956e6c62a8ff2e4dc5960e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "user/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5aff1c43636df79c3919286669d4b00551ec8b040d6fe5f32e8d43841f803dca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5aff1c43636df79c3919286669d4b00551ec8b040d6fe5f32e8d43841f803dca->enter($__internal_5aff1c43636df79c3919286669d4b00551ec8b040d6fe5f32e8d43841f803dca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "user/edit.html.twig"));

        $__internal_0c5415ead9fbf0c68b6ac4c9fe656f6dae59387424c828a15d365b39e17420f5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0c5415ead9fbf0c68b6ac4c9fe656f6dae59387424c828a15d365b39e17420f5->enter($__internal_0c5415ead9fbf0c68b6ac4c9fe656f6dae59387424c828a15d365b39e17420f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "user/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5aff1c43636df79c3919286669d4b00551ec8b040d6fe5f32e8d43841f803dca->leave($__internal_5aff1c43636df79c3919286669d4b00551ec8b040d6fe5f32e8d43841f803dca_prof);

        
        $__internal_0c5415ead9fbf0c68b6ac4c9fe656f6dae59387424c828a15d365b39e17420f5->leave($__internal_0c5415ead9fbf0c68b6ac4c9fe656f6dae59387424c828a15d365b39e17420f5_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_ab3603ca656ff05a67bdd84f02c8a9ffaa64ea87ff7843e054d800c33eedb255 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab3603ca656ff05a67bdd84f02c8a9ffaa64ea87ff7843e054d800c33eedb255->enter($__internal_ab3603ca656ff05a67bdd84f02c8a9ffaa64ea87ff7843e054d800c33eedb255_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_56cad377f3ae7aefd5cbd6ec778a1fc3f8094e63a8ff21958b7a711bae9c25e8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56cad377f3ae7aefd5cbd6ec778a1fc3f8094e63a8ff21958b7a711bae9c25e8->enter($__internal_56cad377f3ae7aefd5cbd6ec778a1fc3f8094e63a8ff21958b7a711bae9c25e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.min.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>User edit</h1>

    ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 30
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 32
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usercrud_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 39
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 41
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_56cad377f3ae7aefd5cbd6ec778a1fc3f8094e63a8ff21958b7a711bae9c25e8->leave($__internal_56cad377f3ae7aefd5cbd6ec778a1fc3f8094e63a8ff21958b7a711bae9c25e8_prof);

        
        $__internal_ab3603ca656ff05a67bdd84f02c8a9ffaa64ea87ff7843e054d800c33eedb255->leave($__internal_ab3603ca656ff05a67bdd84f02c8a9ffaa64ea87ff7843e054d800c33eedb255_prof);

    }

    public function getTemplateName()
    {
        return "user/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 41,  101 => 39,  95 => 36,  88 => 32,  83 => 30,  79 => 29,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.min.css') }}\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>User edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('usercrud_index') }}\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "user/edit.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/user/edit.html.twig");
    }
}
