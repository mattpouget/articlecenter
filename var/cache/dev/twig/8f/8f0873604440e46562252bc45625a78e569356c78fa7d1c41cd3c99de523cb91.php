<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_48dab8653f2423d0dafad3010d14d4ca4f4bb0da17d6b7a5c6bd0f8505e3872e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c0a2f3a1838b740af2e64ea22a5380797ef2761a4ff845b06737360d3cfcfbee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c0a2f3a1838b740af2e64ea22a5380797ef2761a4ff845b06737360d3cfcfbee->enter($__internal_c0a2f3a1838b740af2e64ea22a5380797ef2761a4ff845b06737360d3cfcfbee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_9b5c46af9fda45392f12f9999c72679106d322d8a16d8e61e1bbf422095c0b76 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b5c46af9fda45392f12f9999c72679106d322d8a16d8e61e1bbf422095c0b76->enter($__internal_9b5c46af9fda45392f12f9999c72679106d322d8a16d8e61e1bbf422095c0b76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c0a2f3a1838b740af2e64ea22a5380797ef2761a4ff845b06737360d3cfcfbee->leave($__internal_c0a2f3a1838b740af2e64ea22a5380797ef2761a4ff845b06737360d3cfcfbee_prof);

        
        $__internal_9b5c46af9fda45392f12f9999c72679106d322d8a16d8e61e1bbf422095c0b76->leave($__internal_9b5c46af9fda45392f12f9999c72679106d322d8a16d8e61e1bbf422095c0b76_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_879247ebd166700d9ac65da949b2125f4f5a9b6f04f143acf412177dde92592a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_879247ebd166700d9ac65da949b2125f4f5a9b6f04f143acf412177dde92592a->enter($__internal_879247ebd166700d9ac65da949b2125f4f5a9b6f04f143acf412177dde92592a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_fe9cad6bb9040adda035343ad467fc90d20657d9009d4ce9cb8b7394b6cd4248 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fe9cad6bb9040adda035343ad467fc90d20657d9009d4ce9cb8b7394b6cd4248->enter($__internal_fe9cad6bb9040adda035343ad467fc90d20657d9009d4ce9cb8b7394b6cd4248_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_fe9cad6bb9040adda035343ad467fc90d20657d9009d4ce9cb8b7394b6cd4248->leave($__internal_fe9cad6bb9040adda035343ad467fc90d20657d9009d4ce9cb8b7394b6cd4248_prof);

        
        $__internal_879247ebd166700d9ac65da949b2125f4f5a9b6f04f143acf412177dde92592a->leave($__internal_879247ebd166700d9ac65da949b2125f4f5a9b6f04f143acf412177dde92592a_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_550cb6b9091c21b97974c2aa7ad2833b00504339772f856f26f679c906155fd5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_550cb6b9091c21b97974c2aa7ad2833b00504339772f856f26f679c906155fd5->enter($__internal_550cb6b9091c21b97974c2aa7ad2833b00504339772f856f26f679c906155fd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_9fa8b20d58fd1349eb8716f96a9d8d5dedc8d7e6ebfbe3005e7e8d118388fe76 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9fa8b20d58fd1349eb8716f96a9d8d5dedc8d7e6ebfbe3005e7e8d118388fe76->enter($__internal_9fa8b20d58fd1349eb8716f96a9d8d5dedc8d7e6ebfbe3005e7e8d118388fe76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_9fa8b20d58fd1349eb8716f96a9d8d5dedc8d7e6ebfbe3005e7e8d118388fe76->leave($__internal_9fa8b20d58fd1349eb8716f96a9d8d5dedc8d7e6ebfbe3005e7e8d118388fe76_prof);

        
        $__internal_550cb6b9091c21b97974c2aa7ad2833b00504339772f856f26f679c906155fd5->leave($__internal_550cb6b9091c21b97974c2aa7ad2833b00504339772f856f26f679c906155fd5_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_6493ae435a40122f898b505ca516b6b69c169ec2b16a558af3b295b2c985ef89 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6493ae435a40122f898b505ca516b6b69c169ec2b16a558af3b295b2c985ef89->enter($__internal_6493ae435a40122f898b505ca516b6b69c169ec2b16a558af3b295b2c985ef89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_2334ea95227b4c805779286ce2b6482e43575a99ec250f9ea248b001f6b5ce90 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2334ea95227b4c805779286ce2b6482e43575a99ec250f9ea248b001f6b5ce90->enter($__internal_2334ea95227b4c805779286ce2b6482e43575a99ec250f9ea248b001f6b5ce90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_2334ea95227b4c805779286ce2b6482e43575a99ec250f9ea248b001f6b5ce90->leave($__internal_2334ea95227b4c805779286ce2b6482e43575a99ec250f9ea248b001f6b5ce90_prof);

        
        $__internal_6493ae435a40122f898b505ca516b6b69c169ec2b16a558af3b295b2c985ef89->leave($__internal_6493ae435a40122f898b505ca516b6b69c169ec2b16a558af3b295b2c985ef89_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/Users/matt/Prog/Symfo/Mymfo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
