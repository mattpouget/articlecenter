<?php

/* @FOSUser/Registration/confirmed.html.twig */
class __TwigTemplate_9d5913ce2d5c2c98481a7df3a1cea88ddad21f9df98bdde9908b1d6f94f5bb1a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3bb0e3a7aeb2eda09de2e81c40a390f89e663ffbc560acc094a9a5d0d6045549 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3bb0e3a7aeb2eda09de2e81c40a390f89e663ffbc560acc094a9a5d0d6045549->enter($__internal_3bb0e3a7aeb2eda09de2e81c40a390f89e663ffbc560acc094a9a5d0d6045549_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $__internal_ef32d77701eff296abbc026b6afea0e79fbaacaf799d13390225c4c68e21c85a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef32d77701eff296abbc026b6afea0e79fbaacaf799d13390225c4c68e21c85a->enter($__internal_ef32d77701eff296abbc026b6afea0e79fbaacaf799d13390225c4c68e21c85a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3bb0e3a7aeb2eda09de2e81c40a390f89e663ffbc560acc094a9a5d0d6045549->leave($__internal_3bb0e3a7aeb2eda09de2e81c40a390f89e663ffbc560acc094a9a5d0d6045549_prof);

        
        $__internal_ef32d77701eff296abbc026b6afea0e79fbaacaf799d13390225c4c68e21c85a->leave($__internal_ef32d77701eff296abbc026b6afea0e79fbaacaf799d13390225c4c68e21c85a_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b983ddfb3c16567ff81fe01773aa730a305b102207893d645e31cd8213109f55 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b983ddfb3c16567ff81fe01773aa730a305b102207893d645e31cd8213109f55->enter($__internal_b983ddfb3c16567ff81fe01773aa730a305b102207893d645e31cd8213109f55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_c1b65258a487d536b10dd27a041504596850f020b8ecc1ae7d1a5658dfa246b5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1b65258a487d536b10dd27a041504596850f020b8ecc1ae7d1a5658dfa246b5->enter($__internal_c1b65258a487d536b10dd27a041504596850f020b8ecc1ae7d1a5658dfa246b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if ((isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_c1b65258a487d536b10dd27a041504596850f020b8ecc1ae7d1a5658dfa246b5->leave($__internal_c1b65258a487d536b10dd27a041504596850f020b8ecc1ae7d1a5658dfa246b5_prof);

        
        $__internal_b983ddfb3c16567ff81fe01773aa730a305b102207893d645e31cd8213109f55->leave($__internal_b983ddfb3c16567ff81fe01773aa730a305b102207893d645e31cd8213109f55_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/confirmed.html.twig", "/Users/matt/Prog/Symfo/Mymfo/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/confirmed.html.twig");
    }
}
