<?php

/* @FOSUser/Registration/register.html.twig */
class __TwigTemplate_4a0374c66e6a12104ff9965e165d8d7397812e533102c0c8cc15a3dbb55ec6a7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fbe526bf0b3aaadeebfa4d2c15e2b8553db10cfcbbc3226a8c4b845e90fb38d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fbe526bf0b3aaadeebfa4d2c15e2b8553db10cfcbbc3226a8c4b845e90fb38d2->enter($__internal_fbe526bf0b3aaadeebfa4d2c15e2b8553db10cfcbbc3226a8c4b845e90fb38d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $__internal_d36567045e7a4f6fe16a28c70f9d331f2497cdc03bbdc3b8c9578b55967209cb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d36567045e7a4f6fe16a28c70f9d331f2497cdc03bbdc3b8c9578b55967209cb->enter($__internal_d36567045e7a4f6fe16a28c70f9d331f2497cdc03bbdc3b8c9578b55967209cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fbe526bf0b3aaadeebfa4d2c15e2b8553db10cfcbbc3226a8c4b845e90fb38d2->leave($__internal_fbe526bf0b3aaadeebfa4d2c15e2b8553db10cfcbbc3226a8c4b845e90fb38d2_prof);

        
        $__internal_d36567045e7a4f6fe16a28c70f9d331f2497cdc03bbdc3b8c9578b55967209cb->leave($__internal_d36567045e7a4f6fe16a28c70f9d331f2497cdc03bbdc3b8c9578b55967209cb_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_4132364b0334e4ca2e539be968159593d1d7e24647c07e33db9e28236da80573 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4132364b0334e4ca2e539be968159593d1d7e24647c07e33db9e28236da80573->enter($__internal_4132364b0334e4ca2e539be968159593d1d7e24647c07e33db9e28236da80573_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_bbd0ec1b7c781cf92f1d8625f9df9216364e5efe62d32e89aa7bb3cab8583d12 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bbd0ec1b7c781cf92f1d8625f9df9216364e5efe62d32e89aa7bb3cab8583d12->enter($__internal_bbd0ec1b7c781cf92f1d8625f9df9216364e5efe62d32e89aa7bb3cab8583d12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "@FOSUser/Registration/register.html.twig", 4)->display($context);
        
        $__internal_bbd0ec1b7c781cf92f1d8625f9df9216364e5efe62d32e89aa7bb3cab8583d12->leave($__internal_bbd0ec1b7c781cf92f1d8625f9df9216364e5efe62d32e89aa7bb3cab8583d12_prof);

        
        $__internal_4132364b0334e4ca2e539be968159593d1d7e24647c07e33db9e28236da80573->leave($__internal_4132364b0334e4ca2e539be968159593d1d7e24647c07e33db9e28236da80573_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/register.html.twig", "/Users/matt/Prog/Symfo/Mymfo/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}
