<?php

/* category/index.html.twig */
class __TwigTemplate_42b9b9ac89c52baac63f59699517101b190a31885754dec3ef24c7f61d3a14da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "category/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cee4cfff1cdbed99577069f03ee4c2319d4ae809edec0f039ab201fa3fe62e5c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cee4cfff1cdbed99577069f03ee4c2319d4ae809edec0f039ab201fa3fe62e5c->enter($__internal_cee4cfff1cdbed99577069f03ee4c2319d4ae809edec0f039ab201fa3fe62e5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "category/index.html.twig"));

        $__internal_51b308bed39e346fd1ed0f437abeed8c3ca5d0ee2b62a4655088f1187de33638 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_51b308bed39e346fd1ed0f437abeed8c3ca5d0ee2b62a4655088f1187de33638->enter($__internal_51b308bed39e346fd1ed0f437abeed8c3ca5d0ee2b62a4655088f1187de33638_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "category/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cee4cfff1cdbed99577069f03ee4c2319d4ae809edec0f039ab201fa3fe62e5c->leave($__internal_cee4cfff1cdbed99577069f03ee4c2319d4ae809edec0f039ab201fa3fe62e5c_prof);

        
        $__internal_51b308bed39e346fd1ed0f437abeed8c3ca5d0ee2b62a4655088f1187de33638->leave($__internal_51b308bed39e346fd1ed0f437abeed8c3ca5d0ee2b62a4655088f1187de33638_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_2acdc7e76b0475498f5508a111c35dc65f8b34531883cc25678070abdd7085a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2acdc7e76b0475498f5508a111c35dc65f8b34531883cc25678070abdd7085a7->enter($__internal_2acdc7e76b0475498f5508a111c35dc65f8b34531883cc25678070abdd7085a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_61f2e178e6f2a6ef676c63f4933fae032af2063bc09875d4ff3ab6d0c7c6573f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61f2e178e6f2a6ef676c63f4933fae032af2063bc09875d4ff3ab6d0c7c6573f->enter($__internal_61f2e178e6f2a6ef676c63f4933fae032af2063bc09875d4ff3ab6d0c7c6573f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.min.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>

    <h1>Categories list</h1>

    <table>
        <thead>
            <tr>
                <th class=\"text-warning\">Id</th>
                <th class=\"text-warning\">Name</th>
                <th class=\"text-warning\">Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 20
            echo "            <tr>
                <td><a href=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("catcrud_show", array("id" => $this->getAttribute($context["category"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "</td>
                <td>
                    <a href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("catcrud_show", array("id" => $this->getAttribute($context["category"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default\">show</a>
                    <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("catcrud_edit", array("id" => $this->getAttribute($context["category"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default\">edit</a>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "        </tbody>
    </table>
    <a href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("catcrud_new");
        echo "\" class=\"btn btn-default\">Create a new category</a>
";
        
        $__internal_61f2e178e6f2a6ef676c63f4933fae032af2063bc09875d4ff3ab6d0c7c6573f->leave($__internal_61f2e178e6f2a6ef676c63f4933fae032af2063bc09875d4ff3ab6d0c7c6573f_prof);

        
        $__internal_2acdc7e76b0475498f5508a111c35dc65f8b34531883cc25678070abdd7085a7->leave($__internal_2acdc7e76b0475498f5508a111c35dc65f8b34531883cc25678070abdd7085a7_prof);

    }

    public function getTemplateName()
    {
        return "category/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 31,  101 => 29,  91 => 25,  87 => 24,  82 => 22,  76 => 21,  73 => 20,  69 => 19,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.min.css') }}\" crossorigin=\"anonymous\">
    </head>

    <h1>Categories list</h1>

    <table>
        <thead>
            <tr>
                <th class=\"text-warning\">Id</th>
                <th class=\"text-warning\">Name</th>
                <th class=\"text-warning\">Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for category in categories %}
            <tr>
                <td><a href=\"{{ path('catcrud_show', { 'id': category.id }) }}\">{{ category.id }}</a></td>
                <td>{{ category.name }}</td>
                <td>
                    <a href=\"{{ path('catcrud_show', { 'id': category.id }) }}\" class=\"btn btn-default\">show</a>
                    <a href=\"{{ path('catcrud_edit', { 'id': category.id }) }}\" class=\"btn btn-default\">edit</a>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
    <a href=\"{{ path('catcrud_new') }}\" class=\"btn btn-default\">Create a new category</a>
{% endblock %}
", "category/index.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/category/index.html.twig");
    }
}
