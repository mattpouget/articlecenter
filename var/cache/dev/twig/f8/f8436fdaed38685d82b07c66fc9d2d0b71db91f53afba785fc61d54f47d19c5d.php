<?php

/* @FOSUser/Registration/register_content.html.twig */
class __TwigTemplate_b131d0d0f7a40ee3630fef1b9617cb7b44d03e57b6decc704aa2f0e46e21f787 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7efbd0a9d7e3faa17d00787b329441158bfe85109e5e66b060d74921b1af9b6e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7efbd0a9d7e3faa17d00787b329441158bfe85109e5e66b060d74921b1af9b6e->enter($__internal_7efbd0a9d7e3faa17d00787b329441158bfe85109e5e66b060d74921b1af9b6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig"));

        $__internal_6861b791d0145f88b3874318ca3277058e67be6cc168d3aae17c67d3a4ea7915 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6861b791d0145f88b3874318ca3277058e67be6cc168d3aae17c67d3a4ea7915->enter($__internal_6861b791d0145f88b3874318ca3277058e67be6cc168d3aae17c67d3a4ea7915_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig"));

        // line 2
        echo "<head>
    <link rel=\"stylesheet\" href=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
</head>

<nav class=\"navbar navbar-inverse\">
    <div class=\"container-fluid\">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
        </div>

        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
            <ul class=\"nav navbar-nav\">
                <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                <li><a href=\"listcat\">Catégories</a></li>
                ";
        // line 22
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 23
            echo "                    <li><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\" class=\"btn btn-danger\">Logout</a></li>
                ";
        } else {
            // line 25
            echo "                    <li><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\" class=\"btn btn-success\">Login</a></li>
                ";
        }
        // line 27
        echo "                <li><a href=\"/register\" class=\"btn btn-info\">Sign up.</a></li>
            </ul>
            ";
        // line 29
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            // line 30
            echo "                <ul class=\"nav navbar-nav navbar-right\">
                    <li><a href=\"/admin\">AdminBoard</a></li>
                </ul>
            ";
        }
        // line 34
        echo "        </div>
    </div>
</nav>

";
        // line 38
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("method" => "post", "action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register"), "attr" => array("class" => "fos_user_registration_register")));
        echo "
    ";
        // line 39
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    <div>
        <input class=\"btn btn-info\" type=\"submit\" value=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 43
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_7efbd0a9d7e3faa17d00787b329441158bfe85109e5e66b060d74921b1af9b6e->leave($__internal_7efbd0a9d7e3faa17d00787b329441158bfe85109e5e66b060d74921b1af9b6e_prof);

        
        $__internal_6861b791d0145f88b3874318ca3277058e67be6cc168d3aae17c67d3a4ea7915->leave($__internal_6861b791d0145f88b3874318ca3277058e67be6cc168d3aae17c67d3a4ea7915_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 43,  91 => 41,  86 => 39,  82 => 38,  76 => 34,  70 => 30,  68 => 29,  64 => 27,  58 => 25,  52 => 23,  50 => 22,  28 => 3,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
<head>
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.css') }}\" crossorigin=\"anonymous\">
</head>

<nav class=\"navbar navbar-inverse\">
    <div class=\"container-fluid\">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
        </div>

        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
            <ul class=\"nav navbar-nav\">
                <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                <li><a href=\"listcat\">Catégories</a></li>
                {% if is_granted('ROLE_USER') %}
                    <li><a href=\"{{ path('fos_user_security_logout') }}\" class=\"btn btn-danger\">Logout</a></li>
                {% else %}
                    <li><a href=\"{{ path('fos_user_security_login') }}\" class=\"btn btn-success\">Login</a></li>
                {% endif %}
                <li><a href=\"/register\" class=\"btn btn-info\">Sign up.</a></li>
            </ul>
            {% if is_granted('ROLE_ADMIN') %}
                <ul class=\"nav navbar-nav navbar-right\">
                    <li><a href=\"/admin\">AdminBoard</a></li>
                </ul>
            {% endif %}
        </div>
    </div>
</nav>

{{ form_start(form, {'method': 'post', 'action': path('fos_user_registration_register'), 'attr': {'class': 'fos_user_registration_register'}}) }}
    {{ form_widget(form) }}
    <div>
        <input class=\"btn btn-info\" type=\"submit\" value=\"{{ 'registration.submit'|trans }}\" />
    </div>
{{ form_end(form) }}
", "@FOSUser/Registration/register_content.html.twig", "/Users/matt/Prog/Symfo/Mymfo/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register_content.html.twig");
    }
}
