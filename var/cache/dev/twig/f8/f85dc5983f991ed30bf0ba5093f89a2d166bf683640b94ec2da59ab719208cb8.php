<?php

/* article/edit.html.twig */
class __TwigTemplate_562267c8dca1829985f856b41faa025946dd1b314af0bccec0252701d1a1ff0a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "article/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e1a609c2b4faa83377f3e26569ada94f38f75b6ea3bfabe3f28733bb18a2b376 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e1a609c2b4faa83377f3e26569ada94f38f75b6ea3bfabe3f28733bb18a2b376->enter($__internal_e1a609c2b4faa83377f3e26569ada94f38f75b6ea3bfabe3f28733bb18a2b376_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "article/edit.html.twig"));

        $__internal_a3ef16be8e232781a03c499b74dfa44b3a577c8428516f3e4c7f9a17ab54db69 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3ef16be8e232781a03c499b74dfa44b3a577c8428516f3e4c7f9a17ab54db69->enter($__internal_a3ef16be8e232781a03c499b74dfa44b3a577c8428516f3e4c7f9a17ab54db69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "article/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e1a609c2b4faa83377f3e26569ada94f38f75b6ea3bfabe3f28733bb18a2b376->leave($__internal_e1a609c2b4faa83377f3e26569ada94f38f75b6ea3bfabe3f28733bb18a2b376_prof);

        
        $__internal_a3ef16be8e232781a03c499b74dfa44b3a577c8428516f3e4c7f9a17ab54db69->leave($__internal_a3ef16be8e232781a03c499b74dfa44b3a577c8428516f3e4c7f9a17ab54db69_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_190e485e355850a6fc8101e6653814dbdbd7ec8e5270f12137edf7dcbe557e27 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_190e485e355850a6fc8101e6653814dbdbd7ec8e5270f12137edf7dcbe557e27->enter($__internal_190e485e355850a6fc8101e6653814dbdbd7ec8e5270f12137edf7dcbe557e27_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_68cdc8bbb0c89eaaa3e116baf0ae1969c847b0daff30f085ff4a44eb52105131 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_68cdc8bbb0c89eaaa3e116baf0ae1969c847b0daff30f085ff4a44eb52105131->enter($__internal_68cdc8bbb0c89eaaa3e116baf0ae1969c847b0daff30f085ff4a44eb52105131_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.min.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>Article edit</h1>
    ";
        // line 28
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 29
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "name", array()), 'label');
        echo "
        ";
        // line 30
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "name", array()), 'widget');
        echo "
        ";
        // line 31
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "content", array()), 'label');
        echo "
        ";
        // line 32
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "content", array()), 'widget');
        echo "
        ";
        // line 33
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "author", array()), 'label');
        echo "
        ";
        // line 34
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "author", array()), 'widget');
        echo "
        ";
        // line 35
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "date", array()), 'label');
        echo "
        ";
        // line 36
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "date", array()), 'widget');
        echo "
        ";
        // line 37
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "category", array()), 'label');
        echo "
        ";
        // line 38
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "category", array()), 'widget');
        echo "
        ";
        // line 39
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "image", array()), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" class=\"btn btn-warning\"/>
    ";
        // line 41
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <a href=\"";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("artcrud_index");
        echo "\" class=\"btn btn-default\">Back to the list</a>

    ";
        // line 45
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
        <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
    ";
        // line 47
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "


";
        
        $__internal_68cdc8bbb0c89eaaa3e116baf0ae1969c847b0daff30f085ff4a44eb52105131->leave($__internal_68cdc8bbb0c89eaaa3e116baf0ae1969c847b0daff30f085ff4a44eb52105131_prof);

        
        $__internal_190e485e355850a6fc8101e6653814dbdbd7ec8e5270f12137edf7dcbe557e27->leave($__internal_190e485e355850a6fc8101e6653814dbdbd7ec8e5270f12137edf7dcbe557e27_prof);

    }

    public function getTemplateName()
    {
        return "article/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 47,  137 => 45,  132 => 43,  127 => 41,  122 => 39,  118 => 38,  114 => 37,  110 => 36,  106 => 35,  102 => 34,  98 => 33,  94 => 32,  90 => 31,  86 => 30,  82 => 29,  78 => 28,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.min.css') }}\" crossorigin=\"anonymous\">
    </head>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-2\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"/\">Article's Center</a>
            </div>

            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-2\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/list\">Liste d'articles <span class=\"sr-only\">(current)</span></a></li>
                    <li><a href=\"/listcat\">Catégories</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>Article edit</h1>
    {{ form_start(edit_form) }}
        {{ form_label(edit_form.name) }}
        {{ form_widget(edit_form.name) }}
        {{ form_label(edit_form.content) }}
        {{ form_widget(edit_form.content) }}
        {{ form_label(edit_form.author) }}
        {{ form_widget(edit_form.author) }}
        {{ form_label(edit_form.date) }}
        {{ form_widget(edit_form.date) }}
        {{ form_label(edit_form.category) }}
        {{ form_widget(edit_form.category) }}
        {{ form_widget(edit_form.image) }}
        <input type=\"submit\" value=\"Edit\" class=\"btn btn-warning\"/>
    {{ form_end(edit_form) }}

    <a href=\"{{ path('artcrud_index') }}\" class=\"btn btn-default\">Back to the list</a>

    {{ form_start(delete_form) }}
        <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
    {{ form_end(delete_form) }}


{% endblock %}
", "article/edit.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/article/edit.html.twig");
    }
}
