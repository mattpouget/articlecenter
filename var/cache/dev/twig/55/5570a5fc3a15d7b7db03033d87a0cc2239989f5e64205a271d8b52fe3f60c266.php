<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_f41977333767a1e227a284071bf00bdd65d8cc45ef8bc5b82a9d5f870bdfe928 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_117f04c38d2564141e30a1083dd356eb6d9e9aef5058fa196e11a694bc822106 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_117f04c38d2564141e30a1083dd356eb6d9e9aef5058fa196e11a694bc822106->enter($__internal_117f04c38d2564141e30a1083dd356eb6d9e9aef5058fa196e11a694bc822106_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_503ca074e0b26f6ac58ebc376094b535212bea96f079ae8a09ebbbd51b4029e8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_503ca074e0b26f6ac58ebc376094b535212bea96f079ae8a09ebbbd51b4029e8->enter($__internal_503ca074e0b26f6ac58ebc376094b535212bea96f079ae8a09ebbbd51b4029e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_117f04c38d2564141e30a1083dd356eb6d9e9aef5058fa196e11a694bc822106->leave($__internal_117f04c38d2564141e30a1083dd356eb6d9e9aef5058fa196e11a694bc822106_prof);

        
        $__internal_503ca074e0b26f6ac58ebc376094b535212bea96f079ae8a09ebbbd51b4029e8->leave($__internal_503ca074e0b26f6ac58ebc376094b535212bea96f079ae8a09ebbbd51b4029e8_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_8e24e35aaea8bbc8c7c7ea5b786c74929b9279a2ae5e40aa54f0923e924159b8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e24e35aaea8bbc8c7c7ea5b786c74929b9279a2ae5e40aa54f0923e924159b8->enter($__internal_8e24e35aaea8bbc8c7c7ea5b786c74929b9279a2ae5e40aa54f0923e924159b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_3416e13b7a102ad1d3d2e42331fdc5ed1d02c700c45fc7d8cacf780bc1956561 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3416e13b7a102ad1d3d2e42331fdc5ed1d02c700c45fc7d8cacf780bc1956561->enter($__internal_3416e13b7a102ad1d3d2e42331fdc5ed1d02c700c45fc7d8cacf780bc1956561_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_3416e13b7a102ad1d3d2e42331fdc5ed1d02c700c45fc7d8cacf780bc1956561->leave($__internal_3416e13b7a102ad1d3d2e42331fdc5ed1d02c700c45fc7d8cacf780bc1956561_prof);

        
        $__internal_8e24e35aaea8bbc8c7c7ea5b786c74929b9279a2ae5e40aa54f0923e924159b8->leave($__internal_8e24e35aaea8bbc8c7c7ea5b786c74929b9279a2ae5e40aa54f0923e924159b8_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "@FOSUser/Security/login.html.twig", "/Users/matt/Prog/Symfo/Mymfo/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login.html.twig");
    }
}
