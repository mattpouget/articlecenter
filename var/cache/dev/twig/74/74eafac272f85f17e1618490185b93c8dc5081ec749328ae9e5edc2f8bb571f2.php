<?php

/* @Twig/images/icon-plus-square-o.svg */
class __TwigTemplate_f83bb2d0771211bc1567f4b1cecc1fb479f0d2cd8564f56038ec6f653e78c2bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2ee561ef4cb1bfc07e9ec5ef93bc9a89e25041d00e39f63ad4fa2233f44f024a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2ee561ef4cb1bfc07e9ec5ef93bc9a89e25041d00e39f63ad4fa2233f44f024a->enter($__internal_2ee561ef4cb1bfc07e9ec5ef93bc9a89e25041d00e39f63ad4fa2233f44f024a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square-o.svg"));

        $__internal_28b2b71a1bf4446d03e3b6fd54758f2a29e8e22e7fccf3ae973c189cdf1be740 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28b2b71a1bf4446d03e3b6fd54758f2a29e8e22e7fccf3ae973c189cdf1be740->enter($__internal_28b2b71a1bf4446d03e3b6fd54758f2a29e8e22e7fccf3ae973c189cdf1be740_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square-o.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H960v352q0 14-9 23t-23 9h-64q-14 0-23-9t-9-23V896H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h352V416q0-14 9-23t23-9h64q14 0 23 9t9 23v352h352q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
";
        
        $__internal_2ee561ef4cb1bfc07e9ec5ef93bc9a89e25041d00e39f63ad4fa2233f44f024a->leave($__internal_2ee561ef4cb1bfc07e9ec5ef93bc9a89e25041d00e39f63ad4fa2233f44f024a_prof);

        
        $__internal_28b2b71a1bf4446d03e3b6fd54758f2a29e8e22e7fccf3ae973c189cdf1be740->leave($__internal_28b2b71a1bf4446d03e3b6fd54758f2a29e8e22e7fccf3ae973c189cdf1be740_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-plus-square-o.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H960v352q0 14-9 23t-23 9h-64q-14 0-23-9t-9-23V896H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h352V416q0-14 9-23t23-9h64q14 0 23 9t9 23v352h352q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
", "@Twig/images/icon-plus-square-o.svg", "/Users/matt/Prog/Symfo/Mymfo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-plus-square-o.svg");
    }
}
