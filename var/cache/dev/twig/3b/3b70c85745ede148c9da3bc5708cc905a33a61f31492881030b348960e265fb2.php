<?php

/* category/show.html.twig */
class __TwigTemplate_836df5c2711979db2a6259f0d6b3cf96cb0def7c916b1bd5099f025f59344b1e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "category/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a12a9dc54e465834c3d97bae2048cc2884d207c3b897a49a0bcc7594415e748e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a12a9dc54e465834c3d97bae2048cc2884d207c3b897a49a0bcc7594415e748e->enter($__internal_a12a9dc54e465834c3d97bae2048cc2884d207c3b897a49a0bcc7594415e748e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "category/show.html.twig"));

        $__internal_f9d894f7296fea61b624531386c5432fa17c5acabdd8fb80340068b023d832cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f9d894f7296fea61b624531386c5432fa17c5acabdd8fb80340068b023d832cd->enter($__internal_f9d894f7296fea61b624531386c5432fa17c5acabdd8fb80340068b023d832cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "category/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a12a9dc54e465834c3d97bae2048cc2884d207c3b897a49a0bcc7594415e748e->leave($__internal_a12a9dc54e465834c3d97bae2048cc2884d207c3b897a49a0bcc7594415e748e_prof);

        
        $__internal_f9d894f7296fea61b624531386c5432fa17c5acabdd8fb80340068b023d832cd->leave($__internal_f9d894f7296fea61b624531386c5432fa17c5acabdd8fb80340068b023d832cd_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_9f667973b8e66120fb13ae9ff9008dd2d0f6a7cde9ec9bbb5aa244e7a43cc561 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9f667973b8e66120fb13ae9ff9008dd2d0f6a7cde9ec9bbb5aa244e7a43cc561->enter($__internal_9f667973b8e66120fb13ae9ff9008dd2d0f6a7cde9ec9bbb5aa244e7a43cc561_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b4cccca4b458bdae2351f01762a98b383e8ae7f6515ab5c5a74e578fba60d886 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4cccca4b458bdae2351f01762a98b383e8ae7f6515ab5c5a74e578fba60d886->enter($__internal_b4cccca4b458bdae2351f01762a98b383e8ae7f6515ab5c5a74e578fba60d886_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <head>
        <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/css/bootstrap.min.css"), "html", null, true);
        echo "\" crossorigin=\"anonymous\">
    </head>

    <h1>Category</h1>

    <table>
        <tbody>
            <tr>
                <th class=\"text-warning\">Id</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th class=\"text-warning\">Name</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "name", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

            <a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("catcrud_index");
        echo "\" class=\"btn btn-default\">Back to the list</a>
            <a href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("catcrud_edit", array("id" => $this->getAttribute((isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-default\">Edit</a>
            ";
        // line 25
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "

";
        
        $__internal_b4cccca4b458bdae2351f01762a98b383e8ae7f6515ab5c5a74e578fba60d886->leave($__internal_b4cccca4b458bdae2351f01762a98b383e8ae7f6515ab5c5a74e578fba60d886_prof);

        
        $__internal_9f667973b8e66120fb13ae9ff9008dd2d0f6a7cde9ec9bbb5aa244e7a43cc561->leave($__internal_9f667973b8e66120fb13ae9ff9008dd2d0f6a7cde9ec9bbb5aa244e7a43cc561_prof);

    }

    public function getTemplateName()
    {
        return "category/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 27,  87 => 25,  83 => 24,  79 => 23,  71 => 18,  64 => 14,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <head>
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/app/css/bootstrap.min.css') }}\" crossorigin=\"anonymous\">
    </head>

    <h1>Category</h1>

    <table>
        <tbody>
            <tr>
                <th class=\"text-warning\">Id</th>
                <td>{{ category.id }}</td>
            </tr>
            <tr>
                <th class=\"text-warning\">Name</th>
                <td>{{ category.name }}</td>
            </tr>
        </tbody>
    </table>

            <a href=\"{{ path('catcrud_index') }}\" class=\"btn btn-default\">Back to the list</a>
            <a href=\"{{ path('catcrud_edit', { 'id': category.id }) }}\" class=\"btn btn-default\">Edit</a>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            {{ form_end(delete_form) }}

{% endblock %}
", "category/show.html.twig", "/Users/matt/Prog/Symfo/Mymfo/app/Resources/views/category/show.html.twig");
    }
}
